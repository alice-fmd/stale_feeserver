/*
 * $Id: mainpage.c,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $
 *
/************************************************************************
**
** FeeClient library
** Copyright (c) 2005
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter,
** Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free
** Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
** MA 02111-1307  USA  
**
*************************************************************************/

/** @file   mainpage.c
    @author Matthias Richter
    @date   
    @brief  Title page documentation. */
/** @mainpage FeeServer

    @section intro Introduction

    @section overview Overview

    <!--
    <center><img src="pic_FeeCom-view.png" border="0" alt=""></center>
    -->

    - FEE API
    - DIM
    
    @section sysregs System requirements

    @section sw_components The S/W Components

    @section mpg_core The FeeServer core

    The core of the FeeServer handles the interface to the DIM framework and
    the FEE API.

    - @ref feesrv_core

    @section mpg_ce The ControlEngine

    The ControlEngine handles the actual hardware access

    - @ref feesrv_ce

    @section mpg_sample_commands Example Commands

    - @ref feesrv_exac
    - @ref rcu_issue

    @section mpg_links Related links on the web

    
    - <a class="el" href="http://www.ift.uib.no/~kjeks/wiki/index.php?title=Detector_Control_System_%28DCS%29_for_ALICE_Front-end_electronics">
          Detector Control System for the ALICE TPC electronics </a> 
    - <a class="el" href="http://ep-ed-alice-tpc.web.cern.ch/ep-ed-alice-tpc/">ALICE TPC electronics pages</a>
    - <a class="el" href="http://www.kip.uni-heidelberg.de/ti/DCS-Board/current/"> DCS board pages</a>
    - <a class="el" href="http://alicedcs.web.cern.ch/AliceDCS/"> ALICE DCS pages</a>
    - <a class="el" href="http://www.cern.ch/dim"> 
          Distributed Information Management System (DIM)</a>
    - <a class="el" href="http://www.ztt.fh-worms.de/en/projects/Alice-FEEControl/index.shtml"> 
          FeeCom Software pages (ZTT Worms)</a>


*/

#error Not for compilation
//
// EOF
//
