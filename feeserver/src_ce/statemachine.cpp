// $Id: statemachine.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include <cerrno>
#include "statemachine.hpp"
#include "device.hpp"
#include "ce_base.h"
#include "strings.h"

using namespace std;

CETransition CEStateMachine::fDefTrans[eLastDefaultTransition]={
  CETransition(), // eTransitionUnknown
  CETransition(eSwitchOn,     eStateOn,          "switchon", eStateOff),
  CETransition(eShutdown,     eStateOff,         "shutdown", eStateOn, eStateError, eStateFailure, eStateConfigured),
  CETransition(eConfigure,    eStateConfiguring, "configure",eStateOn, eStateConfigured),
  // configure done is for internal use and has no corresponding action
  // later this must be an available command in order to allow external configuration
  CETransition(eConfigureDone,eStateConfigured,"",     eStateConfiguring),
  CETransition(eReset,        eStateOn,          "reset",    eStateConfigured, eStateConfiguring, eStateError, eStateFailure),
  CETransition(eStart,        eStateRunning,     "start",    eStateConfigured),
  CETransition(eStop,         eStateConfigured,  "stop",     eStateRunning),
  CETransition(eNotify,       eStateUnknown,     "notify",   eStateFailure)
};

CEStateMachine::CEStateMachine()
{
  fpDevice=NULL;
  fState=eStateUnknown;
  fName.clear();
}

CEStateMachine::~CEStateMachine()
{
}

int CEStateMachine::Armor()
{
  int iResult=0;
  CreateStateChannel();
  if (fpDevice) {
    iResult=fpDevice->ArmorDevice();
    if (iResult<0) {
      CE_Error("can not armor device %s (%p), switch to error state\n", fpDevice->GetName(), fpDevice);
      ChangeState(eStateError);
    }
  }
  return iResult;
}

int CEStateMachine::ChangeState(CEState state) {
  int iResult=0;
  CELockGuard l(this);
  CEState current=GetCurrentState();
  if (current!=state) {
    if (state!=eStateUnknown && state!=current) {
      if ((iResult=StateLeaveDispatcher(current))<0)
	CE_Warning("exit handler for state %s returned %d\n", GetStateName(current), iResult);
      iResult=StateEnterDispatcher(state);
      fState=state;
    }
  } else {
    CE_Debug("device %s already in state %s, ignored\n", GetName(), GetStateName(current)); 
  }
  return iResult;
}

const char* CEStateMachine::GetName() {
  return (const char*)&fName[0];
}

int CEStateMachine::Check(CEState state)
{
  return state==fState;
}

int CEStateMachine::Check(CEState states[])
{
  if (states) {
    CEState* element=states;
    while (*element!=eStateUnknown) {
      if (*element==fState) return 1;
      element++;
    }
  }
  return 0;
}

CEState CEStateMachine::GetCurrentState()
{
  return fState;
}

int CEStateMachine::GetTranslatedState() {
  CEState current=GetCurrentState();
  if (fpMapper)
    return fpMapper->GetMappedState(current);
  return current;
}

int CEStateMachine::SetTranslationScheme(CEStateMapper* pMapper) {
  int iResult=0;
  if (pMapper) {
    if (fpMapper && fpMapper!=pMapper) {
      CE_Warning("overriding previously initialized mapper instance\n");
    }
    fpMapper=pMapper;
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

const char* CEStateMachine::GetCurrentStateName()
{
  if (IsDefaultState(fState)) {
    return GetDefaultStateName(fState);
  }
  CE_Warning("handling of additional states currently not implemented\n");
  return "UNKNOWN";
}

int CEStateMachine::TriggerTransition(const char* action, int iMode, int iParam, void* pParam)
{
  int iResult=0; 
  if (action) {
    CETransition* pTrans=FindTransition(action);
    if (pTrans) {
      iResult=TriggerTransition(pTrans, iMode, iParam, pParam);
    } else {
      CE_Error("no transition defined for action %s\n", action);
      iResult=-ENOENT;
    }
  } else {
    CE_Error("invalid pointer\n");
    iResult=-EINVAL;
  }
  return iResult;
}

int CEStateMachine::TriggerTransition(CETransitionId transition, int iMode, int iParam, void* pParam)
{
  int iResult=0; 
  CETransition* pTrans=FindTransition(transition);
  if (pTrans) {
    iResult=TriggerTransition(pTrans, iMode, iParam, pParam);
  } else {
    CE_Error("no transition defined for id %s\n", transition);
    iResult=-ENOENT;
  }
  return iResult;
}

int CEStateMachine::TriggerTransition(CETransition* pTrans, int iMode, int iParam, void* pParam)
{
  int iResult=0; 
  if (pTrans) {
    CELockGuard l(this);
    CEState current=GetCurrentState();
    if (current==pTrans->GetFinalState()) {
      //CE_Warning("device already in state %s: transition \'%s\' ignored\n", GetCurrentStateName(), GetTransitionName(pTrans));
    } else if (IsAllowedTransition(pTrans)) {
      iResult=StateLeaveDispatcher(current);
      if (iResult<0) {
	ChangeState(eStateError);
	return iResult;
      }
      CEState final=eStateUnknown;
      if (pTrans->IsDefaultTransition()) {
	final=DefaultTransitionDispatcher(pTrans, iParam, pParam);
      } else {
	final=TransitionDispatcher(pTrans, fpDevice, iParam, pParam);
      }
      if (final==eStateError) {
	iResult=-EFAULT;
	ChangeState(eStateError);
      } else if (IsValidState(final)) {
	iResult=StateEnterDispatcher(final);
	if (iResult<0) {
	  ChangeState(eStateError);
	  return iResult;
	}
	fState=final;
      } else {
	CE_Error("dispatcher for transition %d (%s) returned invalid state %d\n", pTrans->GetID(), 
		 pTrans->IsDefaultTransition()?GetDefaultTransitionName(pTrans):pTrans->GetName(), final);
	ChangeState(eStateError);
      }
    } else {
      //CE_Warning("device in state %s: transition \'%s\' not allowed\n", GetCurrentStateName(), action);
    }
  } else {
    CE_Error("invalid pointer\n");
    iResult=-EINVAL;
  }
  return iResult;
}

int CEStateMachine::Synchronize()
{
  int iResult=0; 
  if (fpDevice) {
    CELockGuard l(this);
    CEState state=fpDevice->EvaluateHardware();
    if (IsValidState(state)) {
      ChangeState(state);
    } else {
      CE_Error("device evaluate returned invalid state %d\n", state);
      fState=eStateError;
    }
  } else {
    CE_Fatal("state machine not initialized\n");
    iResult=-EFAULT;
  }
  return iResult;
}

int CEStateMachine::InitDevice(CEDevice* pDevice, string name)
{
  int iResult=0; 
  if (pDevice) {
    if (fpDevice) {
      CE_Warning("overriding device pointer %p with %p\n", fpDevice, pDevice);
    }
    fpDevice=pDevice;
    fName=name;
    CreateStateChannel();
  } else {
    CE_Error("invalid pointer\n");
    iResult=-EINVAL;
  }
  return iResult;
}

int CEStateMachine::UpdateStateChannel() {
  return -ENOSYS;
}

int CEStateMachine::CreateStateChannel() {
  return -ENOSYS;
}

int CEStateMachine::CreateAlarmChannel() {
  return -ENOSYS;
}

int CEStateMachine::SendAlarm(int alarm) {
  return 0;
}

int CEStateMachine::IsDefaultState(CEState state)
{
  int iResult=0; 
  switch (state) {
  case eStateUnknown:
  case eStateOff:         
  case eStateError:       
  case eStateFailure:     
  case eStateOn:          
  case eStateConfiguring: 
  case eStateConfigured:  
  case eStateRunning:     
    iResult=1;
  }
  return iResult;
}

int CEStateMachine::IsValidState(CEState state) {
  // handling of additional states is currently not implemented
  // we forward this function simply
  return IsDefaultState(state);
}

int CEStateMachine::IsDefaultTransition(CETransitionId id)
{
  int iResult=0; 
  iResult=id>eTransitionUnknown && id<eLastDefaultTransition;
  return iResult;
}

CETransition* CEStateMachine::FindTransition(const char* action) {
  // for now we are restricted to the default transitions, thats why there is no list handling
  CETransition* pTransition=NULL;
  for (int id=eTransitionUnknown+1; id<eLastDefaultTransition; id++) {
    const char* tn=GetDefaultTransitionName(&fDefTrans[id]);
    if (strlen(tn)!=0 && strcasecmp(tn, action)==0) {
      pTransition=&fDefTrans[id];
      break;
    }
  }
  return pTransition;
}

CETransition* CEStateMachine::FindTransition(CETransitionId id) 
{
  // for now we are restricted to the default transitions, thats why there is no list handling
  CETransition* pTransition=NULL;
  if (id<eLastDefaultTransition) {
    pTransition=&fDefTrans[id];
  }
  return pTransition;
}

int CEStateMachine::IsAllowedTransition(CETransition* pT) {
  int iResult=0;
  if (pT) {
    iResult=pT->IsInitialState(GetCurrentState());
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

CEState CEStateMachine::DefaultTransitionDispatcher(CETransition* transition, int iParam, void* pParam)
{
  CEState state=eStateUnknown;
  int iResult=0;
  if (transition) {
    if (fpDevice) {
      switch (transition->GetID()) {
      case eSwitchOn:
	iResult=fpDevice->SwitchOn(iParam, pParam);
	break;
      case eShutdown:
	iResult=fpDevice->Shutdown(iParam, pParam); 
	break;
      case eConfigure:
	iResult=fpDevice->Configure(iParam, pParam); 
	break;
      case eConfigureDone:
	// in principle there is no extra handler needed, so we just skip this
	//iResult=fpDevice->(iParam, pParam); 
	state=eStateConfigured;
	break;
      case eReset:
	iResult=fpDevice->Reset(iParam, pParam); 
	break;
      case eStart:
	iResult=fpDevice->Start(iParam, pParam); 
	break;
      case eStop:
	iResult=fpDevice->Stop(iParam, pParam); 
	break;
      case eNotify:
	//iResult=fpDevice->Notify(iParam, pParam); 
	CE_Error("action \'notify\' not yet supported\n");
	iResult=-ENOSYS;
	break;
      default:
	CE_Error("transition %d (%s) is not a default transition\n", transition->GetID(), transition->GetName());
	iResult=-EFAULT;
      }
      if (iResult>=0) state=transition->GetFinalState();
      else state=eStateError;
    } else {
      CE_Fatal("severe internal inconsistancy, device pointer missing\n");
    }
  } else {
    CE_Fatal("severe internal inconsistancy\n");
  }
  return state;
}

int CEStateMachine::StateEnterDispatcher(CEState state)
{
  int iResult=0;
  if (IsDefaultState(state)) {
    iResult=DefaultStateEnterDispatcher(state);
  } else {
    CE_Error("inconsistancy: currently there are no non-default states\n");
    fState=eStateError;
    return -EFAULT;
  }
  return iResult;
}

int CEStateMachine::DefaultStateEnterDispatcher(CEState state)
{
  int iResult=0;
  if (fpDevice) {
    switch (state) {
    case eStateOff:
      iResult=fpDevice->EnterStateOFF();
      break;
    case eStateOn:
      iResult=fpDevice->EnterStateON();
      break;
    case eStateConfiguring:
      //iResult=fpDevice->EnterStateCONFIGURING();
      //CE_Warning("no enter-state handler implemented for state CONFIGURING");
      break;
    case eStateConfigured:
      iResult=fpDevice->EnterStateCONFIGURED();
      break;
    case eStateRunning:
      iResult=fpDevice->EnterStateRUNNING();
      break;
    case eStateUnknown:
    case eStateError:
    case eStateFailure:
      // these states do not have a handler function
      break;
    default:
      CE_Error("state #%d (%s) is not a default state\n", state, GetStateName(state));
    }
  } else {
    CE_Fatal("severe internal inconsistancy, device pointer missing\n");
  }
  return iResult;
}

int CEStateMachine::StateLeaveDispatcher(CEState state)
{
  int iResult=0;
  if (IsDefaultState(state)) {
    iResult=DefaultStateLeaveDispatcher(state);
  } else {
    CE_Error("inconsistancy: currently there are no non-default states\n");
    fState=eStateError;
    return -EFAULT;
  }
  return iResult;
}

int CEStateMachine::DefaultStateLeaveDispatcher(CEState state)
{
  int iResult=0;
  if (fpDevice) {
    switch (state) {
    case eStateOff:
      iResult=fpDevice->LeaveStateOFF();
      break;
    case eStateOn:
      iResult=fpDevice->LeaveStateON();
      break;
    case eStateConfiguring:
      //iResult=fpDevice->LeaveStateCONFIGURING();
      //CE_Warning("no leave-state handler implemented for state CONFIGURING");
      break;
    case eStateConfigured:
      iResult=fpDevice->LeaveStateCONFIGURED();
      break;
    case eStateRunning:
      iResult=fpDevice->LeaveStateRUNNING();
      break;
    case eStateUnknown:
    case eStateError:
    case eStateFailure:
      // these states do not have a handler function
      break;
    default:
      CE_Error("state #%d (%s) is not a default state\n", state, GetStateName(state));
    }
  } else {
    CE_Fatal("severe internal inconsistancy, device pointer missing\n");
  }
  return iResult;
}

CEState CEStateMachine::TransitionDispatcher(CETransition* transition, CEDevice* pDevice, int iParam, void* pParam)
{
  CEState state=eStateUnknown;
  state=eStateError;
  CE_Error("handling of additional states currently not implemented\n");
  return state;
}

const char* CEStateMachine::GetStateName(CEState state) {
  if (IsDefaultState(state)) {
    return GetDefaultStateName(state);
  }
  CE_Error("currentl no other states than default ones allowd\n");
  return NULL;
}

const char* CEStateMachine::GetDefaultStateName(CEState state)
{
  const char* name="UNNAMED"; 
  switch (state) {
  case eStateUnknown:     name="UNKNOWN";     break;
  case eStateOff:         name="OFF";         break;
  case eStateError:       name="ERROR";       break;
  case eStateFailure:     name="FAILURE";     break;
  case eStateOn:          name="ON";          break;
  case eStateConfiguring: name="CONFIGURING"; break;
  case eStateConfigured:  name="CONFIGURED";  break;
  case eStateRunning:     name="RUNNING";     break;
  }
  return name;
}

const char* CEStateMachine::GetTransitionName(CETransition* pTransition)
{
  const char* name="unnamed";
  if (pTransition) {
    if (pTransition->IsDefaultTransition()) {
      name=GetDefaultTransitionName(pTransition);
    } else {
      CE_Warning("dynamic transition handling not implemented yet, ignoring id %d\n", pTransition->GetID());
    }
  } else {
    name="invalid";
    CE_Error("invalid argument\n");
  }
  return name;
}

const char* CEStateMachine::GetDefaultTransitionName(CETransition* pTransition)
{
  const char* name="unnamed";
  if (pTransition) {
    if (pTransition->IsDefaultTransition()) {
      name=pTransition->GetName();
    }
  } else {
    name="invalid";
    CE_Error("invalid argument\n");
  }
  return name;
}

CETransition::CETransition() {
  fId=eTransitionUnknown;
  fFinalState=eStateUnknown;
  fName="unknown";
}

CETransition::CETransition(CETransitionId id, 
			   CEState finalState, 
			   const char* name,
			   CEState initialState1, CEState initialState2, 
			   CEState initialState3, CEState initialState4, 
			   CEState initialState5) {
  fId=id;
  if (name) fName=name;
  else fName="unknown";
  fFinalState=finalState;
  if (initialState1!=eStateUnknown) fInitialStates.push_back(initialState1);
  if (initialState2!=eStateUnknown) fInitialStates.push_back(initialState2);
  if (initialState3!=eStateUnknown) fInitialStates.push_back(initialState3);
  if (initialState4!=eStateUnknown) fInitialStates.push_back(initialState4);
  if (initialState5!=eStateUnknown) fInitialStates.push_back(initialState5);
}

CETransition::~CETransition() {
}

CETransitionId CETransition::GetID() {
  return fId;
}

const char* CETransition::GetName() {
  return fName;
}

int CETransition::IsDefaultTransition() {
  int iResult=fId>eTransitionUnknown && fId<eLastDefaultTransition;
  return iResult;
}

int CETransition::IsInitialState(CEState state) {
  int iResult=0;
  if (fInitialStates.size()==0) iResult=1;
  else {
    vector<CEState>::iterator initialState=fInitialStates.begin();
    while (initialState!=fInitialStates.end()) {
      if ((iResult=(*initialState==state))==1) break;
      initialState++;
    }
  }
  return iResult;
}

CEState CETransition::GetFinalState() {
  return fFinalState;
}
