// $Id: rcu_service.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter and Dag T Larsen
** Please report bugs to Matthias.Richter@ift.uib.no, dagtl@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include <stdlib.h>        // malloc, free,
#include <stdio.h>         // snprintf
#include <string.h>
//#include <math.h>
#include <errno.h>
#include "ce_command.h"
#include "fee_errors.h"
#include "dcscMsgBufferInterface.h" // access library to the dcs board message buffer interface
#include "codebook_rcu.h"  // the mapping of the rcu memory
#include "ce_base.h" // common service handling
#ifdef TPC
#include "ce_tpc.h"
#endif //TPC
#ifdef PHOS
#include "ce_phos.h"
#endif //PHOS
#include "rcu_service.h"     // handling of the service API methods for rcu-like CEs
#include "rcu_issue.h"     // handling of the issue API method for rcu-like CEs
#include <unistd.h>	//for usleep
#include "dev_rcu.hpp"

/**
 * @file rcu_service.c
 * Service handling of the RCU, belongs to @ref rcu_service.
 */

/***************************************************************************
 * general initialisation
 */
int RCUinitializeCE() {
  int ceState=CE_OK;
  int nRet=CE_OK;
  if ((nRet=initRcuAccess(NULL))>=0) {
    unsigned int dcscDbgOptions=0;
    dcscDbgOptions|=PRINT_RESULT_HUMAN_READABLE;
    setDebugOptions(dcscDbgOptions);
  } else {
    ceState=FEE_FAILED;
    CE_Error("initRcuAccess finished with error code %d\n", nRet);
    fflush(stdout);
  }
  /*
  nRet=enableControl();
  if(nRet<0){
//   ceState=nRet;
    CE_Warning("enableControl finished with error code %d\n", nRet);
    CE_Info("set RCU state to RCU_NOT_ACCESSIBLE\n");
    fflush(stdout);
  }

  // initialize the FEC arrays
  nRet=readFECValidList();
  if(nRet<CE_OK){
    ceState=nRet;
    CE_Warning("readFECValidList finished with error code %d\n", nRet);
    fflush(stdout);
  }

  nRet=makeRegItemLists();
  if(nRet<CE_OK){
    ceState=nRet;
    CE_Warning("makeRegItemLists finished with error code %d\n", nRet);
    fflush(stdout);
  }

  nRet=publishServices();
  if(nRet<CE_OK){
    ceState=nRet;
    CE_Warning("publishServices finished with error code %d\n", nRet);
    fflush(stdout);
  }

  nRet=publishRegs();
  if(nRet<CE_OK){
    ceState=nRet;
    CE_Warning("publishRegs finished with error code %d\n", nRet);
    fflush(stdout);
  }
  */

  return ceState;
}

int RCUcleanUpCE() {
  int ceState=CE_OK;
  int nRet=CE_OK;
  nRet=releaseRcuAccess();
  CE_Info("releaseRcuAccess finished with error code %d\n", nRet);
  fflush(stdout);
  return ceState;
}

/***************************************************************************
 * handling of services
 */
#define numberOfRegs 20
#define SERVICE_NOLINK_VAL -2000
#define SERVICE_SIM_HYST 5
#ifdef RCUDUMMY
__u32 g_RCUAFL=0;
#endif //RCUDUMMY

int     numberOfAdrs=0;
/* Dag: check this declarations! 
__u32  FEESERVER_FEC_MONITOR   = 0;
__u32  FEESERVER_FEC_PRESENT   = 0;
char** FEESERVER_SERVICE_NAMES = 0;
__u32* FEESERVER_SERVICE_ADRS  = 0;
*/
const char*       g_regName[numberOfRegs]={"T_TH","AV_TH","AC_TH","DV_TH","DC_TH","TEMP",  "AV", "AC",  "DV","DC","L1CNT","L2CNT","SCLKCNT","DSTBCNT","TSMWORD","USRATIO","CSR0","CSR1","CSR2","CSR3"};
const char    g_regBitwidth[numberOfRegs]={    10,     10,     10,     10,     10,    10,    10,   10,    10,  10,     16,     16,       16,        8,        9,       16,    11,    14,    16,    16};
const float g_regConvFactor[numberOfRegs]={  0.25, 0.0043,  0.017, 0.0043,   0.03,  0.25,0.0043,0.017,0.0043,0.03,      1,      1,        1,        1,        1,        1,     1,     1,     1,     1}; 
const int      g_publishReg[numberOfRegs]={     0,      0,      0,      0,      0,     1,     1,    1,     1,   1,      0,      0,        0,        0,        0,        0,     0,     0,     0,     0};
const float   g_regDeadband[numberOfRegs]={  0.01,   0.01,   0.01,   0.01,   0.01,   0.5,  0.05, 0.05,  0.05,0.05,    0.5,    0.5,      0.5,      0.5,      0.5,      0.5,   0.5,   0.5,   0.5,   0.5};
/* this is indended to be a quick solution for "FEC simulation" and should be replaced
 * by functionality of the dev_fec class
 */
const float    g_regDefault[numberOfRegs]={    40,   3.61,   0.75,   2.83,   1.92,    40,   3.61,0.75,  2.83,1.92,      0,      0,        0,        0,        1,        1,     0,     0,     0,     0};

int   g_nofFECs=0;          // size of the two following arrays
int   g_nofFEC_A=0;         // number of FECs branch A
int   g_nofFEC_B=0;         // number of FECs branch B
int*  g_validList=NULL;     // array of registered FECs
int*  g_FECActiveList=NULL; // array of active FECs (from hardware register)

Item**     g_pRegItem=0;
__u32*   g_regItemAdr=0;
char** g_pRegItemName=0;
float*     g_deadband=0;
float* g_regItemValue=0;

int updateFECenable(float* pF, int FEC, int minor, void* parameter);
int setFECenable(float value, int FEC, int minor, void* parameter);

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::PublishServices()
{
  int ceState=CE_OK;
  int    nRet=CE_OK;
  int i=0;
  int j=0;
  nRet=readFECActiveList();
  if(ceState<CE_OK){
    ceState++;
    CE_Warning("readFECActiveList failed\n");
  }
  nRet=setFECCSR0();
  if(nRet<CE_OK){
    ceState++;
    CE_Warning("setCntRdo failed\n");
  }
  
  for (i=0; i<g_nofFECs; i++) {
    if (RCUisFECvalid(i)) {
      //Publish all regs of FEC 'i'
      for (j=0; j<numberOfRegs; j++) {
        if (g_publishReg[j]) {
          nRet=PublishService(i,j);
          if(nRet<CE_OK){
            CE_Error("publishService failed\n");
            ceState=nRet;
            return ceState;
          }
	}
      }
      char servicename[MAX_SERVICE_NAME_LEN+1]="";
      sprintf(servicename, "%02i_STATE", i);
      nRet=RegisterService(servicename, 0.5, updateFECenable, setFECenable, i, 0, this);
    }
  }
  return ceState;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::PublishService(int FEC, int reg) {
  int ceState=CE_OK;
  int nRet=CE_OK;
  //Name first, FEC last
  char servicename[MAX_SERVICE_NAME_LEN+1]="";
  if(FEC<10) {
    servicename[0]='0';
    sprintf(servicename+1,"%i_", FEC);
  }
  else {
    sprintf(servicename, "%i_", FEC);
  }
  strncpy(servicename+3, g_regName[reg], MAX_SERVICE_NAME_LEN-3);
  nRet=RegisterService(servicename, g_regDeadband[reg], updateService, NULL, FEC, reg, this);
  if(nRet<CE_OK) {
    CE_Error("publish failed\n");
    ceState=nRet;
    return ceState;
  }
  return ceState;
}

int updateService(float* pF, int FEC, int reg, void* parameter) {
  int ceState=CE_OK;
  int nRet=CE_OK;
  unsigned short unused;
  unsigned short base;
  unsigned short rnw;
  unsigned short bcast;
  unsigned short branch;
  unsigned short FECAdr;
  unsigned short BCRegAdr;
  int retFEC;
  CErcu* pRCU=(CErcu*)parameter;
    if (pF && pRCU) {
      //printf("updateService FEC-%d (%d) reg=%d\n", FEC, g_FECActiveList[FEC], reg);
      CEState states[]={eStateOn, eStateConfiguring, eStateConfigured, eStateRunning, eStateUnknown};
      if (pRCU->Check(states) && RCUisFECactive(FEC)) {
#ifdef  FECSIM
	/* simulate the FEC services */
	if (*pF==SERVICE_NOLINK_VAL) *pF=g_regDefault[reg];
	else if (*pF<g_regDefault[reg]+SERVICE_SIM_HYST*g_regDeadband[reg]) *pF+=g_regDeadband[reg];
	else *pF=g_regDefault[reg]-SERVICE_SIM_HYST*g_regDeadband[reg];
#elif ! defined(RCUDUMMY)
	unused   = 0x0000;
	base     = 0x000c;
	rnw      = 0x0001;
	bcast    = 0x0000;
	branch   = FEC>=16;
	FECAdr   = FEC-16*branch;
	BCRegAdr = reg + 1;
//	__u32 writeAddress = (unused<<16) | (base<<12) | (rnw<<11) | (bcast<<10) | (branch<<9) | (FECAdr<<5) | (BCRegAdr<<0);
        __u32 writeAddress = (unused<<16) | (base<<MSMCommand_base) | (rnw<<MSMCommand_rnw) | (bcast<<MSMCommand_bcast) | (branch<<MSMCommand_branch) | (FECAdr<<MSMCommand_FECAdr) | (BCRegAdr<<MSMCommand_BCRegAdr);
	__u32 u32RawData;
	nRet=rcuSingleWrite(FECResetErrReg, 0x0);
	if(nRet<0){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("can not reset RCU SC Error register: rcuSingleWrite failed (%d)\n", nRet);
	  ceState=FEE_FAILED;
	  return ceState;
	}
	nRet=rcuSingleWrite(writeAddress, 0x0);
	if(nRet<0){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("can not write RCU SC access command: rcuSingleWrite failed (%d)\n", nRet);
	  ceState=FEE_FAILED;
	  return ceState;
	}
	nRet=rcuSingleRead(FECErrReg, &u32RawData);
	if(nRet<0){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("updateService: rcuSingleRead failed\n");
	  ceState=FEE_FAILED;
	  return ceState;
	}
	if(u32RawData&(0x1<<0)){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("updateService: instruction to not active FEC\n");
	  ceState=FEE_FAILED;
	  return ceState;
	}
	if(u32RawData&(0x1<<1)){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("updateService: no acknowledge from FEC\n");
	  ceState=FEE_FAILED;
	  return ceState;
	}
	nRet=rcuSingleRead(FECResultREG, &u32RawData);
	if(nRet<0){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("can not read RCU SC result register: rcuSingleRead failed\n", nRet);
	  ceState=FEE_FAILED;
	  return ceState;
	}
	*pF = (u32RawData & 0xffff) * g_regConvFactor[reg];
	retFEC=u32RawData>>16;
	if(retFEC!=FEC){
	  *pF=SERVICE_NOLINK_VAL;
	  CE_Warning("updateService: FEC mismatch, addressed %d, got back %d\n", FEC, retFEC);
	  ceState=FEE_FAILED;
	  return ceState;
	}
	//    printf("u32RawData=%i\n",u32RawData);
	//    printf("u32RawData=%i FEC=%i retFEC=%i 65536=%i\n",u32RawData, FEC, retFEC, u32RawData % 65536);
	//    printf("unused=%i base=%i rnw=%i bcast=%i branch=%i FEC=%i FECAdr=%i BCRegAdr=%i g_pItemValue[%i][%i]=%f\n", unused, base, rnw, bcast, branch, FEC, FECAdr, BCRegAdr, FEC, reg, *pF);
#endif // FECSIM & RCUDUMMY
      }
      else {
	//printf("not active");
	*pF=SERVICE_NOLINK_VAL;
      }
    } else {
      CE_Error("invalid location\n");
    }
  //printf("...done\n");
  return ceState;
}

/**
 * Update the value of the 'ACTIVE' service for a FEC.
 * This function is registerd during service registration in
 * @ref publishService.  
 * @param pF         location to update
 * @param FEC        FEC number
 * @param minor      not used
 * @param parameter  not used
 * @ingroup rcu_service
 */
int updateFECenable(float* pF, int FEC, int minor, void* parameter) {
  int ceState=CE_OK;
  int nRet=CE_OK;
  CErcu* pRCU=(CErcu*)parameter;
  if (pF && pRCU) {
      CEState states[]={eStateOn, eStateConfiguring, eStateConfigured, eStateRunning, eStateUnknown};
    *pF=(float)(pRCU->Check(states) && RCUisFECactive(FEC));
  } else {
    CE_Error("invalid location\n");
  }
  return ceState;
}

/**
 * Activate a FEC.
 * This function corresponds to the service 'ACTIVE' of a FEC.
 * This function is registerd during service registration in
 * @ref publishService.  
 * @param value      0 diactivated, otherwise activated 
 * @param FEC        FEC number
 * @param minor      not used
 * @param parameter  not used
 * @ingroup rcu_service
 */
int setFECenable(float value, int FEC, int minor, void* parameter) {
  int ceState=CE_OK;
  int nRet=CE_OK;
  CErcu* pRCU=(CErcu*)parameter;
  if (RCUisFECvalid(FEC)) {
    CEState states[]={eStateOn, eStateConfiguring, eStateConfigured, eStateRunning, eStateUnknown};
    if (pRCU && pRCU->Check(states)) {
      if (FEC<g_nofFECs) {
#ifndef RCUDUMMY
	__u32 u32FECActiveList=0;
	int i;
	nRet=rcuSingleRead(FECActiveList, &u32FECActiveList);
	if(nRet>=0){
	  if (value==0.0) {
	    u32FECActiveList&=~(0x1<<FEC);
	  } else {
	    u32FECActiveList|=0x1<<FEC;
	  }
	  nRet=rcuSingleWrite(FECActiveList, u32FECActiveList);
	  if (nRet<0) {
	    CE_Warning("can not write RCU AFL: rcuSingleWrite failed\n");
	    ceState=FEE_FAILED;
	  }
	} else {
	  CE_Warning("can not fetch RCU AFL: rcuSingleRead failed\n");
	  ceState=FEE_FAILED;
	}
#else
	if (value==0.0) {
	  g_RCUAFL&=~(0x1<<FEC);
	} else {
	  g_RCUAFL|=0x1<<FEC;
	}
#endif //RCUDUMMY
      } else {
	CE_Error("FEC index %d out of range, max %d FECs defined\n", FEC, g_nofFECs);
	ceState=FEE_FAILED;
      }
    }
  } else {
    CE_Warning("FEC #%d was not validated at startup, can not enable\n", FEC); 
  }
  return ceState;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::PublishRegs()
{
  int ceState=CE_OK;
  int nRet=CE_OK;
  int i;
  for(i=0;i<numberOfAdrs;i++){
    nRet=PublishReg(i);
    if(nRet<CE_OK){
      CE_Error("publishReg failed\n");
      ceState=nRet;
      return ceState;
    }
  }
  return ceState;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::PublishReg(int regNum)
{
  int ceState=CE_OK;
  int nRet=CE_OK;
  nRet =  RegisterService(g_pRegItemName[regNum], g_deadband[regNum], updateReg, NULL, regNum, 0, this);
  nRet=publish(g_pRegItem[regNum]);
  if(nRet<CE_OK){
    CE_Error("publish failed\n");
    ceState=nRet;
    return ceState;
  }
  return ceState;
}

int updateReg(float* pF, int regNum, int dummy, void* parameter){
  int ceState=CE_OK;
  int nRet=CE_OK;
  CErcu* pRCU=(CErcu*)parameter;
  CEState states[]={eStateOn, eStateConfiguring, eStateConfigured, eStateRunning, eStateUnknown};
  if (pF && pRCU && pRCU->Check(states)) {
#ifndef RCUDUMMY
    __u32 u32RawData;
    nRet=rcuSingleRead(g_regItemAdr[regNum],&u32RawData);
    if(nRet<0){
      *pF=SERVICE_NOLINK_VAL;
      CE_Warning("updateReg: rcuSingleRead failed\n");
      ceState=FEE_FAILED;
      return ceState;
    }
    *pF=u32RawData;
#endif // RCUDUMMY
  } else {
    CE_Error("updateReg: invalid location\n");
  }
  return ceState;
}

/* update the active FEC list from the hardware
 * reads the corresponding RCU register and updates the internal array
 * the function also creates the global array 
 */
int readFECActiveList(){
  int ceState=CE_OK;
  int nRet=CE_OK;
  extern CErcu g_RCU;
  CEState states[]={eStateOn, eStateConfiguring, eStateConfigured, eStateRunning, eStateUnknown};
  if (g_RCU.Check(states)) {
    if (g_nofFECs!=0) {
      if (g_FECActiveList==NULL) {
	g_FECActiveList=(int*)malloc(g_nofFECs*sizeof(int));
	if (g_FECActiveList) {
	  memset(g_FECActiveList, 0, g_nofFECs*sizeof(int));
	}
      }
      __u32 u32FECActiveList=0;
      int i;
#ifndef RCUDUMMY
      nRet=rcuSingleRead(FECActiveList, &u32FECActiveList);
#else
      nRet=0;
      u32FECActiveList=g_RCUAFL;
#endif //RCUDUMMY
      if (nRet==-ETIMEDOUT) {
      }
      if (g_FECActiveList) {
	int bChanged=0;
	for (i=0; i<8*sizeof(__u32) && i<g_nofFECs; i++){
	  // TODO: this is a bug, global and local (branch) addressing mixed
	  bChanged|=(g_FECActiveList[i]!=((u32FECActiveList&((__u32)0x1<<i))));
	  g_FECActiveList[i]=(u32FECActiveList&((__u32)0x1<<i))!=0;
	  //printf("g_FECActiveList[%i]=%i\n",i,g_FECActiveList[i]);
	}
	  if (bChanged) {
	    // set the board controller registers to continous meassurement
	    // this is a quick hack
#ifndef RCUDUMMY
	    setFECCSR0();
#endif
	  }
      } else {
	CE_Error("active FEC list not initialized\n");
      }
      if(nRet<0){
	CE_Warning("readFECActiveList: rcuSingleRead failed\n");
	ceState=FEE_FAILED;
      }
    }
  }
  return ceState;
}

/* produce a text output from the list of valid FECs
 * NOTE: there are two internal list for the status of the FECs
 *       the "vaild list"  denotes the cards which has been registered to the CE at startup
 *       the "active list" represents the hardware status 
 */
int getValidFECTableString(char** ppTgt) {
  int iResult=0;
  if (g_validList) {
    if (ppTgt) {
      int cards_per_branch[NOF_BRANCHES];
      int branch_rows[NOF_BRANCHES];
      const char* branch_name[NOF_BRANCHES];
      int branch_card_offset[NOF_BRANCHES];
      cards_per_branch[0]=g_nofFEC_A;
      cards_per_branch[1]=g_nofFEC_B;
      branch_rows[0]=0;
      branch_rows[1]=0;
      branch_name[0]="A";
      branch_name[1]="B";
      branch_card_offset[0]=FEC_BRANCH_A_TO_GLOBAL(0);
      branch_card_offset[1]=FEC_BRANCH_B_TO_GLOBAL(0);
      int branch=0;
      int i=0;
      int row=0;
      int cardsPerRow=10;
      int nofRows=0;
      for (branch=0; branch<NOF_BRANCHES; branch++) {
	if (cards_per_branch[branch]>0)
	  branch_rows[branch]=1+(cards_per_branch[branch]-1)/cardsPerRow;
	nofRows+=branch_rows[branch];
      }
      char* strTable=NULL;
      const char* blank=" ";
      int iSizeTableString=(cardsPerRow*3 + 12)*3*nofRows+nofRows;
      strTable=(char*)malloc(iSizeTableString);
      if (strTable) {
	memset(strTable, 0, iSizeTableString);
	branch=0;
	for (branch=0; branch<NOF_BRANCHES; branch++) {
	  for (row=0;  row<branch_rows[branch]; row++) {
	    // header line
	    if (strlen(strTable)>0)
	      sprintf(strTable, "%s\n\nBRANCH %s #:", strTable, branch_name[branch]);
	    else
	      sprintf(strTable, "BRANCH %s #:", branch_name[branch]);
	    for (i=row*cardsPerRow; i<(row+1)*cardsPerRow && i<cards_per_branch[branch]; i++) {
	      if (i<10) blank=" ";
	      else blank="";
	      sprintf(strTable, "%s %s%d", strTable, blank, i);
	    }
	    sprintf(strTable, "%s\n===========", strTable);
	    for (i=row*cardsPerRow; i<(row+1)*cardsPerRow && i<cards_per_branch[branch]; i++) {
	      sprintf(strTable, "%s===", strTable);
	    }
	    sprintf(strTable, "%s\n           ", strTable);
	    for (i=row*cardsPerRow; i<(row+1)*cardsPerRow && i<cards_per_branch[branch]; i++) {
	      sprintf(strTable, "%s  %d", strTable, g_validList[branch_card_offset[branch]+i]);
	    }
	  }
	}
      }
      *ppTgt=strTable;
      iResult=iSizeTableString;
    } else {
      iResult=FEE_NULLPOINTER;
    }
  } else {
    const char* message="no Front-End Cards defined";
    iResult=strlen(message);
    *ppTgt=(char*)malloc(iResult+1);
    if (*ppTgt) strcpy(*ppTgt, message);
    else iResult=FEE_INSUFFICIENT_MEMORY;
  }
  return iResult;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::ReadFECValidList(){
  int ceState=CE_OK;
  if (g_validList==NULL) {
#   ifdef NOF_FEC_BRANCH_A
    g_nofFEC_A=NOF_FEC_BRANCH_A;
#   endif
#   ifdef NOF_FEC_BRANCH_B
    g_nofFEC_B=NOF_FEC_BRANCH_B;
#   endif
    if (g_nofFEC_A>MAX_BRANCH_A_FEC) {
      g_nofFEC_A=MAX_BRANCH_A_FEC;
      CE_Warning("number of FEC in branch A (%d) exceeds limit %d, list truncated\n", g_nofFEC_A, MAX_BRANCH_A_FEC);
    }
    if (g_nofFEC_B>MAX_BRANCH_B_FEC) {
      g_nofFEC_B=MAX_BRANCH_B_FEC;
      CE_Warning("number of FEC in branch B (%d) exceeds limit %d, list truncated\n", g_nofFEC_B, MAX_BRANCH_B_FEC);
    }
    if (g_nofFEC_A>0 || g_nofFEC_B>0)
      g_nofFECs=MAX_NOF_FEC;
    else
      g_nofFECs=NOF_FEC_DEFAULT;

    if (g_nofFECs>0)
      g_validList=(int*)malloc(g_nofFECs*sizeof(int));
  }
  if (g_validList) {
    memset(g_validList, 0, g_nofFECs*sizeof(int));
    int i;
    char* FECValidList;
    FECValidList = getenv("FEESERVER_FEC_MONITOR");
    if(FECValidList!=NULL){
      for(i=0; i<g_nofFECs && FECValidList[i]!=0 && ceState==CE_OK; i++)
	{
	  if(FECValidList[i]=='1'){
	    g_validList[i] = 1;
	  }
	  else if (FECValidList[i]!='0'){
	    ceState=FEE_FAILED;
	    CE_Error("environment variable FEESERVER_FEC_MONITOR not set properly\n");
	    fflush(stdout);
	    return ceState;
	  }
	}
    } else{
#ifdef ENABLE_AUTO_DETECTION
      /**********************************************************
       * testing for Environment variable FEESERVER_FEC_MONITOR
       * if variable is not set, try to detect the FECs
       ***********************************************************/
      int detectFECs();
      detectFECs();
      //checkValidFECs();
#endif //ENABLE_AUTO_DETECTION
    }
  } else if (g_nofFECs>0) {
    ceState=FEE_INSUFFICIENT_MEMORY;
  }

  if (ceState==CE_OK) {
    char* strTable=NULL;
    int strSize=getValidFECTableString(&strTable);
    if (strTable!=NULL) {
      if (strSize>0)
	CE_Info("\nvalid Frontend cards:\n%s\n\n", strTable);
      else 
	CE_Error("string pointer received but size zero\n"); 
      free(strTable);
    }
  }
  return ceState;
}

/* sets the g_validList of the service handling according to the bitfield
 * parameter:
 *    bitfield - each bit set corresponds to a valid FEC
 */
int setValidFECs(__u32 bitfield) 
{
  int ceState = CE_OK;
  int i=0;
  if (g_validList) {
    for(i=0; i<g_nofFECs && i<sizeof(__u32); i++){
      g_validList[i] = (bitfield&(0x1<<i))!=0;
      if (g_validList[i]==1)
	CE_Info("FEC #%d activated\n", i);
    }
  } else if (g_nofFECs>0) {
    CE_Error("setValidFECs: list uninitialized\n");
  } else  {
    CE_Warning("setValidFECs: no Front-End Crads defined\n");
  }
  return ceState;
}

/* sets the g_validList of the service handling according to the bitfield
 * parameter:
 *    bitfield - each bit set corresponds to a valid FEC
 */
int CErcu::MakeRegItemLists()
{
  int ceState = CE_OK;
  char* serviceAdr  = NULL;
  char* serviceName = NULL;
  char* serviceDead = NULL;
  serviceAdr  = getenv("FEESERVER_SERVICE_ADR" );
  serviceName = getenv("FEESERVER_SERVICE_NAME");
  serviceDead = getenv("FEESERVER_SERVICE_DEAD");
  g_regItemAdr   = cnvAllExtEnvI(splitString(serviceAdr ));
  g_pRegItemName =               splitString(serviceName);
  g_deadband     = cnvAllExtEnvF(splitString(serviceDead));
  if(g_regItemAdr!=NULL && g_pRegItemName!=NULL && g_deadband!=NULL){
    if((g_regItemAdr!=NULL && (g_pRegItemName==NULL || g_deadband==NULL)) || (g_pRegItemName!=NULL && (g_regItemAdr==NULL || g_deadband==NULL)) || (g_deadband!=NULL && (g_pRegItemName==NULL || g_regItemAdr==NULL))){
      ceState=FEE_FAILED;
      CE_Error("Environment variables FEESERVER_SERVICE_ADR, FEESERVER_SERVICE_DEAD  and FEESERVER_SERVICE_NAME do not match\n");
      fflush(stdout);
      return ceState;
    }
    numberOfAdrs = sizeof(g_regItemAdr)/sizeof(g_regItemAdr[0]);
    if(numberOfAdrs!=(sizeof(g_pRegItemName)/sizeof(g_pRegItemName[0])) || numberOfAdrs!=(sizeof(g_deadband)/sizeof(g_deadband[0]))){
      ceState=FEE_FAILED;
      CE_Error("Environment variables FEESERVER_SERVICE_NAME and FEESERVER_SERVICE_ADR do not match\n");
      fflush(stdout);
      return ceState;
    }
    g_pRegItem     = (Item**) malloc(numberOfAdrs*sizeof(Item*));
    g_regItemValue = (float*) malloc(numberOfAdrs*sizeof(float));
    if (g_pRegItem==0 || g_deadband==0 || g_regItemValue==0){
      ceState=FEE_INSUFFICIENT_MEMORY;
      CE_Error("no memory available\n");
      fflush(stdout);
      return ceState;
    }
  }
  return ceState;
}

/**
 * Enable the control of the RCU Altro Bus for the DCS board.
 * The functions reads the RCU firmware version register and probes for supported
 * FW functionality if server was compiled with --enable-auto-detection.
 * Newer firmware versions provide access to the Slow control registers even 
 * when the DDL has access to the Altro Bus. In that case the control of the Altro 
 * Bus is given to the DDL. Older firmware versions require that the DCS board has 
 * access to the Altro Bus. The RCU firmware does not provide any state register
 * about the Altro Bus switch and others, so we have to probe and be lucky.<br>
 * The following scheme applies:
 * 1. The version register 0x8008 is checked for a valid RCU FW version number. <br>
 *  if no valid version number found <br>
 * 2. The version register 0x8006 is checked for a valid RCU FW version number. A
 *    few FW version from May 06 use 0x8006 as version register. This was before the
 *    final address map was fixed. <br>
 *  if no valid version number found and compiled with --enable-auto-detection <br>
 * 3. The FW is probed for features
 * @return   0 DCS board is ABM <br>
 *           1 no RCU firmware version number found but MSM registers accessible
 *           while DDL SIU is ABM <br>
 *           >1 RCU firmware version number: DDL SIU is ABM <br>
 *           neg. error code if failed
 */
int enableControl(){
  int nRet=CE_OK;
  int i=0;
  __u32 abState=CErcu::eAbmDDLsiu;
  __u32 fwVersion=0;

  nRet=rcuSingleWrite(abState, 0x0);
  if(nRet<0){
    CE_Warning("no access to RCU\n");
    return FEE_FAILED;
  }

  nRet=rcuSingleRead(RCUFwVersion, &fwVersion);
  if(nRet<0){
    CE_Error("reading of FW version register %#x failed\n", RCUFwVersion);
  }

  if((nRet<0) || (fwVersion==0)){
    CE_Info("could not get FW version ... "
	    "try to read FW version register again with DCS board as bus master\n");
    nRet=rcuSingleWrite(CErcu::eAbmDCSboard, 0x0);
    nRet=rcuSingleRead(RCUFwVersion, &fwVersion);
    if(nRet<0){
      CE_Warning("second trial to read FW version failed\n");
    }
  }

  if((nRet<0) || (fwVersion==0)){
    CE_Info("could not get FW version from %x ... "
	    "try to read old FW version register %x\n",
	    RCUFwVersion, RCUFwVersionOld);
    nRet=rcuSingleRead(RCUFwVersionOld, &fwVersion);
    if(nRet<0){
      CE_Warning("second trial to read FW version failed\n");
    }
  }

  if (fwVersion>0) {
    CE_Info("RCU firmware version is %#x\n", fwVersion);
    CE_Info("DDL set to default Altro Bus master\n");
    abState=CErcu::eAbmDDLsiu;
    nRet=rcuSingleWrite(abState, 0x0);
    if(nRet<0){
      CE_Warning("no access to RCU\n");
      return FEE_FAILED;
    }
    return fwVersion;
  }

#ifdef ENABLE_AUTO_DETECTION
  CE_Info("RCU firmware without version register; probing MSM access\n");
  __u32 aflBackup=0;
  nRet=rcuSingleRead(FECActiveList, &aflBackup);
  if(nRet<0){
    CE_Error("reading Active FEC List failed\n");
    return FEE_FAILED;
  }

  abState=CErcu::eAbmDDLsiu;
  nRet=rcuSingleWrite(abState, 0x0);
  if(nRet<0){
    CE_Warning("no access to RCU\n");
    return FEE_FAILED;
  }

  for (i=0; i < 2; i++) {
    __u32 testPattern=~aflBackup;
    __u32 readback=0;
    nRet=rcuSingleWrite(FECActiveList, testPattern);
    if(nRet<0){
      CE_Warning("can not write test pattern to AFL\n");
      return FEE_FAILED;
    }
    nRet=rcuSingleRead(FECActiveList, &readback);
    if(nRet<0){
      CE_Warning("reading of test pattern from AFL failed\n");
      return FEE_FAILED;
    }
    if (readback==testPattern) {
      if (abState==CErcu::eAbmDDLsiu) {
	CE_Info("this RCU firmware versions seems to allow MSM access while the DDL is Altro Bus master\n");
	CE_Info("DDL set to default Altro Bus master\n");
      } else {
	CE_Info("this RCU firmware versions does not allow MSM access while the DDL is Altro Bus master\n");
	CE_Info("DCS board set to default Altro Bus master\n");
      }
      break;
    } else if (abState==CErcu::eAbmDDLsiu) {
      // next trial with DCS as Altro Bus master
      abState=CErcu::eAbmDCSboard;
      nRet=rcuSingleWrite(abState, 0x0);
      if(nRet<0){
	CE_Warning("no access to RCU\n");
	return FEE_FAILED;
      }
    }
  }
  
  nRet=rcuSingleWrite(FECActiveList, aflBackup);
  if(nRet<0){
    CE_Warning("can not restore AFL\n");
    return FEE_FAILED;
  }
  nRet=abState==CErcu::eAbmDDLsiu;
#else //!ENABLE_AUTO_DETECTION
  CE_Info("RCU firmware without version register;"
	  "RCU firmware probing is disabled\n");
  CE_Info("DDL set to default Altro Bus master\n");
  abState=CErcu::eAbmDDLsiu;
  nRet=rcuSingleWrite(abState, 0x0);
  if(nRet<0){
    CE_Warning("no access to RCU\n");
    return FEE_FAILED;
  }
  nRet=1;
#endif //ENABLE_AUTO_DETECTION
  return nRet;
}

int CErcu::EvaluateFirmwareVersion() 
{
  return enableControl();
}

/**
 * Build an address for writing to or reading from a BC register.
 * 
 * @param rnw		ReadNotWrite: set 0 for a write to a BC register, 1 for a read of an register
 * @param bcast		Broadcast: set to 1 for an boradcast
 * @param branch	Branch: set 0 for Branch A or 1 for Branch B
 * @param FECAdr	FrontEndCard address, specifies the FEC, 0x000 is the first one
 * @param BCRegAdr	Bordcontroller registeraddress: the address of the register you want to talk to
 * @return 		the address to the BC register you want to write to
 * 
 * For further information see the RCU Firmware: Registers and Commands, chapter 3.3
*/
__u32 getFecAddress(unsigned short rnw, unsigned short bcast, unsigned short branch, unsigned short FECAdr, unsigned short BCRegAdr){
  unsigned short unused = 0x000;
  unsigned short base = 0x00C;

  __u32 writeAddress = (unused<<16) | (base<<MSMCommand_base) | (rnw<<MSMCommand_rnw) | (bcast<<MSMCommand_bcast) | (branch<<MSMCommand_branch) | (FECAdr<<MSMCommand_FECAdr) | (BCRegAdr<<MSMCommand_BCRegAdr);

  return writeAddress;
}

/**
 * Find out how much FECs can be attached to a branch
 * 
 * @param branch	for now either 0x0 for Branch A or 0x1 for branch B
 * @return 		number of FECs that can max. be attached
*/
unsigned maxFecsInBranch(unsigned short branch){
  unsigned maxFECAdr=0;
  maxFECAdr = (branch == 0x0) ? g_nofFEC_A : g_nofFEC_B;
  return maxFECAdr;
}

/**
 * Find out which branch we are in
 * 
 * @param		index: the address of the fec from which we want to know to know the branch
 * @return		0x000 for branch A or 0x001 for branch B
*/
unsigned short getBranch(int index){
  unsigned short branch=0x000;
  (index < MAX_BRANCH_A_FEC) ? (branch = 0x000) : (branch = 0x001);
  return branch;
}

/**
 * tries to read the temperature register of the given BC address and evaluates the Errorregister
 * 
 * @param address	writeaddress: the address that should be checked
 * @return		returns 0 if no error occurred, -1 if card was not active, -2 if no ack was received from card
*/
int checkSingleFEC(__u32 writeAddress){
  __u32 u32RawData;
  int ceState=CE_OK;
  int nRet=CE_OK;
  int result;

  //clear Error Register	
  nRet = rcuSingleWrite(FECResetErrReg, 0x0);
    if(nRet<0){
    CE_Warning("detectFECs: rcuSingleWrite failed\n");
    ceState=FEE_FAILED;
    return ceState;
  }

  // Register schreiben
  nRet=rcuSingleWrite(writeAddress, (0x0));
  if(nRet<0){
    CE_Warning("detectFECs: rcuSingleWrite failed\n");
    ceState=FEE_FAILED;
    return ceState;
  }

  //Errorregister auswerten
  nRet=rcuSingleRead(FECErrReg,&u32RawData);
  if(nRet<0){
    CE_Warning("rcuSingleRead failed\n");
    ceState=FEE_FAILED;
    return ceState;
  }
  
  //Instruction to non active FEC
  if(u32RawData&(0x1<<0)){
    result = -1;
  }

  //No ack from FEC
  if(u32RawData&(0x1<<1)){
    result = -2;
  }

  //ok
  if(u32RawData==(0x00)){
    result = 0;
  }
  return result;
}

/**
 * Checks if the validList matches the hardware.
 * The function checks the valid entry for all FECs
 * which are enabled in the FECActiveList register.
 *
 * return		the errorcode
 * 
*/
int checkValidFECs(){
  int ceState=CE_OK;
  int nRet=CE_OK;
  unsigned short rnw      = 0x001;   //write
  unsigned short bcast    = 0x000;   //dont broadcast
  unsigned short branch   = 0x000;   //start at Branch A (0)
  unsigned short FECAdr   = 0x000;   //address of FEC to cycle
  unsigned short BCRegAdr = 0x006;  //read Temperature register
  int* n_FECActiveList = NULL;
  int answer;
  __u32 writeAddress;
  __u32 u32RawData;
  unsigned maxFECAdr = 0;
  unsigned i, cardsactive, cardsvalid, match;
  cardsactive = 0;
  cardsvalid = 0;
  answer = 0;
  match = 0;


  nRet = rcuSingleRead(FECActiveList,&u32RawData);
  if(nRet<0){
    CE_Warning("detectFECs: rcuSingleWrite failed\n");
    ceState=FEE_FAILED;
    return ceState;
  }
    
  for(i=0; i < g_nofFECs; i++){
    //Card is turned on
      
      branch = (i < MAX_BRANCH_A_FEC) ? 0x000 : 0x001;
      FECAdr = (branch == 0x0) ? i : i%MAX_BRANCH_A_FEC;    

      writeAddress = getFecAddress(rnw, bcast, branch, FECAdr, BCRegAdr);
      answer = checkSingleFEC(writeAddress);
      
      switch(answer){
	case -2: //no ack from FEC
		if(~u32RawData&(0x1<<i)){
		  match++;
		}
		if(u32RawData&(0x1<<i)){
		  CE_Warning("FEC %d was turned on but received no Ack from FEC\n",i );
		}	
		break;
	case -1: //instruction to not active card
		if(u32RawData&(0x1<<i)){
		  CE_Error("FEC %d was turned on but received 'Instruction to not active FEC' Error\n",i );
		}
		if(~u32RawData&(0x1<<i)){
		  match++;
		}
		break;
	case 0:  // Card answered
		if(u32RawData&(0x1<<i)){
		  match++; cardsactive++; cardsvalid++;
		}
		if(~u32RawData&(0x1<<i)){
		  CE_Error("Card %d was turned off, but received ack from this FEC.\n",i);
		}
		break;
      }
	CE_Debug("wAdr: 0x%x, brnch: 0x%x , FECAdr: 0x%x, Pos: %d, Ans: %d\n",writeAddress, branch, FECAdr, i, answer);
  }
  if(match != g_nofFECs) 
  CE_Debug("Found %d mismatches.\n", (g_nofFECs - match));
return nRet;
}


/**
 * tries to detect all working FECs by turning all the cards on, and then trying to read a register from each card. 
 * By this the list of valid cards is created. The status of the AFL register (RCU) is restored after the test.
 * 
 * @return	nRet, the errorcode
 * 
*/
int detectFECs(){ 
  int ceState=CE_OK;
  int nRet=CE_OK;
  unsigned short rnw      = 0x001;   //write
  unsigned short bcast    = 0x000;   //dont broadcast
  unsigned short branch   = 0x000;   //start at Branch A (0)
  unsigned short FECAdr   = 0x000;   //address of FEC to cycle
  unsigned short BCRegAdr = 0x006;  //write Temperature register
  __u32 u32RawData, writeAddress;
  unsigned maxFECAdr = 0;
  unsigned position = 0;
  int answer, i;
  
  if (g_validList == NULL) {
    CE_Error("detectFECs: valid list array nor initialized\n");
    return FEE_FAILED;
  }

  CE_Info("running detectFECs\n");

/*   //new list for detected FECs */
/*   n_FECActiveList=(int*)malloc(g_nofFECs*sizeof(int)); */
/*   if (n_FECActiveList) { */
/*     memset(n_FECActiveList, 0, g_nofFECs*sizeof(int)); */
/*   } */
/*   g_validList=(int*)malloc(g_nofFECs*sizeof(int)); */
/*   g_FECActiveList=(int*)malloc(g_nofFECs*sizeof(int)); */

  //turn on all cards
  __u32 aflBackup=0;
  nRet = rcuSingleRead(FECActiveList, &aflBackup);
  if(nRet<0){
    CE_Warning("detectFECs: rcuSingleRead failed can not read AFL\n");
    ceState=FEE_FAILED;
    return ceState;
  }

  nRet = rcuSingleWrite(FECActiveList, (0xFFFFFFFF));
  if(nRet<0){
    CE_Warning("detectFECs: rcuSingleWrite failed\n");
    ceState=FEE_FAILED;
    return ceState;
  }

  //Wait for the Boardcontrollers to come up
  usleep(300000);

  for(branch = 0x000; branch < NOF_BRANCHES; branch++){

    maxFECAdr = (branch == 0x0) ? g_nofFEC_A : g_nofFEC_B;
    //Loop through the FEC addresses
    for(FECAdr = 0x000; FECAdr < maxFECAdr; FECAdr++){
		
	//what position are we in the AFL?
	position = (branch == 0x0) ? FECAdr : FECAdr + MAX_BRANCH_A_FEC;

      	writeAddress = getFecAddress(rnw, bcast, branch, FECAdr, BCRegAdr);

        answer = checkSingleFEC(writeAddress);
	
	if (position<g_nofFECs) {	
	switch(answer){
	  case -2://no ack from FEC
		  //CE_Debug("Case -2\n");
		  g_validList[position] = 0;
		  break;
	  case -1://command to not active FEC
		  //CE_Debug("Case -1\n");
		  g_validList[position] = 0;
		  break;
	  case 0://no error
		  //CE_Debug("Case 0\n");
		  g_validList[position] = 1;
		  break;
	}
	CE_Debug("wAdr: 0x%x, brnch: 0x%x , FECAdr: 0x%x, Pos: %d, Ans: %d\n",writeAddress, branch, FECAdr, position, answer);
	} else {
	  CE_Error("position %d out of valid list array (size %d)\n", position, g_nofFECs);
	}
    }//FECAdr
  }//Branches

/*   for(i = 0; i < g_nofFECs; i++){ */
/*     //CE_Debug("run:%i, value: %i\n",i,n_FECActiveList[i]); */
/*     g_FECActiveList[i] = n_FECActiveList[i]; */
/*     g_validList[i] = n_FECActiveList[i];   */
/*   } */

  // restore the original value of the AFL
  nRet = rcuSingleWrite(FECActiveList, aflBackup);
  if(nRet<0){
    CE_Warning("detectFECs: can not restore AFL\n");
    ceState=FEE_FAILED;
    return ceState;
  }

return nRet;
}

//write something to a FEC
int setFECCSR0(){
  int ceState=CE_OK;
  int nRet=CE_OK;
  unsigned short unused   = 0x000;
  unsigned short base     = 0x00C;
  unsigned short rnw      = 0x000;
  unsigned short bcast    = 0x001;
  unsigned short branch   = 0x000;
  unsigned short FECAdr   = 0x000;
  unsigned short BCRegAdr = FECCSR0;
  extern CErcu g_RCU;
  CEState states[]={eStateOn, eStateConfiguring, eStateConfigured, eStateRunning, eStateUnknown};
  if (g_RCU.Check(states)) {
    __u32 writeAddress = (unused<<16) | (base<<12) | (rnw<<11) | (bcast<<10) | (branch<<9) | (FECAdr<<5) | (BCRegAdr<<0);
    __u32 u32RawData;
    nRet=rcuSingleWrite(writeAddress,0x7FF);
    if(nRet<0){
      CE_Warning("rcuSingleWrite failed\n");
      ceState=FEE_FAILED;
      return ceState;
    }
    nRet=rcuSingleRead(FECErrReg,&u32RawData);
    if(nRet<0){
      CE_Warning("rcuSingleRead failed\n");
      ceState=FEE_FAILED;
      return ceState;
    }
    if(u32RawData&(0x1<<0)){
      CE_Warning("instruction to not active FEC\n");
      ceState=FEE_FAILED;
      return ceState;
    }
    if(u32RawData&(0x1<<1)){
      CE_Warning("no acknowledge from FEC\n");
      ceState=FEE_FAILED;
      return ceState;
    }
  }
  return ceState;
}

__u32* cnvAllExtEnvI(char** values){
  int i = 0;
  int arrSize = 0;
  char* pEnd=0;
  __u32* retVal=0;
  if(values==NULL){
    return NULL;
  }
  arrSize = (sizeof values/sizeof values[0]);
  retVal = (__u32*)malloc(arrSize*sizeof(__u32*));
  if (retVal == NULL) {
    CE_Error("no memory available\n");
    fflush(stdout);
    return NULL;
  }
  for(i=0; 0<arrSize; i++){
    if(values[i]!=NULL){
      retVal[i] = strtoul(values[i],&pEnd,0);
    }
    else{
      retVal[i]=0;
    }
  }
  free(pEnd);
  return retVal;
}

float* cnvAllExtEnvF(char** values){
  int i=0;
  int arrSize=0;
  float* retVal=0;
  if(values==NULL){
    return NULL;
  }
  arrSize = (sizeof values/sizeof values[0]);
  retVal = (float*)malloc(arrSize*sizeof(float*));
  if (retVal == NULL) {
    CE_Error("no memory available\n");
    fflush(stdout);
    return NULL;
  }
  for(i=0; 0<arrSize; i++){
    if(values[i]!=NULL){
      retVal[i] = atof(values[i]);
    }
    else{
      retVal[i]=0;
    }
  }
  return retVal;
}

//__u32 cnvExtEnv(char* var){
//  __u32 retVal = 0;
//  unsigned char i=0;
//  while(var!= 0 && var[i]!='\0' && i<g_nofFECs && (var[i]=='0' || var[i]=='1')){
//    retVal += var[i]*pow(2,i);
//    i++;
//  }
//  return retVal;
//}

char** splitString(char* string){
  char** retVal = 0;
  __u32 nextPos = 0;
  __u32 curPos  = 0;
  unsigned short int nSeg = 1;
  unsigned char i = 0;
  if(string==NULL){
    return retVal;
  }
  while (string[i]!='\0'){
    if(string[i]==','){
      nSeg++;
    }
    i++;
  }
  retVal = (char**) malloc(nSeg*sizeof(char*));
  for(i=0;i<nSeg;i++){
    curPos = nextPos;
    if(i==nSeg-1){
      nextPos = (__u32)strchr(string+curPos, '\0');
    }
    else{
      nextPos = (__u32)strchr(string+curPos, ',');
    }
    retVal[i] = (char*) malloc((nextPos-curPos)*sizeof(char));
    strncpy (retVal[i],string+curPos+1,nextPos-curPos-1);
    retVal[i][nextPos-curPos]='\0';
  }
  return retVal;
}

int RCUisFECvalid(int i) {
  int iResult=0;
  if (g_validList) {
    if (i<g_nofFECs) {
      iResult=g_validList[i]==1;
    } else {
      CE_Error("RCUisFECvalid: invalid FEC id\n");
      iResult=-EINVAL;
    }
  } else if (g_nofFECs>0) {
    CE_Error("RCUisFECvalid: list uninitialized\n");
  } else  {
    CE_Warning("RCUisFECvalid: no Front-End Cards defined\n");
  }
  return iResult;
}

int RCUisFECactive(int i) {
  int iResult=0;
  if (g_FECActiveList) {
    if (i<g_nofFECs) {
      iResult=g_FECActiveList[i]==1;
    } else {
      CE_Error("RCUisFECactive: invalid FEC id\n");
      iResult=-EINVAL;
    }
  } else if (g_nofFECs>0) {
    CE_Error("RCUisFECactive: list uninitialized\n");
  } else  {
    CE_Warning("RCUisFECactive: no Front-End Cards defined\n");
  }
  return iResult;
}

