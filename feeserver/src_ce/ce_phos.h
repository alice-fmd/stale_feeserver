// $Id: ce_phos.h,v 1.1.1.2 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/
#ifndef __CE_PHOS_H
#define __CE_PHOS_H

#include <linux/types.h>   // for the __u32 type

/**
 * @defgroup rcu_ce_phos PHOS specific modules of the CE
 * The group is intended to contain all PHOS specific implementations. 
 * The PHOS module must be enabled by the configure option <b>--enable-phos</b>. 
 * Dependend on the activated module, the number of FECs per branch is defined.
 *
 * @ingroup feesrv_ce
 */

#ifdef PHOS
/**
 * Number of FECs in branch A.
 * @ingroup rcu_ce_phos
 */
#define NOF_FEC_BRANCH_A 14
/**
 * Number of FECs in branch B.
 * @ingroup rcu_ce_phos
 */
#define NOF_FEC_BRANCH_B 14
/**
 * Number of APD channels per FEC
 * @ingroup rcu_ce_phos
 */
#define numberOfAPDchannels 32

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialization handler.
 * called during initialization of the CE
 * @ingroup rcu_ce_phos
 */
int PHOSinitializeCE();

/**
 * Destruction handler.
 * called at cleanup of the CE
 * @ingroup rcu_ce_phos
 */
int PHOScleanUpCE();

#ifdef __cplusplus
}
#endif

#endif //PHOS

#endif //__CE_PHOS_H
