// $Id: ce_tpc.h,v 1.1.1.2 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter,
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/
#ifndef __CE_TPC_H
#define __CE_TPC_H

#include <linux/types.h>   // for the __u32 type
/**
 * @defgroup rcu_ce_tpc TPC specific modules of the CE
 * The group is intended to contain all TPC specific implementations. Currently,
 * there are none, since everything can be related to the RCU funtionality.
 * The module is defined as an example for the development of specific code
 * for other detectors.<br>
 * The TPC module must be enabled by the configure option <b>--enable-tpc</b>. 
 * Dependend on the activated module, the number of FECs per branch is defined.
 *
 * @author Matthias Richter
 * @ingroup feesrv_ce
 */

#ifdef TPC
/**
 * Maximum FECs in branch A.
 * some of the RCUs have less FECs, total 25 is the maximum
 * @ingroup rcu_ce_tpc
 */
#define NOF_FEC_BRANCH_A 13
/**
 * Maximum FECs in branch B.
 * @ingroup rcu_ce_tpc
 */
#define NOF_FEC_BRANCH_B 12

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialization handler.
 * called during initialization of the CE
 * @ingroup rcu_ce_tpc
 */
int TPCinitializeCE();

/**
 * Destruction handler.
 * called at cleanup of the CE
 * @ingroup rcu_ce_tpc
 */
int TPCcleanUpCE();

#ifdef __cplusplus
}
#endif

#endif //TPC

#endif //__CE_TPC_H
