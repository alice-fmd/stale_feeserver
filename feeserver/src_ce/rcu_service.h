// $Id: rcu_service.h,v 1.1.1.2 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter and Dag T Larsen
** Please report bugs to Matthias.Richter@ift.uib.no, dagtl@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#ifndef __RCU_SERVICE_H
#define __RCU_SERVICE_H

#include <linux/types.h>   // for the __u32 type

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup rcu_service Service handling for the RCU FeeServer
 * This group describes service handling methods of the RCU FeeServer.
 * Standard services for the RCU and the attached FECs are defined in rcu_service.h,   
 * implemented in rcu_service.c. <br>
 * The RCU provides access to the attached Front-end cards and the control engine publishes
 * the corresponding services. The access is carried out by the Monitoring and Safety Module
 * of the RCU firmware.<br>
 *
 * Services:
 * <pre>
 * The standard services per FEC are:
 * =============================================================================================
 *
 * def.conf    name      description                    deadband  bitwidth conversion  unit
 * ---------------------------------------------------------------------------------------------
 *   on      "T_TH"    temperature threshold              0.5        10      0.25       oC
 *   on      "AV_TH"   analog voltage threshold           0.5        10      0.0043     V
 *   on      "AC_TH"   analog current threshold           0.5        10      0.017      A
 *   on      "DV_TH"   digital voltage threshold          0.5        10      0.0043     V
 *   on      "DC_TH"   digital current threshold          0.5        10      0.03       A
 *   on      "TEMP"    temperature                        0.5        10      0.25       oC
 *   on      "AV"      analog voltage                     0.5        10      0.0043     V
 *   on      "AC"      analog current                     0.5        10      0.017      A
 *   on      "DV"      digital voltage                    0.5        10      0.0043     V
 *   on      "DC"      digital current                    0.5        10      0.03       A
 *   off     "L1CNT"   level 1 trigger count              0.5        16      1.0
 *   off     "L2CNT"   level 2 trigger count              0.5        16      1.0
 *   off     "SCLKCNT"                                    0.5        16      1.0
 *   off     "DSTBCNT"                                    0.5         8      1.0
 *   off     "TSMWORD"                                    0.5         9      1.0
 *   off     "USRATIO"                                    0.5        16      1.0
 *   off     "CSR0"    BC control & status register 0     0.5        11      1.0
 *   off     "CSR1"    BC control & status register 1     0.5        14      1.0
 *   off     "CSR2"    BC control & status register 2     0.5        16      1.0
 *   off     "CSR3"    BC control & status register 3     0.5        16      1.0
 * </pre>
 *
 * A service with state 'no link' (FEC inactive, no access) is set to -2000.<br>
 * 
 * The name of the service is derived from the number of the FEC and the service base name,
 * like 05_TEMP 
 *
 * @author Matthias Richter
 * @ingroup feesrv_ce
*/

/**
 * the maximum length of a service name.
 * The define denotes the default length of service names. Used inside rcu_service, there is
 * no restriction within the @ref rcu_ce_base_services module.
 * @ingroup rcu_service
 */
#  define MAX_SERVICE_NAME_LEN 15

/**
 * @name Branch layout
 * @ingroup rcu_service
 */

/**
 * An RCU can serve at max 32 FECs grouped into two branches
 * @ingroup rcu_service
 */
#  define NOF_BRANCHES     2
/**
 * The maximum number of FECs in branch A. The actual number is defined by 
 * @ref NOF_FEC_BRANCH_A in the detector specific CE header file. 
 * @see ce_tpc.h, ce_phos.h
 * @ingroup rcu_service
 */
#  define MAX_BRANCH_A_FEC 16
/**
 * The maximum number of FECs in branch B. The actual number is defined by 
 * @ref NOF_FEC_BRANCH_B in the detector specific CE header file. 
 * @see ce_tpc.h, ce_phos.h
 * @ingroup rcu_service
 */
#  define MAX_BRANCH_B_FEC 16
/**
 * The maximum total number of FECs.
 * @ingroup rcu_service
 */
#  define MAX_NOF_FEC      MAX_BRANCH_A_FEC+MAX_BRANCH_B_FEC

/**
 * The default number of FECs.
 * The number of Front-end cards is set by the detector specific CE with the defines
 * - @ref NOF_FEC_BRANCH_A
 * - @ref NOF_FEC_BRANCH_B
 *
 * @see ce_tpc.h, ce_phos.h, ... for the values
 *
 * If none of the above defines is set the default value of @ref NOF_FEC_DEFAULT is taken.<br>
 * @note Always make sure that the include file for the detector is placed before this file, otherwise 
 * you get a conflict at the customized detector definition. If no detector specific CE is enabled,
 * the fall back is zero FECs.
 * @ingroup rcu_service
 *
*/
#  define NOF_FEC_DEFAULT  0

/**
 * @name Helper macros to convert between local (within a branch) and global addressing of a FEC.
 * @ingroup rcu_service
 */

/**
 * Convert Branch A FEC address to global address
 * @ingroup rcu_service
 */
#  define FEC_BRANCH_A_TO_GLOBAL(x) x
/**
 * Convert Branch B FEC address to global address
 * @ingroup rcu_service
 */
#  define FEC_BRANCH_B_TO_GLOBAL(x) MAX_BRANCH_A_FEC + x
/**
 * Convert global address to Branch A FEC address
 * returns -1 if the global address is outside branch A
 * @ingroup rcu_service
 */
#  define FEC_GLOBAL_TO_BRANCH_A(x) (x>=0 && x<MAX_BRANCH_A_FEC)?x:-1
/**
 * Convert global address to Branch B FEC address
 * returns -1 if the global address is outside branch B
 * @ingroup rcu_service
 */
#  define FEC_GLOBAL_TO_BRANCH_B(x) (x>=MAX_BRANCH_A_FEC && x<MAX_NOF_FEC)?x-MAX_BRANCH_A_FEC:-1

/*   
   The actual configuration of services and FECs is defined by a couple of environment variables:
   FEESERVER_FEC_MONITOR   determines the valid FECs, its a string of '0' and '1', missing entries
                           are set to 0, e.g. '01001' puts FEC 2 and 5 into the configuration
   FEESERVER_FECSRV_ENABLE override the default FEC service configuration, comma separated list of names 
   FEESERVER_FECSRV_ADDEN  add FEC services to the default service configuration, comma separated list of names 
   FEESERVER_SERVICE_ADR   list of addresses in the RCU going to be published as services
   FEESERVER_SERVICE_NAME  list of the service names for the above RCU locations
   FEESERVER_SERVICE_DEAD  list of the default deadbands for the above RCU locations

   States:
   =============================================================================================
   The RCU publishes 1 channel for its own state and a state channel for each FEC
   The states are defined in rcu_state.h

   RCU_STATE name of the RCU channel
   xx_STATE  name of the FEC #xx channel
 */


/******************************************************************************************/

/**
 * @name General API methods
 * @ingroup rcu_service
 */

/**
 * CE Init function for the RCU.
 * is called during initializeCE before the initialization of the detector
 * specific CE
 * @ingroup rcu_service
 */
int RCUinitializeCE();

/**
 * Cleanup function for the RCU.
 * called during cleanUp after the clean up of the detector
 * specific CE.
 * @ingroup rcu_service
 */
int RCUcleanUpCE();

/**
 * Enable the RCU access.
 * After each reset of the RCU, the DDL mezzanine card has by the default the bus 
 * control. An arbitrary access to address 0xe000 in RCU address space enables the
 * bud control for the DCS board.
 * @ingroup rcu_service
 */
int enableControl();

/**
 * Enable the Slow Control measurements.
 * The <i>Monitoring and Safety</i> module of the RCU firmware is set up to start
 * continuous monitoring. A broadcast to all FEC Board Controllers is sent to set the
 * CSR0 of the BC to 0x7FF.<br>
 * The action is a write access to address 0xc411 in RCU address space.
 * @ingroup rcu_service
 */
int setFECCSR0();

/**
 * Read the FEESERVER_FEC_MONITOR environment variable and enable the 
 * service handling for the corresponding FECs.
 * <b>Note:</b>There is a difference between the states <i>valid</i> and <i>active</i>:
 * - <i>valid</i> is set at startup according to the FeeServer setup specified by the
 * FEESERVER_FEC_MONITOR variable.
 * - <i>active</i> is synchronized to the AFL (active FEC list) before each update cycle. 
 * @return 0 if successful, <0 neg. error code
 * @ingroup rcu_service
 */
int readFECValidList();

/**
 * check whether a FEC is valid or not.
 * @param i index of a FEC (global addressing)
 * @return 1 if valid, 0 i not, <0 neg. error code
 * @ingroup rcu_service
 */
int RCUisFECvalid(int i);


/******************************************************************************************/

/**
 * @name Service publishing
 * @ingroup rcu_service
 */

/**
 * publish all services for the FECs
 * the services are registered/published for all valid FECs, the valid FECs
 * are determined by the environment variable FEESERVER_FEC_MONITOR
 * @ingroup rcu_service
 */
int publishServices();

/**
 * publish a certain service
 * @param FEC Front-en card index (global addressing)
 * @param reg service no
 * @ingroup rcu_service
 */
int publishService(int FEC, int reg);

/**
 * publish additional registers for the RCU itself (RCU memory locations).
 * the registers are defined by the three environment variables 
 * FEESERVER_SERVICE_ADR  list of addresses
 * FEESERVER_SERVICE_NAME list of the service names
 * FEESERVER_SERVICE_DEAD list of the default deadbands
 * the function makeRegItemLists() builds the internal arrays
 * @ingroup rcu_service
 */
int publishRegs();

/**
 * read the environment variables and build the internal arrays for the RCU services.
 * not yet implemented
 * @ingroup rcu_service
 */
int makeRegItemLists();

/* publish a certain register
 */
int publishReg(int regNum);



/******************************************************************************************/

/**
 * @name Service update
 * @ingroup rcu_service
 */

/**
 * read the AFL (active FEC list) from the RCU and set the corresponding flags
 * this function is called at the begin of each update cycle 
 * @ingroup rcu_service
 */
int readFECActiveList();

/**
 * check whether a certain FEC is active
 * be aware that the address is a 'global' address, derived from branch no and FEC no 
 * within the branch. Use the FEC_BRANCH_A_TO_GLOBAL and FEC_BRANCH_B_TO_GLOBAL
 * macros to convert the card no. 
 * @param address  global address of the FEC
 * @ingroup rcu_service
 */
int RCUisFECactive(int address);

/**
 * Update function for the standard FEC services.
 * This function is registered for the FEC services and is called each update loop
 * the <i>FEC</i>, <i>reg</i> and <i>parameter</i> parameters have been set during 
 * registration of the service, two indexes and one void pointer can be used to register 
 * the same update function for different services and to access appropriate registers
 * inside the function.
 * @param pF        pointer to the float variable to receive the updated data
 * @param FEC       index of the FEC
 * @param reg       no of the register
 * @param parameter don't care, optional parameter required by API but not used
 * @ingroup rcu_service
 */
int updateService(float* pF, int FEC, int reg, void* parameter);

/**
 * the update function for standard RCU memory locations
 * this function is registered for the RCU services and is called each update loop
 * @param pF        pointer to the float variable to receive the updated data
 * @param regNo     index of the register
 * @param dummy     don't care, required by API but not used
 * @param parameter don't care, optional parameter required by API but not used
 * @ingroup rcu_service
 */
int updateReg(float* pF, int regNum, int dummy, void* parameter);

/**
 */
int setValidFECs(__u32 bitfield);                // set the g_validList of the service handling

/******************************************************************************************/

/**
 * @name Internal methods
 * @ingroup rcu_service
 */

/**
 * 
 * @ingroup rcu_service
 */
int readExtEnv();

/**
 * 
 * @ingroup rcu_service
 */
__u32* cnvAllExtEnvI(char** values);

/**
 * 
 * @ingroup rcu_service
 */
float* cnvAllExtEnvF(char** values);

/**
 * 
 * @ingroup rcu_service
 */
//__u32 cnvExtEnv(char* var);

/**
 * 
 * @ingroup rcu_service
 */
char** splitString(char* string);

#ifdef __cplusplus
}
#endif

#endif //__RCU_SERVICE_H
