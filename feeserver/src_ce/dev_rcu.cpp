// $Id: dev_rcu.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include "dev_rcu.hpp"
#include "ce_base.h"
#include "dcscMsgBufferInterface.h"

#ifdef RCU

CErcu::CErcu()
  : CEDimDevice("RCU")
{
  fFwVersion=-1;
}

CErcu::~CErcu() 
{
}

CEState CErcu::EvaluateHardware() 
{
  int iResult=0;
  CEState state=eStateUnknown;
  if (GetCurrentState()==eStateUnknown) {
    if (fFwVersion==0) {
      /* this is the case for invalid FW version number and no access to
       * MSM registers while the DDL SIU is Altro Bus Master (ABM)
       * -> go directly to state CONFIGURED with DCS board as ABM
       */
      state=eStateConfigured;
    } else if (fFwVersion>=1) {
      /* all other cases where the DDL SIU is the default ABM
       */
      state=eStateOn;
    } else {
      CE_Info("RCU hardware access not established, setting to state OFF\n");
      state=eStateOff;
    }
  }
  return state;
}

int CErcu::ArmorDevice()
{
  int iResult=0;
#ifndef RCUDUMMY
  iResult=EvaluateFirmwareVersion();
  if (iResult>=0) {
    fFwVersion=iResult;
  } else {
    // the state machine should not go to ERROR state, better to OFF during
    // EvaluateHardware
    iResult=0;
  }
#else // RCUDUMMY
  fFwVersion=1;
#endif //RCUDUMMY
  if ((iResult=ReadFECValidList())<0) {
  }
  if ((iResult=MakeRegItemLists())<0) {
  }
  if ((iResult=PublishServices())<0) {
  }
  if ((iResult=PublishRegs())<0) {
  }
  return iResult;
}

// int CErcu::IsAllowedTransition(CETransition* pT) 
// {
//  int iResult=0;
//  return iResult;
// }

int CErcu::EnterStateOFF() 
{
  int iResult=0;
  // think about turning the voltage regulator off and disable
  // msgBuffer (rcu bus)
  return iResult;
}

int CErcu::LeaveStateOFF() 
{
  int iResult=0;
  // think about turning the voltage regulator on and enable
  // msgBuffer (rcu bus)
  return iResult;
}

int CErcu::EnterStateON() 
{
  int iResult=0;
  CE_Debug("enter state ON for CErcu, setting DDL SIU as Altro Bus Master\n");
  iResult=SetAltroBusMaster(eAbmDDLsiu);
  return iResult;
}

int CErcu::LeaveStateON() 
{
  int iResult=0;
  return iResult;
}

int CErcu::EnterStateCONFIGURED() 
{
  int iResult=0;
  CE_Debug("enter state CONFIGURED for CErcu, setting DCS board as Altro Bus Master\n");
  iResult=SetAltroBusMaster(eAbmDCSboard);
  return iResult;
}

int CErcu::LeaveStateCONFIGURED() 
{
  int iResult=0;
  return iResult;
}

int CErcu::EnterStateRUNNING() 
{
  int iResult=0;
  CE_Debug("enter state RUNNING for CErcu, setting DDL SIU as Altro Bus Master\n");
  iResult=SetAltroBusMaster(eAbmDDLsiu);
  return iResult;
}

int CErcu::LeaveStateRUNNING() 
{
  int iResult=0;
  return iResult;
}

int CErcu::SwitchOn(int iParam, void* pParam) 
{
  int iResult=0;
  return iResult;
} 

int CErcu::Shutdown(int iParam, void* pParam) 
{
  int iResult=0;
  return iResult;
} 

int CErcu::Configure(int iParam, void* pParam) 
{
  int iResult=0;
  CE_Debug("configure CErcu, setting DCS board as Altro Bus Master\n");
  iResult=SetAltroBusMaster(eAbmDCSboard);
  return iResult;
} 

int CErcu::Reset(int iParam, void* pParam) 
{
  int iResult=0;
  return iResult;
} 

int CErcu::Start(int iParam, void* pParam) 
{
  int iResult=0;
  return iResult;
} 

int CErcu::Stop(int iParam, void* pParam) 
{
  int iResult=0;
  return iResult;
}

int CErcu::SetAltroBusMaster(abm_t master) 
{
  int iResult=0;
#ifndef RCUDUMMY
  if (master==eAbmDDLsiu || master==eAbmDCSboard) {
    if ((iResult=rcuSingleWrite(master, 0x0))<0) {
      CE_Error("can not set Altro Bus Master: error %d\n", iResult);
    }
  } else {
    CE_Error("ivalid parameter, I don't know about Altro Bus Master %#x\n", master);
    iResult=-EINVAL;
  }
#endif //RCUDUMMY
  return iResult;
}

/******************************************************************************
 ********************                                     *********************
 ********************       global device handling        *********************
 ********************                                     *********************
 *****************************************************************************/

/**
 * There is only one RCU per FeeServer, so we make it global.
 * The device is treated as sub-device to @ref RCUControlEngine. 
 */
CErcu g_RCU;

int translateRcuCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb) 
{
  return g_RCU.TranslateRcuCommand(cmd, parameter, pData, iDataSize, rb);
}

#endif //RCU
