// $Id: dev_rcu.hpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#ifndef __DEV_RCU_HPP
#define __DEV_RCU_HPP

#include "dimdevice.hpp"
#include "codebook_rcu.h"

/**
 * @class CErcu
 * Device implementation of the RCU.
 * This is the main device for the RCU. It controls the status of the RCU, the
 * Altro Bus Master setting, ...
 * <br>
 * The device follows the standard States and Transitions of the @ref CEStateEngine:<br>
 * <b>OFF:</b>
 * - no access to the RCU
 * - RCU bus disabled
 * <br>
 *
 * <b>ON:</b>
 * - DDL SIU is AltroBus Master (ABM)
 * <br>
 *
 * <b>CONFIGURING:</b>
 * - not yet decided whether this is needed or not
 * - DCS board is AltroBus Master (ABM)
 * - configuration of RCU behavior in progress
 * <br>
 *
 * <b>CONFIGURED:</b>
 * - DCS board is AltroBus Master (ABM)
 * <br>
 *
 * <b>RUNNING:</b>
 * - start the RCU sequencer and wait until it is finished
 * - timeout @ref RCU_SEQUENCER_TIMEOUT in usec
 *
 *
 * @ingroup rcu_ce
 */
class CErcu : public CEDimDevice {
public:
#ifdef RCU
  CErcu();
  ~CErcu();

private:
  /**
   * Evaluate the state of the hardware.
   * The function does not change any internal data, it performes a number of evaluation
   * operations.
   * @return             state
   */
  CEState EvaluateHardware();

  /**
   * Internal function called during the @ref CEStateMachine::Armor procedure.
   * The function is called from the @ref CEStateMachine base class to carry out
   * device specific start-up tasks.
   */
  int ArmorDevice();

  /**
   * Check if the transition is allowed for the current state.
   * This overrides the @ref CEStateMachine::IsAllowedTransition method in order
   * to change the default behavior.
   * @param transition   pointer to transition descriptor
   * @return             1 if yes, 0 if not, neg. error code if failed
   */
  //int IsAllowedTransition(CETransition* pT);

  /******************************************************************************
   ********************                                     *********************
   ********************    state and transition handlers    *********************
   ********************                                     *********************
   *****************************************************************************/

  /**
   * Handler called when state machine changes to state OFF
   * The function can be implemented to execute specific functions when entering
   * state OFF.
   * @return            neg. error code if failed
   */

  int EnterStateOFF();

  /**
   * Handler called when state machine leaves state OFF
   * The function can be implemented to execute specific functions when leaving
   * state OFF.
   * @return            neg. error code if failed
   */
  int LeaveStateOFF();

  /**
   * Handler called when state machine changes to state ON
   * The function can be implemented to execute specific functions when entering
   * state ON.
   * @return            neg. error code if failed
   */
  int EnterStateON();

  /**
   * Handler called when state machine leaves state ON
   * The function can be implemented to execute specific functions when leaving
   * state ON.
   * @return            neg. error code if failed
   */
  int LeaveStateON();

  /**
   * Handler called when state machine changes to state CONFIGURED
   * The function can be implemented to execute specific functions when entering
   * state CONFIGURED.
   * @return            neg. error code if failed
   */
  int EnterStateCONFIGURED();

  /**
   * Handler called when state machine leaves state CONFIGURED
   * The function can be implemented to execute specific functions when leaving
   * state CONFIGURED.
   * @return            neg. error code if failed
   */
  int LeaveStateCONFIGURED();

  /**
   * Handler called when state machine changes to state RUNNING
   * The function can be implemented to execute specific functions when entering
   * state RUNNING.
   * @return            neg. error code if failed
   */
  int EnterStateRUNNING();

  /**
   * Handler called when state machine leaves state RUNNING
   * The function can be implemented to execute specific functions when leaving
   * state RUNNING.
   * @return            neg. error code if failed
   */
  int LeaveStateRUNNING();

  /**
   * The handler for the <i>switchon</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int SwitchOn(int iParam, void* pParam); 

  /**
   * The handler for the <i>shutdown</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Shutdown(int iParam, void* pParam); 

  /**
   * The handler for the <i>configure</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Configure(int iParam, void* pParam); 

  /**
   * The handler for the <i>reset</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Reset(int iParam, void* pParam); 

  /**
   * The handler for the <i>start</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Start(int iParam, void* pParam); 

  /**
   * The handler for the <i>stop</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Stop(int iParam, void* pParam);

public:
  /******************************************************************************
   ********************                                     *********************
   ********************             API functions           *********************
   ********************                                     *********************
   *****************************************************************************/

  /**
   * Command handling for the RCU command group.
   * @param cmd       command id stripped py the parameter
   * @param parameter the parameter (16 lsb of the command header)
   * @param pData     payload buffer excluding the header
   * @param iDataSize size of the payload buffer in bytes 
   * @param rb        reference to result buffer
   * @return          number of bytes of the payload buffer which have been processed
   *                  neg. error code if failed
   * @ingroup rcu_issue
   */
  int TranslateRcuCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb);

private:
  /******************************************************************************
   ********************                                     *********************
   ********************          internal functions         *********************
   ********************                                     *********************
   *****************************************************************************/

  /**
   * check the rcu error register
   * @result neg. error code if failed
   *         -BUSY        busy bit remains active
   *         -ETIMEDOUT   timeout during FEC access
   *         -EBADF       pattern memory missmatch
   *         -EIO         Altro Bus in error state of Channel HW address missmatch
   *         -EINTR       Sequencer aborted
   */
  int checkRCUError();

  /**
   * start the sequencer to interprete the sequence written to rcu instruction memory
   * @param start       8 bit start address in the RCU instruction memory
   */
  int sendRCUExecCommand(unsigned short start);

  /**
   * send a stop command to the rcu sequencer
   */
  int sendStopRCUExec();

  /**
   * write data to rcu memory
   * @param pBuffer     data buffer
   * @param iCount      # of words to write
   * @param iWordSize   size of word in byte
   * @param u32BaseAddress address in RCU memory space
   */
  int writeCommandBufferToRCUMem(const char* pBuffer, int iCount, int iWordSize, __u32 u32BaseAddress);

  /**
   * write a data block to the rcu instruction memory
   * the address of the instruction memory is determined by the ALTROInstMEM identifier in codebook_rcu.h
   * the function expects 32 bit data
   * parameters:
   * @param pBuffer     data Buffer
   * @param iSize       size of data buffer in byte
   * @return            # of words written, neg. error code if failed
   */
  //int writeCommandBufferToRCUInstructionMem(const char* pBuffer, int iSize);

  /**
   * write a data block to the rcu pattern memory
   * the address of the pattern memory is determined by the ALTROPatternMEM identifier in codebook_rcu.h
   * the function can handle 8, 16 and 32 bit data, current constraint: the data block must be aligned 
   * to 32 bit, otherwize the last bytes are skipped 
   * handling of 10bit compressed data forseen
   * parameters:
   * @param pBuffer     data Buffer
   * @param iSize       size of data buffer in byte
   * @param iWordSize   bit width of the data
   * @return            # of words written, neg. error code if failed
   */
  //int writeDataBlockToRCUPatternMem(const char* pBuffer, int iSize, int iWordSize);

  /**
   * Write a data block to RCU memory
   * @param pBuffer      data buffer to write
   * @param iSize        size of the buffer in byte
   * @param iNofWords    number of words to write
   * @param iWordSize    size of one word in bit
   * @param tgtAddress   address in RCU memory space
   * @param partSize     size of the corresponding partition in RCU memory space
   * @return number of processed bytes; neg. error code if failed 
   *
   */
  int CErcu::writeDataBlockToRCUMem(const char* pBuffer, int iSize, int iNofWords, int iWordSize,
				    __u32 tgtAddress, int partSize);

  /**
   * Read from result buffer, allocate the target buffer and insert it as the result
   * the buffer will be automatically transferred as result of the issue method to the
   * FeeServer core, and will be relaesed by that.<br>
   * If the size of the partition is provided the function will check whether the partition
   * is large enough. If not, the output is truncated.
   * @param address  address in rcu memory space
   * @param size     # of 32 bit words to read from the memory
   * @param partSize size of the partition in RCU memory space
   * @return:        >0 no of words read, neg. error code if failed
   */
  int readRcuMemoryIntoResultBuffer(__u32 address, int size, int partSize, CEResultBuffer& rb);

  /**
   * Read the FEC configuration from system variable FEESERVER_FEC_MONITOR or
   * probe for FECs if not set.
   * The function is called once at startup
   * format of FEESERVER_FEC_MONITOR: string of subsequent '0' and '1' each indicating 
   * a not valid/valid card missing digets at the and are treated as '0'.
   * The function also creates the global array.
   */
  int ReadFECValidList();

  /*
   * Update the active FEC list from the hardware.
   * Reads the corresponding RCU register and updates the internal array
   * the function also creates the global array if needed.<br>
   * <b>Note:</b> There are two internal lists: the <i>valid</i> list which 
   * is build at startup of the server and indicates all FECs which are valid
   * and have corresponding service channels, and the <i>active</i> list which
   * indicates an <i>actice</i> FEC out of the <i>valid</i> ones. 
   */
  //int ReadFECActiveList();

  /**
   * Read the registers to publish from environment variables.
   * The function was raraly tested.
   */
  int MakeRegItemLists();

  /**
   * Publish services for the FECs.
   * The function is a loop over all valid FECs which calls @ref PublishService.
   */
  int PublishServices();

  /**
   * Publish a single service for a FEC.
   */
  int PublishService(int FEC, int reg);

  /**
   * Publish registers of RCU memory.
   * The function is a loop over all valid FECs which calls @ref PublishReg.
   */
  int PublishRegs();

  /**
   * Publish a single register of the RCU
   */
  int PublishReg(int regNum);

  /**
   * Version ids for RCU firmware
   */
  enum rcu_fw_version {
    eRCUversionUnknown   = 0,
    eRCUversionAncient05,
    eRCUversionMar06,
    eRCUversionMay06
  };

  /**
   * Read the version register of the RCU firmware/ evaluate the version.
   * The functin Reads the version register of the RCU firmware which is available 
   * in newer fw versions. Otherwise it tries to evaluate the version by probing
   * properties. 
   * The register table is loaded according to the fw version. If version avaluation
   * is not possible, always the latest layout is assumed but a warning is given.
   * By this policy, this function adopts to older fw versions if they are known.<br>
   * Currently, the function sets also the altro bus master, this is due to the code
   * conversion policy, we just re-use the old enableControl function. 
   * @return @ref rcu_fw_version, neg. error code if failed
   */
  int EvaluateFirmwareVersion();

public:
  /**
   * Ids for the Altro Bus Master settings
   */
  enum abm_t {
    eAbmUnknown  = 0,
    eAbmDCSboard = CMDDCS_ON,
    eAbmDDLsiu   = CMDSIU_ON
  };

private:
  /**
   * Set the Altro Bus Master
   * The two mezzanine cards of the RCU have both a connection to the Altro Bus, but
   * only one at a time can control it. The DCS board can switch the multiplexer,
   * after Power On the DDL-SIU has by default the access.
   * Older RCU firmware versions does not allow the DCS board te access the registers
   * of the Monitoring and Safety Module (MSM) of the RCU. From May06 this was fixed,
   * and will later be extended to the Sequencer memories as well together with a
   * sequencer scheduler. <br>
   * This method is called according to the state @ref eStateOn and 
   * @ref eStateConfigured.
   * @return @ref abm_t, neg. error code if failed
   */
  int SetAltroBusMaster(abm_t master);

  /** firmware version */
  int fFwVersion;
#endif //RCU
};

#endif //__DEV_RCU_HPP
