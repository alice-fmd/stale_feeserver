// $Id: RCU_ControlEngine.hpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#ifndef __RCU_CONTROLENGINE_HPP
#define __RCU_CONTROLENGINE_HPP

#include "dimdevice.hpp"

class CErcu;
/**
 * @defgroup rcu_ce The ControlEngine for the RCU
 * @ingroup feesrv_ce
 */

/**
 * @class RCUControlEngine
 * The main class for the RCU ControlEngine.
 * This is the main device for the RCU FeeServer. It's a plain software
 * device meaning there is no direct hardware underlying. It controles hardware
 * devices like the @class CErcu and @class FrontEndCard. The states and
 * transitions of this devices are known to the control system in the upper
 * layers. The states are published via a DIM channel and mirrord by a state
 * machine in the SCADA system.
 * <br>
 * States and Transitions:<br>
 * <b>IDLE:</b>
 * - FECs off
 * - DDL SIU is default AltroBus Master (ABM)
 * - ABM can be set by the @ref RCU_SET_ABM command 
 * - Msg Buffer Driver unlocked (other application can alter the hardware status
 *   via the msgBufferInterface)
 * <br>
 *
 * <b>STANDBY:</b>
 * - state of FECs unchanged
 * - DDL SIU is AltroBus Master (ABM)
 * - ABM can be set by the @ref RCU_SET_ABM command 
 * - Msg Buffer Driver unlocked (other application can alter the hardware status
 *   via the msgBufferInterface)
 * <br>
 *
 * <b>CONFIGURING:</b>
 * - state of FECs will be changed according to configuration
 * - DCS board is AltroBus Master (ABM)
 * - Msg Buffer Driver locked (other application dont't have access to the hardware
 * <br>
 *
 * <b>STBY_CONFIGURED:</b>
 * - state of FECs according to configuration
 * - DDL SIU is AltroBus Master (ABM)
 * - Msg Buffer Driver locked (other application dont't have access to the hardware
 * <br>
 *
 * <b>RUNNING:</b>
 * - state of FECs according to configuration
 * - DDL SIU is AltroBus Master (ABM)
 * - Msg Buffer Driver locked (other application dont't have access to the hardware
 * - Trigger enabled
 *
 * <b>Note:</b> The FeeServer evaluates the version of the RCU firmware. If the version
 * does not support access to the MSM registers while the DDL SIU is ABM, the DCS board
 * is set as ABM
 * <pre>
 *
 *      ------
 *     | IDLE | <------------------------------------------- 
 *      ------        |                |                    |
 *        |           ^                |                    |
 *        |           | go_idle        |                    |
 *        |           |                |                    |
 *        |       ---------            |                 -------
 *        |      | STANDBY | <---------+-<- go_standby -| ERROR |
 *        |       ---------    |       |                 -------
 *        |           |        |       |                can be reached by any state
 *        | configure |        |       |                or result of transition	      
 *        |           |        |       |                left by action 'go_standby'
 *      ----------------       |       |                or 'go_idle'
 *     |  CONFIGURING   |      ^       ^   
 *      ----------------       |       |   
 *          ^       |     go_standby   | 
 *          |       |          |       |   
 *      configure   |          |    go_idle 
 *          |       |          |       |  
 *          |  -------------------     |
 *           -|  STBY_CONFIGURED  |----
 *             ------------------- 
 *                   | |         
 *            start  | ^  stop   
 *                   | |         
 *                ---------      
 *               | RUNNING |
 *                ---------
 * </pre>
 *
 * @ingroup rcu_ce
 */
class RCUControlEngine : public CEDimDevice {
public:
#ifdef RCU
  RCUControlEngine();
  ~RCUControlEngine();

private:
  /**
   * Evaluate the state of the hardware.
   * The function does not change any internal data, it performes a number of evaluation
   * operations.
   * @return             state
   */
  CEState EvaluateHardware();

  /**
   * Internal function called during the @ref CEStateMachine::Armor procedure.
   * The function is called from the @ref CEStateMachine base class to carry out
   * device specific start-up tasks.
   */
  int ArmorDevice();

  /**
   * get the name of a default state.
   * Can be overridden in order to change the default name.
   * @param state        the state to check
   * @return             pointer to name
   */
  const char* GetDefaultStateName(CEState state);

  /**
   * get the name of a default transition.
   * Can be overridden in order to change the default name. The name of the transition corresponds
   * one to one to the name of the <i>Action</i> which triggers the transition.
   * @param transition   the id of the transition to check
   * @return             pointer to name
   */
  const char* GetDefaultTransitionName(CETransition* pTransition);

  /**
   * Check if the transition is allowed for the current state.
   * This overrides the @ref CEStateMachine::IsAllowedTransition method in order
   * to change the default behavior.
   * @param transition   pointer to transition descriptor
   * @return             1 if yes, 0 if not, neg. error code if failed
   */
  int IsAllowedTransition(CETransition* pT);

  /**
   * Handler called when state machine changes to state OFF
   * The function can be implemented to execute specific functions when entering
   * state OFF.
   * @return            neg. error code if failed
   */

  int EnterStateOFF();

  /**
   * Handler called when state machine leaves state OFF
   * The function can be implemented to execute specific functions when leaving
   * state OFF.
   * @return            neg. error code if failed
   */
  int LeaveStateOFF();

  /**
   * Handler called when state machine changes to state ON
   * The function can be implemented to execute specific functions when entering
   * state ON.
   * @return            neg. error code if failed
   */
  int EnterStateON();

  /**
   * Handler called when state machine leaves state ON
   * The function can be implemented to execute specific functions when leaving
   * state ON.
   * @return            neg. error code if failed
   */
  int LeaveStateON();

  /**
   * Handler called when state machine changes to state CONFIGURED
   * The function can be implemented to execute specific functions when entering
   * state CONFIGURED.
   * @return            neg. error code if failed
   */
  int EnterStateCONFIGURED();

  /**
   * Handler called when state machine leaves state CONFIGURED
   * The function can be implemented to execute specific functions when leaving
   * state CONFIGURED.
   * @return            neg. error code if failed
   */
  int LeaveStateCONFIGURED();

  /**
   * Handler called when state machine changes to state RUNNING
   * The function can be implemented to execute specific functions when entering
   * state RUNNING.
   * @return            neg. error code if failed
   */
  int EnterStateRUNNING();

  /**
   * Handler called when state machine leaves state RUNNING
   * The function can be implemented to execute specific functions when leaving
   * state RUNNING.
   * @return            neg. error code if failed
   */
  int LeaveStateRUNNING();

  /**
   * The handler for the <i>switchon</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int SwitchOn(int iParam, void* pParam); 

  /**
   * The handler for the <i>shutdown</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Shutdown(int iParam, void* pParam); 

  /**
   * The handler for the <i>configure</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Configure(int iParam, void* pParam); 

  /**
   * The handler for the <i>reset</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Reset(int iParam, void* pParam); 

  /**
   * The handler for the <i>start</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Start(int iParam, void* pParam); 

  /**
   * The handler for the <i>stop</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  int Stop(int iParam, void* pParam);

  /** the RCU sub device */
  CErcu* fpRCU;

#endif //RCU
};

/**
 * @class PVSSStateMapper
 * A mapper class to translate to the PVSS state encoding.
 * @ingroup rcu_ce
 */
class PVSSStateMapper : public CEStateMapper {
public:
  PVSSStateMapper();
  ~PVSSStateMapper();

  /**
   * Action definitions of the PVSS mirror SM
   */
  enum {
    kActionStart         = 16, // Start 
    kActionConfigure     =  6, // Configure
    kActionStop          =  3, // Stop 
    kActionGoStandby     =  2, // GoStandby 
    kActionGoOff         =  1  // GoIdle 
  };

  /**
   * State definitions of the PVSS mirror SM
   */
  enum {
    kStateRunning        = 33, // RUNNING 
    kStateDownloading    = 21, // CONFIGURING 
    kStateError          = 13, // ERROR 
    kStateStandby        =  5, // STANDBY 
    kStateOff            =  4, // IDLE
    kStateStbyConfigured =  3, // STBYCONFIGURED
    kStateElse           =  0, // UNKNOWN
  };

  /**
   * Get the mapped state from the CEState.
   * @param state         CEState
   * @return              translated state
   */
  int GetMappedState(CEState state);

  /**
   * Get the CEState from the mapped state.
   * @param state         CEState
   * @return              translated state
   */
  CEState GetStateFromMappedState(int state);
};
#endif //__RCU_CONTROLENGINE_HPP
