// $Id: dimdevice.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include <cerrno>
#include <cstring>
#include "dimdevice.hpp"
#include "ce_base.h"

using namespace std;

/**
 * Update the value of the 'STATE' service for the specific @ref CEDimDevice.
 * This function is registerd during service registration in
 * @ref CEDimDevice::CreateStateChannel.
 * @param pF         location to update
 * @param major      not used
 * @param minor      not used
 * @param parameter  pointer to device instance
 * @ingroup rcu_service
 */
int updateDimDeviceState(float* pF, int major, int minor, void* parameter) {
  int iResult=0;
  if (pF) {
    if (parameter) {
      *pF=(float)((CEStateMachine*)parameter)->GetTranslatedState();
    } else {
      CE_Error("missing parameter for function updateMainState, check service registration\n");
      iResult=-EFAULT;
    }
  } else {
    CE_Error("invalid location\n");
    iResult=-EINVAL;
  }
  return iResult;
}

/**
 * Update the value of the 'ALARM' service for the @ref CEDimDevice.
 * This function is registerd during service registration in
 * @ref CEDimDevice::CreateAlarmChannel.
 * @param pF         location to update
 * @param major      not used
 * @param minor      not used
 * @param parameter  pointer to device instance
 * @ingroup rcu_service
 */
int updateDimDeviceAlarm(float* pF, int major, int minor, void* parameter) {
  int iResult=0;
  if (parameter) {
    iResult=((CEDimDevice*)parameter)->SendNextAlarm(pF);
  } else {
    CE_Error("missing parameter for function updateMainState, check service registration\n");
    iResult=-EFAULT;
  }
  return iResult;
}

CEDimDevice::CEDimDevice(std::string name) 
  : CEDevice(name)
{
  fLastSentAlarm=0;
}

CEDimDevice::~CEDimDevice() {
}

int CEDimDevice::SendNextAlarm(float* pF) {
  int iResult=0;
  if (pF) {
  } else {
    CE_Error("invalid location\n");
    iResult=-EINVAL;
  }
  return iResult;
}

int CEDimDevice::CreateStateChannel() {
  string name=GetName();
  if (name.length()>0) name+="_";
  name+="STATE";
  return RegisterService((const char*)&name[0], 0.5, updateDimDeviceState, NULL, 0, 0, this);
}

int CEDimDevice::CreateAlarmChannel() {
  string name=GetName();
  if (name.length()>0) name+="_";
  name+="ALARM";
  return RegisterService((const char*)&name[0], 0.5, updateDimDeviceAlarm, NULL, 0, 0, this);
}

int CEDimDevice::SendAlarm(int alarm) {
  return 0;
}

