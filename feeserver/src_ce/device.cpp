// $Id: device.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include <vector>
#include <cerrno>
#include "device.hpp"

CEDevice::CEDevice(std::string name, int id,  CEDevice* pParent, std::string arguments) {
  InitDevice(this, name);
  fId=id;
  fpParent=pParent;
  fArguments=arguments;
}

CEDevice::~CEDevice() {
}

int CEDevice::ArmorDevice() {
  int iResult=0;
  return iResult;
}

CEState CEDevice::EvaluateHardware() {
  CEState state=eStateUnknown;
  return state;
}

int CEDevice::SynchronizeSubDevices() {
  int iResult=0;
  return iResult;
}

int CEDevice::SynchronizeAll() {
  int iResult=0;
  return iResult;
}

int  CEDevice::EnterStateOFF() {
  int iResult=0;
  return iResult;
}

int  CEDevice::LeaveStateOFF() {
  int iResult=0;
  return iResult;
}

int  CEDevice::EnterStateON() {
  int iResult=0;
  return iResult;
}

int  CEDevice::LeaveStateON() {
  int iResult=0;
  return iResult;
}

int  CEDevice::EnterStateCONFIGURED() {
  int iResult=0;
  return iResult;
}

int  CEDevice::LeaveStateCONFIGURED() {
  int iResult=0;
  return iResult;
}

int  CEDevice::EnterStateRUNNING() {
  int iResult=0;
  return iResult;
}

int  CEDevice::LeaveStateRUNNING() {
  int iResult=0;
  return iResult;
}

int CEDevice::SwitchOn(int iParam, void* pParam) {
  int iResult=0;
  return iResult;
} 

int CEDevice::Shutdown(int iParam, void* pParam) {
  int iResult=0;
  return iResult;
} 

int CEDevice::Configure(int iParam, void* pParam) {
  int iResult=0;
  return iResult;
} 

int CEDevice::Reset(int iParam, void* pParam) {
  int iResult=0;
  return iResult;
} 

int CEDevice::Start(int iParam, void* pParam) {
  int iResult=0;
  return iResult;
} 

int CEDevice::Stop(int iParam, void* pParam) {
  int iResult=0;
  return iResult;
}

int CEDevice::AddSubDevice(CEDevice* pDevice) {
  int iResult=0;
  return iResult;
}

int CEDevice::CleanupSubDevies() {
  int iResult=0;
  return iResult;
}
