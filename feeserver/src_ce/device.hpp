// $Id: device.hpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#ifndef __DEVICE_HPP
#define __DEVICE_HPP

#include <vector>
#include <cerrno>
#include <linux/types.h>   // for the __u32 type
#include "statemachine.hpp"

/**
 * @class CEDevice
 * Base class for devices. It inherits a state machine
 * from @ref CEStateMachine and provides default handlers for transitions and 
 * enter/leave-state events. Those handlers esentially doesn't do enything and can be
 * overloaded by the specific implementation. E.g. the @ref EnterStateOFF function
 * is called whenever the state OFF is entered, but after the specific transition has
 * been executed. The @ref LeaveStateOFF method is called when leaving state OFF before the 
 * transition handler @ref SwitchOn is executed.<br>
 * Refer to @ref CEState and @ref CETransitionId for states and transitions and to @class
 * CEStateMachine for a diagram of the default behavior. Note that the states and 
 * transitions can get other names by overloading @ref CEStateMachine::GetDefaultStateName
 * and @ref CEStateMachine::GetDefaultTransitionName.
 * @ingroup rcu_ce_base
 */
class CEDevice : public CEStateMachine {
public:
  /** constructor */
  CEDevice(std::string name="", int id=0, CEDevice* pParent=NULL, std::string arguments="");

  /** destructor */
  ~CEDevice();

  /**
   * Synchronize all sub-devices.
   * Loops over all sub-devices and calls the @ref CEStateMachine::Synchronize function for
   * each device.
   * @return             >=0 success, neg error code if failed
   */
  int SynchronizeSubDevices();

  /**
   * Synchronize all sub-devices and the device itself afterwards.
   * @return             >=0 success, neg error code if failed
   */
  int SynchronizeAll();

protected:
  /**
   * Get the Id of the device.
   * The id is usually used for faster indexing of sub-devices
   * within the parent device.
   * @return id of the device
   */
  int GetId() {return fId;}
  /**
   * Get the parent device.
   * @return pointer to parent device
   */
  CEDevice* GetParentDevice() {return fpParent;}


  /**
   * Get the initialization arguments.
   */
  std::string& GetArguments() {return fArguments;};

  /**
   * Internal function called during the @ref CEStateMachine::Armor procedure.
   * The function is called from the @ref CEStateMachine base class. It is pure virtual
   * and must be implemented by the derived device to carry out specific start-up tasks.
   */
  virtual int ArmorDevice()=0;

  /**
   * Evaluate the state of the hardware.
   * The function does not change any internal data, it performes a number of evaluation
   * operations.
   * @return             state
   */
  virtual CEState EvaluateHardware();

  /**
   * Handler called when state machine changes to state OFF
   * The function can be implemented to execute specific functions when entering
   * state OFF.
   * @return            neg. error code if failed
   */
  virtual int EnterStateOFF();

  /**
   * Handler called when state machine leaves state OFF
   * The function can be implemented to execute specific functions when leaving
   * state OFF.
   * @return            neg. error code if failed
   */
  virtual int LeaveStateOFF();

  /**
   * Handler called when state machine changes to state ON
   * The function can be implemented to execute specific functions when entering
   * state ON.
   * @return            neg. error code if failed
   */
  virtual int EnterStateON();

  /**
   * Handler called when state machine leaves state ON
   * The function can be implemented to execute specific functions when leaving
   * state ON.
   * @return            neg. error code if failed
   */
  virtual int LeaveStateON();

  /**
   * Handler called when state machine changes to state CONFIGURED
   * The function can be implemented to execute specific functions when entering
   * state CONFIGURED.
   * @return            neg. error code if failed
   */
  virtual int EnterStateCONFIGURED();

  /**
   * Handler called when state machine leaves state CONFIGURED
   * The function can be implemented to execute specific functions when leaving
   * state CONFIGURED.
   * @return            neg. error code if failed
   */
  virtual int LeaveStateCONFIGURED();

  /**
   * Handler called when state machine changes to state RUNNING
   * The function can be implemented to execute specific functions when entering
   * state RUNNING.
   * @return            neg. error code if failed
   */
  virtual int EnterStateRUNNING();

  /**
   * Handler called when state machine leaves state RUNNING
   * The function can be implemented to execute specific functions when leaving
   * state RUNNING.
   * @return            neg. error code if failed
   */
  virtual int LeaveStateRUNNING();

  /**
   * The handler for the <i>switchon</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  virtual int SwitchOn(int iParam, void* pParam); 

  /**
   * The handler for the <i>shutdown</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  virtual int Shutdown(int iParam, void* pParam); 

  /**
   * The handler for the <i>configure</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  virtual int Configure(int iParam, void* pParam); 

  /**
   * The handler for the <i>reset</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  virtual int Reset(int iParam, void* pParam); 

  /**
   * The handler for the <i>start</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  virtual int Start(int iParam, void* pParam); 

  /**
   * The handler for the <i>stop</i> action.
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             >=0 success, neg error code if failed
   */
  virtual int Stop(int iParam, void* pParam);

private:
  /**
   * Add a sub-device to the list.
   * Handling of sub-devices is currently strictly private. That means that all sub-devices
   * have to be created in the class, e.g. constructor.
   * @param pDevice      pointer to device object
   * @return             >=0 success, neg error code if failed
   */
  int AddSubDevice(CEDevice* pDevice);

  /**
   * Delete all sub devices.
   * @return             >=0 success, neg error code if failed
   */
  int CleanupSubDevies();

  /** 
   * Id of the device.
   * The id is used for faster device routing inside the parent device.
   */
  int fId;

  /** the parent device */
  CEDevice* fpParent;

  /** 'Command line' arguments to the device */
  std::string fArguments;

  /**
   * the device list.
   * Currently there is no protection of the list as we assume that there is no dynamic
   * change of the list during operation.
   */
  std::vector<CEDevice*> fSubDevices;

  friend class CEStateMachine;
};

/***************************************************************************/

/**
 * @defgroup rcu_ce_base_issue CE properties
 * Issue handling for the ControlEngine
 * Currently, the dispatcher for the incoming @ref issue request from the 
 * FeeServer is hard-wired. Later one should think about a handler registration
 * scheme like for the services. 
 * @author Matthias Richter
 * @ingroup rcu_ce_base
 */

/**
 * The result buffer for the @ref issue handling.
 * @ingroup rcu_ce_base_issue
 */
typedef std::vector<__u32> CEResultBuffer; 

#endif //__DEVICE_HPP
