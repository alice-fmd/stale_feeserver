#ifndef CE_COMMAND_H
#define CE_COMMAND_H

#include "fee_types.h"

#ifdef __cplusplus
extern "C" {
#endif
/** @defgroup feesrv_ce The FeeServer ControlEngine (CE)
 * The ControlEngine is the abstraction layer for the actual hardware access.
 */

/** @defgroup feesrv_ceapi The ControlEngine API
 * Definition of the API between FeeServer core and ControlEngine
 * @ingroup feesrv_ce
 */

/**
 * Issue function to give a command to the control engine and to receive the
 * result of this command. This must be implemented by the CE, and will be
 * called by the FeeServer. The memory of the command is allocated by the
 * FeeServer and also freed by it, the memory for the result is allocated by
 * the CE, and will be freed by the FeeServer. The SIZE represents, when
 * called, the size of the command; when returning, it should be the size of
 * the result data (both in bytes). As return value an errorCode is offered,
 * 0 stands for everything went all right (FEE_OK).
 *
 * @see fee_errors.h for more details on the error code.
 *
 * @param command pointer to the memory of the command for the CE (allocated
 *			and freed by FeeServer).
 * @param result pointer to a pointer for the CE to tell the FeeServer where
 *			it can find the result data (allocated by CE, freed by FeeServer).
 * @param size when calling it keeps the size of the command (in bytes), when
 *			returning, it should contain the size of the result (in bytes).
 *
 * @return an errorCode is returned, 0 if the call was successful (FEE_OK). More
 *			details in fee_errors.h .
 * @ingroup feesrv_ceapi
 */
int issue(char* command, char** result, int* size);

/**
 * Function to clean up the CE and its components. This function is implemnted
 * by the CE.
 * @ingroup feesrv_ceapi
 */
void cleanUpCE();

/**
 * Creates a log message.
 * Via this function both, the FeeServer and the ControlEngine, can send messages
 * to the upper layers. Depending on the current LogLevel of the FeeServer the
 * provided message will be sent or not. (is implemented by the FeeServer)
 *
 * @param type the event type of this message - the priority / severity
 *				(see fee_loglevels.h for more)
 * @param description the message body HAS TO BE TERMINATED wiht NULL!
 * @param origin who created this message (most likely "FeeServer" or "CE") HAS
 *				TO BE TERMINATED with NULL!
 * @ingroup feesrv_ceapi
 */
void createLogMessage(unsigned int type, char* description, char* origin);

/**
 * Function, that initalizes the control engine and its components.
 * Should be called within a dedicated thread. If items are published, this
 * function is reponsible for updating the corresponding values regularly.
 * This function is implemented by the CE.
 * @ingroup feesrv_ceapi
 */
void initializeCE();

/**
 * Publishes items.
 * Function, called by the Control Engine for each service the DIM-Server
 * should offer. This must be done before the server starts serving (start()).
 * (is implemented by the FeeServer).
 *
 * @param item pointer to the item-struct; contains the value-location, the
 *			value-name and the default dead band for this item.
 *
 * @return FEE_OK, if publishing to server was successful, else an error code
 *				possible errors are: 
 *				FEE_WRONG_STATE -> FeeServer is not in state running
 *				FEE_NULLPOINTER -> given Item contains null pointer, item is 
 *                  not published and not stored in list (but CE can continue)
 *				FEE_INSUFFICIENT_MEMORY -> insufficient memory on board
 *				FEE_ITEM_NAME_EXISTS -> item name already exists, item is
 *					not published and not stored in list (but CE can continue)
 * @ingroup feesrv_ceapi
 */
int publish(Item* item);

/**
 * Sends a signal to the calling thread, that the initialisation of the CE is finished.
 * The thread can/must stay alive afterwards to do other stuff, if needed.
 * (When Items are published, it has to stay alive).
 * (is implemented by the FeeServer)
 *
 * @param ceState tells the FeeServer, if CE has been completely and successfully
 *				initialized, else an errorCode is provided.
 *
 * @see fee_errors.h
 * @ingroup feesrv_ceapi
 */
void signalCEready(int ceState);

/**
 * Function to signal the CE, that a property of the FeeServer has changed, that
 * might be interessting to the ControlEngine. For now, it is only forseen, that
 * the update rate of deadband checks is propagated to the CE. This function
 * provides a FeeProperty, which consists of a flag, telling what kind of 
 * property is signaled, and the value itself.
 *
 * @param prop pointer to the FeeProperty, providing flag and property value
 * @ingroup feesrv_ceapi
 */
void signalFeePropertyChanged(FeeProperty* prop);

/**
 * Triggers an update of the service given by serviceName. This function can
 * be called by the CE and is executed by the FeeServer. This function is only
 * executed, when the FeeServer is in the state RUNNING, else an error is 
 * returned.
 *
 * @param serviceName the name of the service, which shall be updated.
 *
 * @return number of updated clients, if service is available and has been
 * 			updated, else an error code (negative value).
 *
 * @see fee_errors.h
 */
int updateFeeService(char* serviceName);

/**
 * Function to write out a benchmark timestamp
 * If the environmental variable "FEE_BENCHMARK_FILENAME" is specified, the
 * printout is written to this file, if not a log message with the same content
 * is generated, event level MSG_SUCCESS_AUDIT.  
 * This function is only available, if the FeeServer is compiled
 * with the flag __BENCHMARK.
 *
 * @param msg text containing the location, where the benchmark timestamp is
 *				generated. This text is prefixed to the benchmark timestamp.
 *				NOTE: The string has to be NULL terminated.
 */
void createBenchmark(char* msg);

#ifdef __cplusplus
}
#endif

#endif
