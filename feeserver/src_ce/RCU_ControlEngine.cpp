// $Id: RCU_ControlEngine.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include <cerrno>
#include <cstring>
#include "ce_base.h"
#include "RCU_ControlEngine.hpp"
#include "dev_rcu.hpp"
#include "dcscMsgBufferInterface.h" 

using namespace std;

#ifdef RCU

RCUControlEngine::RCUControlEngine()
  : CEDimDevice("MAIN")
{
  fpRCU=NULL;
}

RCUControlEngine::~RCUControlEngine() {
}

CEState RCUControlEngine::EvaluateHardware() {
  CEState state=eStateUnknown;
  if (GetCurrentState()==eStateUnknown) state=eStateOff;
  if (fpRCU) {
    fpRCU->Synchronize();
    if (fpRCU->GetCurrentState()==eStateError) {
      CE_Error("sub-device %s is in ERROR state, switching to ERROR\n", fpRCU->GetName());
      state=eStateError;
    }
  } else {
    CE_Error("I need an RCU sub-device! Seems to be uninitialized\n");
    state=eStateError;
  }
  return state;
}

extern CErcu g_RCU; // dev_rcu.cpp
int RCUControlEngine::ArmorDevice()
{
 int iResult=0;
 // init the msg buffer interface
 // later this will be covered by a separate device
 if ((iResult=initRcuAccess(NULL))>=0) {
   unsigned int dcscDbgOptions=0;
   dcscDbgOptions|=PRINT_RESULT_HUMAN_READABLE;
   setDebugOptions(dcscDbgOptions);
 } else {
   iResult=-EACCES;
   CE_Error("initRcuAccess finished with error code %d\n", iResult);
 }

 // init the rcu sub-device, later this has to be added to the list
 // of sub-devices, but this functionality has to be implemented
 // in the CEdevice class first
 fpRCU=&g_RCU;
 fpRCU->Armor();
 return iResult;
}

const char* RCUControlEngine::GetDefaultStateName(CEState state)
{
  const char* name="UNNAMED"; 
  switch (state) {
  case eStateUnknown:     name="UNKNOWN";         break;
  case eStateOff:         name="IDLE";            break;
  case eStateError:       name="ERROR";           break;
  case eStateFailure:     name="FAILURE";         break;
  case eStateOn:          name="STANDBY";         break;
  case eStateConfiguring: name="DOWNLOADING";     break;
  case eStateConfigured:  name="STBY_CONFIGURED"; break;
  case eStateRunning:     name="RUNNING";         break;
  }
  return name;
}

const char* RCUControlEngine::GetDefaultTransitionName(CETransition* pTransition)
{
  const char* name="unnamed";
  if (pTransition) {
    if (pTransition->IsDefaultTransition()) {
      /* some of the transitions should be renamed in order to have the same
       * name as in the mirroring SCADA implementation.
       */
      switch (pTransition->GetID()) {
      case eShutdown:
	name="Go_Idle";
	break;
      case eReset:
	name="Go_Standby";
	break;
      default:
	name=pTransition->GetName();
      }
    } else {
      CE_Warning("transition with id %d is not a default one\n", pTransition->GetID());
    }
  } else {
    name="invalid";
    CE_Error("invalid argument\n");
  }
  return name;
}

int RCUControlEngine::IsAllowedTransition(CETransition* pT) {
  int iResult=0;
  if (pT) {
    CEState current=GetCurrentState();
    int id=pT->GetID();
    CE_Debug("checking whether transition %d (%s) is allowed for state %d (%s)\n", id, GetDefaultTransitionName(pT), current, GetCurrentStateName());
    switch (id) {
    case eConfigure:
      iResult=(current==eStateOff) || 
	(current==eStateOn) ||
	(current==eStateConfigured);
      break;
    case eSwitchOn:
      // SwitchOn is replaced by GoStandby
      iResult=0;
      break;
    case eReset:
      // add the GoStandby transition for the OFF state
      iResult=(current==eStateOff);
      if (iResult==0) iResult=CEStateMachine::IsAllowedTransition(pT);
      break;
    default:
      // forward to the default method
      iResult=CEStateMachine::IsAllowedTransition(pT);
    }
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

int RCUControlEngine::EnterStateOFF() {
  int iResult=0;
  CE_Debug("RCUControlEngine::EnterStateOFF called\n");
  return iResult;
}

int RCUControlEngine::LeaveStateOFF() {
  int iResult=0;
  CE_Debug("RCUControlEngine::LeaveStateOFF called\n");
  return iResult;
}

int RCUControlEngine::EnterStateON() {
  int iResult=0;
  CE_Debug("RCUControlEngine::EnterStateON called\n");
  // check whether the RCU sub-device is running
  if (fpRCU) {
    CEState blackList[]={eStateError, eStateOff, eStateFailure, eStateUnknown};
    if (fpRCU->Check(blackList)) {
      // try to switch the RCU on if it is in OFF state
      if (fpRCU->Check(eStateOff)) iResult=fpRCU->TriggerTransition(eSwitchOn);
      // try to reset to get the RCU sub-device to ON state
      else iResult=fpRCU->TriggerTransition(eReset);
      if (iResult>=0) {
      } else {
	CE_Error("RCU sub-device is in state %s (%d)\n", fpRCU->GetCurrentStateName(), GetCurrentState());
      }
    }
  }
  return iResult;
}

int RCUControlEngine::LeaveStateON() {
  int iResult=0;
  CE_Debug("RCUControlEngine::LeaveStateON called\n");
  return iResult;
}

int RCUControlEngine::EnterStateCONFIGURED() {
  int iResult=0;
  CE_Debug("RCUControlEngine::EnterStateCONFIGURED called\n");
  if (fpRCU) {
    iResult=fpRCU->TriggerTransition(eConfigureDone);
  }
  return iResult;
}

int RCUControlEngine::LeaveStateCONFIGURED() {
  int iResult=0;
  CE_Debug("RCUControlEngine::LeaveStateCONFIGURED called\n");
  return iResult;
}

int RCUControlEngine::EnterStateRUNNING() {
  int iResult=0;
  CE_Debug("RCUControlEngine::EnterStateRUNNING called\n");
  return iResult;
}

int RCUControlEngine::LeaveStateRUNNING() {
  int iResult=0;
  CE_Debug("RCUControlEngine::LeaveStateRUNNING called\n");
  return iResult;
}

int RCUControlEngine::SwitchOn(int iParam, void* pParam) {
  int iResult=0;
  CE_Debug("RCUControlEngine::SwitchOn called\n");
  return iResult;
} 

int RCUControlEngine::Shutdown(int iParam, void* pParam) {
  int iResult=0;
  CE_Debug("RCUControlEngine::Shutdown called\n");
  if (fpRCU) {
    fpRCU->TriggerTransition(eShutdown);
  }
  return iResult;
} 

int RCUControlEngine::Configure(int iParam, void* pParam) {
  int iResult=0;
  CE_Debug("RCUControlEngine::Configure called\n");
  // check whether the RCU sub-device is running
  if (fpRCU) {
    // try to switch the RCU on if it is in OFF state
    if (fpRCU->Check(eStateOff)) iResult=fpRCU->TriggerTransition(eSwitchOn);
    CEState states[]={eStateOn, eStateConfigured};
    if (fpRCU->Check(states)) {
      // set the RCU to state CONFIGURING
      iResult=fpRCU->TriggerTransition(eConfigure);
    } else {
      CE_Error("RCU sub-device is in state %s (%d)\n", fpRCU->GetCurrentStateName(), fpRCU->GetCurrentState());
      iResult=-ECHILD;
    }
  }
  return iResult;
} 

int RCUControlEngine::Reset(int iParam, void* pParam) {
  int iResult=0;
  CE_Debug("RCUControlEngine::Reset called\n");
  return iResult;
} 

int RCUControlEngine::Start(int iParam, void* pParam) {
  int iResult=0;
  CE_Debug("RCUControlEngine::Start called\n");
  if (fpRCU) {
    iResult=fpRCU->TriggerTransition(eStart);
  }
  return iResult;
} 

int RCUControlEngine::Stop(int iParam, void* pParam) {
  int iResult=0;
  CE_Debug("RCUControlEngine::Stop called\n");
  if (fpRCU) {
    iResult=fpRCU->TriggerTransition(eStop);
  }
  return iResult;
}

/******************************************************************/

/** global instance of the RCU ControlEngine */
RCUControlEngine g_MainCE;
/** global instance of the PVSS state mapper */
PVSSStateMapper  g_PVSSStateMapper;

extern "C" int initializeMainCE() {
  int iResult=0;
  g_MainCE.SetTranslationScheme(&g_PVSSStateMapper);
  g_MainCE.Armor();
  g_MainCE.Synchronize();
  return iResult;
}

/**
 * Main handler of actions received by the commend channel.
 * @param  pAction        string specifying the action
 * @return                neg. error code if failed
 */
int actionHandler(const char* pAction) {
  int iResult=0;
  iResult=g_MainCE.TriggerTransition(pAction);
  return iResult;
}

/**
 * action handler used internally.
 * @param  transition     transition id
 * @return                neg. error code if failed
 */
int actionHandler(CETransitionId transition) {
  int iResult=0;
  iResult=g_MainCE.TriggerTransition(transition);
  return iResult;
}
#endif //RCU

PVSSStateMapper::PVSSStateMapper() {
}

PVSSStateMapper::~PVSSStateMapper() {
}

int PVSSStateMapper::GetMappedState(CEState state) {
  switch (state) {
    case eStateConfiguring: return kStateDownloading;     // DOWNLOADING
    case eStateError:       return kStateError;           // ERROR
    case eStateRunning:     return kStateRunning;         // RUNNING 
    case eStateOn:          return kStateStandby;         // STANDBY
    case eStateOff:         return kStateOff;             // IDLE
    case eStateConfigured:  return kStateStbyConfigured;  // STBY_CONFIGURED
  }
  CE_Warning("can not translate state %d to PVSS state coding\n", state);
  return kStateElse;
}

CEState PVSSStateMapper::GetStateFromMappedState(int state) {
  switch (state) {
    case kStateDownloading:    return eStateConfiguring; // DOWNLOADING
    case kStateError:          return eStateError;       // ERROR	
    case kStateRunning:        return eStateRunning;     // RUNNING 	
    case kStateStandby:        return eStateOn;          // STANDBY	
    case kStateOff:            return eStateOff;         // IDLE	
    case kStateStbyConfigured: return eStateConfigured;  // STBY_CONFIGURED
  }
  CE_Warning("can not translate PVSS state %d to internal state coding\n", state);
  return eStateUnknown;
}
