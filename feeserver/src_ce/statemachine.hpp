// $Id: statemachine.hpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2006
** This file has been written by Matthias Richter
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#ifndef __RCU_STATEMACHINE_HPP
#define __RCU_STATEMACHINE_HPP

#include <string>
#include <vector>
#include "threadmanager.hpp"

class CEDevice;
class CETransition;
class CEStateMachine;
class CEStateMapper;

/**
 * the default states of the device.
 * @ingroup rcu_ce_base
 */
typedef enum {
  /** the current state is unknown */
  eStateUnknown     = -1,
  /** device is off */
  eStateOff         = 0,
  /** error state, left via <tt>shutdown</tt> or <tt>reset</tt> */
  eStateError       = 1,
  /** soft error or error in sub-device, left via <tt>notify</tt> into the previous state*/
  eStateFailure     = 2,
  /** device is on */
  eStateOn          = 16,
  /** device is configuring */
  eStateConfiguring = 32,
  /** device is configured */
  eStateConfigured  = 64,
  /** device is running */
  eStateRunning     = 256,
} CEState;

/**
 * the default transitions of the device.
 * @ingroup rcu_ce_base
 */
typedef enum {
  /** transition is unknown */
  eTransitionUnknown     = 0,
  /** switch the device on */
  eSwitchOn,
  /** switch device off */
  eShutdown,
  /** configure the device */
  eConfigure,
  /** signal configuration done */
  eConfigureDone,
  /** rest the device */
  eReset,
  /** start device */
  eStart,
  /** stop the device */
  eStop,
  /** notify action to confirm the FAILURE state */
  eNotify,
  /** last default transition id */
  eLastDefaultTransition
} CETransitionId;

/**
 * @class CEStateMachine
 * The State Machine class.
 * The State Machine class provides default states and default transitions. The names of the default
 * states and transitions can be adapted by overriding the functions @ref GetDefaultStateName and 
 * @ref GetDefaultTransitionName.
 * <br>
 * The default behaviour is like:
 * <pre>
 *              -----
 *             | OFF | <-------------------------------
 *              -----                                  |
 *               | |                                   |
 *    switchon   | ^  shutdown                         ^  shutdown
 *               | |                                   |
 *              ----                                -------
 *             | ON | <-----------------<- reset --| ERROR |
 *              ----    |             |             -------
 *               |      |             |              can be reached by any state or
 *    configure  |    reset           ^              result of transition	      
 *               |      |             |              left by action 'reset' or
 *     -------------    |             |              'shutdown'
 *    | CONFIGURING |   ^             |
 *     -------------    |             |                    ^
 *               |      |             |-<- reset ----      |
 *               |      |             |              |  shutdown
 *               |      |             ^              |     |
 *               |      |             |              |     |
 *            ------------            \             ---------
 *           | CONFIGURED | <--------- notify -----| FAILURE |
 *            ------------            /             ---------
 *               | |                  |              can be reached by any state or 
 *        start  | ^  stop            |              result of transition
 *               | |                  |              left by action 'notify' to the
 *            ---------               |              previous state, or 'reset' and
 *           | RUNNING | <------------               'shutdown' like the ERROR state
 *            ---------
 * </pre>
 * The @CE_Device calss implements handlers for the entering and leaving a state and the transition
 * handlers for default transitions. The @DefaultTransitionDispatcher is a switch for the default
 * transitions. The concept of default states and transitions was introduced to simplify implementation
 * of devices and to save resources. It is foreseen to extend the default states and transition
 * dynamically. 
 * <b>Note:</b> The FAILURE state and <i>notify</i> handling is forseen but not implemented since
 * it is not needed in the beginning.
 *
 * @ingroup rcu_ce_base
 */
class CEStateMachine : public CEThreadManager {
public:
  CEStateMachine();
  ~CEStateMachine();

  /**
   * This is the external signal to start the state engine.
   * 
   */
  int Armor();

  /**
   * Get the name of the device/state machine
   * @return name as const char array
   */
  const char* GetName();

  /**
   * Check if the device is in a certain state
   * @param state        state to check
   * @return             1 if yes, 0 if not
   */
  int Check(CEState state);

  /**
   * Check if the device is in one of a list of state
   * @param states        array of states to check, terminated by eStateUnknown element
   * @return             1 if yes, 0 if not
   */
  int Check(CEState states[]);

  /**
   * Get the current state.
   */
  CEState GetCurrentState();

  /**
   * Get the current state in a translated encoding.
   * A translation scheme can be defined by the @ref SetTranslationScheme method.
   * If no scheme was defined the output is just the CEState casted to a number.
   * @return             translated state if scheme was defined
   */
  int GetTranslatedState();

  /**
   * Set a translation scheme to present states in an alternative encoding.
   * Currently it involves only the encoding but in the future this can also include
   * the default state names.
   * @param pMapper      a CEStateMapper object defining the translation
   * @return             neg. error code if failed
   */
  int SetTranslationScheme(CEStateMapper* pMapper);

  /**
   * Get name of the current state.
   */
  const char* GetCurrentStateName();

  /**
   * Start a transition.
   * If the device is in the right state for the desired action, the transition
   * dispatcher is called.
   * @param action       char string specifying the action
   * @param iMode        mode flags
   * @param iParam       arbitrary integer parameter passed to dispatcher and handler
   * @param pParam       arbitrary void pointer passed to dispatcher and handler
   * @return             >=0 success, neg error code if failed
   *   -EPERM            wrong state, transition not permittded
   *   -ENOENT           transition not found
   *   -EBUSY            device is busy
   */
  int TriggerTransition(const char* action, int iMode=0, int iParam=0, void* pParam=NULL);

  /**
   * Start a transition.
   * If the device is in the right state for the desired action, the transition
   * dispatcher is called.
   * @param transition   id for the transition to be executed
   * @param iMode        mode flags
   * @param iParam       arbitrary integer parameter passed to dispatcher and handler
   * @param pParam       arbitrary void pointer passed to dispatcher and handler
   * @return             >=0 success, neg error code if failed
   *   -EPERM            wrong state, transition not permittded
   *   -ENOENT           transition not found
   *   -EBUSY            device is busy
   */
  int TriggerTransition(CETransitionId transition, int iMode=0, int iParam=0, void* pParam=NULL);

  /**
   * Start a transition.
   * If the device is in the right state for the desired action, the transition
   * dispatcher is called.
   * @param pTrans       transition to be executed
   * @param iMode        mode flags
   * @param iParam       arbitrary integer parameter passed to dispatcher and handler
   * @param pParam       arbitrary void pointer passed to dispatcher and handler
   * @return             >=0 success, neg error code if failed
   *   -EPERM            wrong state, transition not permittded
   *   -ENOENT           transition not found
   *   -EBUSY            device is busy
   */
  int TriggerTransition(CETransition* pTrans, int iMode=0, int iParam=0, void* pParam=NULL);

  /**
   * Synchronize state machine with the hardware
   * Performe a number of evaluation functions to dtermine the ste of the hardware device and
   * set the state machine according to that
   * @return             >=0 success, neg error code if failed
   */
  int Synchronize();

  /**
   * Update the state channel created by @ref CreateStateChannel.
   */
  virtual int UpdateStateChannel();

protected:
  /** 
   * init the device instance for this state machine.
   * The device instance is initialized from the constructor of the CEDevice class and is used
   * to call handler methods for default transitions.
   * @param pDevice      pointer to device 
   * @return             0 if succeeded, neg error code if failed
   */
  int InitDevice(CEDevice* pDevice, std::string name="");

  /** 
   * create a state channel for this state machine.
   * The function will be called at initialisation and
   * can be overloaded in order to create a 'channel' (whatever it might be),
   * to publish the state trough it. E.g. a DIM channel. 
   * The default implementation does not forsee a channel.
   */
  virtual int CreateStateChannel();

  /** 
   * create an alarm channel for this state machine.
   * The function will be called at initialisation and
   * can be overloaded in order to create a 'channel' (whatever it might be),
   * to publish the alarm trough it. E.g. a DIM channel. 
   * The default implementation does not forsee a channel.
   */
  virtual int CreateAlarmChannel();

  /** 
   * Send an alarm.
   * @param alarm        alarm id
   */
  virtual int SendAlarm(int alarm);

  /**
   * check if the state is one of the default states.
   * @param state        the state to check
   * @return             1 if default state, 0 if not
   */
  static int IsDefaultState(CEState state);

  /**
   * check if the state is a valid state.
   * @param state        the state to check
   * @return             1 if default state, 0 if not
   */
  int IsValidState(CEState state);

  /**
   * check if the transition is one of the default transitions.
   * @param transition   the id of the transition to check
   * @return             1 if default transition, 0 if not
   */
  int IsDefaultTransition(CETransitionId id);
  
  /**
   * Find the transition descriptor for an action.
   * @param action       the name of the action to find a transition for
   * @return             pointer to transition descriptor
   */
  CETransition* FindTransition(const char* action);

  /**
   * Find the transition descriptor for an id.
   * @param id           the id to find a transition for
   * @return             pointer to transition descriptor
   */
  CETransition* FindTransition(CETransitionId id);

  /**
   * Check if the transition is allowed for the current state.
   * See definition of @ref CEStateMachine::fDefTrans for the description of
   * the default transitions.
   * @param transition   pointer to transition descriptor
   * @return             1 if yes, 0 if not, neg. error code if failed
   */
  virtual int IsAllowedTransition(CETransition* pT);

  /**
   * dispatch default transitions to the handler functions.
   * All handler functions for default transitions are virtual functions of the
   * class @ref CEDevice which can be overloaded.
   * @param transition   the transition to be called
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             state after the transition
   */
  CEState DefaultTransitionDispatcher(CETransition* transition, int iParam, void* pParam);

  /**
   * dispatch <i>enter-state</i> events to the handler functions.
   * @param state        state for which the handler has to be called
   * @return             neg. error code is failed
   */
  int StateEnterDispatcher(CEState state);

  /**
   * dispatch <i>enter-state</i> events to the handler functions of default states.
   * @param state        state for which the handler has to be called
   * @return             neg. error code is failed
   */
  int DefaultStateEnterDispatcher(CEState state);

  /**
   * dispatch <i>leave-state</i> events to the handler functions.
   * @param state        state for which the handler has to be called
   * @return             neg. error code is failed
   */
  int StateLeaveDispatcher(CEState state);

  /**
   * dispatch <i>leave-state</i> events to the handler functions for default states.
   * @param state        state for which the handler has to be called
   * @return             neg. error code is failed
   */
  int DefaultStateLeaveDispatcher(CEState state);

  /**
   * dispatch transitions to the handler functions.
   * This function must be overloaded if additional transitions have been added.
   * <b>Note:</b>For future extension.
   * @param transition   the transition to be called
   * @param pDevice      pointer to device instance for this state machine
   * @param iParam       integer parameter passed from the caller of the action
   * @param pParam       void pointer passed from the caller of the action
   * @return             state after the transition
   */
  virtual CEState TransitionDispatcher(CETransition* transition, CEDevice* pDevice, int iParam, void* pParam);

  /**
   * Get the name of a state.
   * The function is a switch between the default states and the dynamic state
   * handling.
   * @param state        the state to check
   * @return             pointer to name
   */
  const char* GetStateName(CEState state);

  /**
   * get the name of a default state.
   * Can be overridden in order to change the default name.
   * @param state        the state to check
   * @return             pointer to name
   */
  virtual const char* GetDefaultStateName(CEState state);

  /**
   * Get the name of a state.
   * The function is a switch between the default transitions and the dynamic transition
   * handling.
   * @param pTrans       the transition to check
   * @return             pointer to name
   */
  const char* GetTransitionName(CETransition* pTrans);

  /**
   * get the name of a default transition.
   * Can be overridden in order to change the default name. The name of the transition corresponds
   * one to one to the name of the <i>Action</i> which triggers the transition.
   * @param transition   the id of the transition to check
   * @return             pointer to name
   */
  virtual const char* GetDefaultTransitionName(CETransition* pTransition);
 
private:
  /**
   * Change from to current state to another one.
   * @param state        the state to change to
   */ 
  int ChangeState(CEState state);

  /** the device instance for this state machine */
  CEDevice* fpDevice;
  /** state variable */
  CEState fState;
  /** name of the state machine */
  std::string fName;
  /** the mapper to translate to another state encoding */
  CEStateMapper* fpMapper;


#ifdef _0
  /* so far we are restricted to the default transitions, this scetch is for later extension
   */

  /** the list of transitions */
  vector<CETransition*> fTransitions;
  /** add a transition to the list */
  int AddTransition(CETransition* pTransition);
  /** delete all transition descriptors in the list */
  int CleanupTransitionList();
#endif

  /** default transition descriptors */
  static CETransition fDefTrans[eLastDefaultTransition];
};

/**
 * @class CETransition
 * The class hosts all parameters which are neccesary for a transition.
 * In order to simplify the handling of the default transitions and to safe memory,
 * there are static dummy descriptors defined in the @ref StateMachine for the default
 * transitions. Both naming and the function callbacks are handled directly via
 * member functions of the @ref CEDevice.<br>
 * The concept is intended to allow the definition of additional transitions. This is currently 
 * not implemented.
 * @ingroup rcu_ce_base
 */
class CETransition : public CEThreadManager {
public:
  /** constructor */
  CETransition();
  /** constructor with initialization */
  CETransition(CETransitionId id, 
	       CEState finalState=eStateUnknown, 
	       const char* name="", 
	       CEState initialState1=eStateUnknown, 
	       CEState initialState2=eStateUnknown, 
	       CEState initialState3=eStateUnknown, 
	       CEState initialState4=eStateUnknown, 
	       CEState initialState5=eStateUnknown);
  /** destructor */
  ~CETransition();

  /**
   * Get the transition id.
   * @return             id of the transition
   */
  CETransitionId GetID();

  /**
   * Get the transition name.
   * @return             name of the transition
   */
  const char* GetName();

  /**
   * check if the transition is one of the default transitions.
   * @param transition   the id of the transition to check
   * @return             1 if default transition, 0 if not
   */
  int IsDefaultTransition();

  /**
   * Check if the transition is allowd for a certain state.
   * The function checks if the specified state is one of the initial states. If
   * there were no initial states defined for the transition the return value is
   * 1.
   * @param state        the state to check   
   * @return             1 if yes, 0 if not, neg. error code if failed
   */
  virtual int IsInitialState(CEState state);

  /**
   * Get the final state of this transition
   * @return final state with successful transition
   */
  CEState GetFinalState();
private:
  /** id of this transition */
  CETransitionId fId;
  /** name of this transition */
  const char* fName;
  /** the final state of this transition */
  CEState fFinalState;
  /** the array of the states which the transition is allowed for */
  std::vector<CEState> fInitialStates;
};

/**
 * A simple base class to provide state code translation.
 * The conversion methods are pure virtual and have to be implemented by the 
 * actual implementation.
 */
class CEStateMapper {
public:
  CEStateMapper() {};
  ~CEStateMapper() {};

  /**
   * Get the mapped state from the CEState.
   * @param state         CEState
   * @return              translated state
   */
  virtual int GetMappedState(CEState state)=0;

  /**
   * Get the CEState from the mapped state.
   * @param state         CEState
   * @return              translated state
   */
  virtual CEState GetStateFromMappedState(int state)=0;
};
#endif //__RCU_STATEMACHINE_HPP
