// $Id: ce_command.c,v 1.1.1.2 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter and Dag T Larsen
** Please report bugs to Matthias.Richter@ift.uib.no, dagtl@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/
//#include <unistd.h>        // sleep
#include <dim/dim_common.h>    // dim_sleep
#include <stdio.h>
#include <errno.h>
#include "ce_command.h"
#include "fee_errors.h"
#include "fee_defines.h"
#ifdef TPC
#include "ce_tpc.h"
#endif //TPC
#ifdef PHOS
#include "ce_phos.h"
#endif //PHOS
#include "rcu_service.h"   // handling of the service API methods for rcu-like CEs
#include "rcu_issue.h"     // handling of the issue API method for rcu-like CEs
#include "ce_base.h"

/**
 * Sleeping time in seconds.
 * The CE update loop sleeps a certain time between the service update function. 
 * The time is adapted to the FeeServer update rate in the 
 * @ref signalFeePropertyChanged API function. 
 * @ingroup feesrv_ce
 */
int g_iSleepSec=1;

/**
 * Sleeping time in micro seconds.
 * If the sleeping time is smaller than a second, this variable is used for a
 * sleep in micro seconds, ignored otherwise. <br>
 * The time is adapted to the FeeServer update rate in the 
 * @ref signalFeePropertyChanged API function. 
 * @ingroup feesrv_ce
 */
int g_iSleepUsec=0;

/***************************************************************************
 * API functions to the feeserver core
 */
int issue(char* command, char** result, int* size) {
  return RCUce_Issue(command, result, size);
}

void cleanUpCE() {
# ifdef RCU
  {
#   ifdef TPC
    {
      TPCcleanUpCE();
    }
#   endif //TPC

#   ifdef PHOS
    {
      PHOScleanUpCE();
    }
#   endif //PHOS

    //  RCUce_CleanIssue();
  RCUcleanUpCE();
  }
#endif //RCU
  return;
}

void initializeCE() {
  int ceState=CE_OK;
  int nRet=CE_OK;
  printf("\ninitializing ControlEngine ...\n");
#ifdef PACKAGE_STRING
  printf("-----   %s   ----\n\n", PACKAGE_STRING);
#endif

#ifdef ENABLE_ANCIENT_05
  printf("compiled for the RCU firmware of May05\n"); 
#endif //ENABLE_ANCIENT_05
#ifdef RCUDUMMY
  printf("This is an RCU dummy version\n"); 
#endif //RCUDUMMY
#ifdef FECSIM
  printf("Front-end card simulation is on\n"); 
#endif //FECSIM

#ifndef DISABLE_SERVICES
# ifdef RCU
  {
    // initialization of the RCU services
    printf("\n-----------------------------------\n");
    printf("init services for configuration RCU\n");
    extern int initializeMainCE();
    initializeMainCE();
    //ceState=RCUinitializeCE();
    if (nRet<CE_OK) {
      fprintf(stderr, "CE::initializedCE error: RCUinitializeCE returned %d\n", nRet);
      if (ceState==CE_OK) ceState=nRet;
    }
    printf("... done\n");

#   ifdef TPC
    {
      // initialization of TPC services
      printf("\n-----------------------------------\n");
      printf("init services for configuration TPC\n");
      TPCinitializeCE();
      if (nRet<CE_OK) {
	fprintf(stderr, "CE::initializedCE error: TPCinitializeCE returned %d\n", nRet);
	if (ceState==CE_OK) ceState=nRet;
      }
      printf("... done\n");
    }
#   endif //TPC

#   ifdef PHOS
    {
      // initialization of PHOS services
      printf("\n------------------------------------\n");
      printf("init services for configuration PHOS\n");
      PHOSinitializeCE();
      if (nRet<CE_OK) {
	fprintf(stderr, "CE::initializedCE error: PHOSinitializeCE returned %d\n", nRet);
	if (ceState==CE_OK) ceState=nRet;
      }
      printf("... done\n");
    }
#   endif //PHOS
  }
# endif //RCU
#else // DISABLE_SERVICES
  printf("CE info:  services disabled\n");
#endif // !DISABLE_SERVICES
  signalCEready(ceState);
  sleep(1);
  while (1) {
    // dont leave the function according to the specifications
#ifndef DISABLE_SERVICES
    nRet=readFECActiveList();
    nRet=ceUpdateServices();
#endif //!DISABLE_SERVICES
    if (g_iSleepSec>0) sleep(g_iSleepSec); // sleep is redirected to dtq_sleep in dim_common.h
    else if (g_iSleepUsec) dim_usleep(g_iSleepUsec);
    else dim_usleep(900000);
  }
  return;
}

void signalFeePropertyChanged(FeeProperty* prop) {
  if (prop) {
    if ((prop->flag|PROPERTY_UPDATE_RATE)!=0) {
      // it isnt clear from the documentation whether to value is ment to present
      // the update rate in seconds, milli or microseconds
      // it seems to be milli seconds
      if (prop->uShortVal>=1000) {
	g_iSleepSec=prop->uShortVal/1000;
	g_iSleepUsec=0;
      }
      else {
	g_iSleepSec=0;
	g_iSleepUsec=prop->uShortVal*1000;
      }
      CE_Info("got property change:PROPERTY_UPDATE_RATE %d\n"
	      "update rate set to %d seconds and %d useconds\n", prop->uShortVal, g_iSleepSec, g_iSleepUsec);  
    }
  }
  return;
}
