// $Id: rcu_issue.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter,
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

/***************************************************************************
 * rcu_issue.c
 * this file implements the handling of 'issue' call of the CE API
 */

#include <errno.h>
#include <stdlib.h>       // malloc, free,
#include <string>         // memcpy
#include <vector>         // vector arrays
#include <unistd.h>       // usleep
#include "fee_errors.h"
#include "ce_command.h"   // CE API
#include "ce_base.h"      // CE primitives
#include "device.hpp"     // CEResultBuffer
#ifdef TPC
#include "ce_tpc.h"
#endif //TPC
#ifdef PHOS
#include "ce_phos.h"
#endif //PHOS
#include "dcscMsgBufferInterface.h" // access library to the dcs board message buffer interface
#include "codebook_rcu.h" // the mapping of the rcu memory
#include "rcu_service.h"
#include "rcu_issue.h"
#include "dev_rcu.hpp"

using namespace std;

/*******************************************************************************************
 * global options
 */
int g_iMaxBufferPrintSize=4;
//int g_printError=0;
#define RCU_OPT_AUTO_CHECK       0x0001
#define RCU_DEFAULT_OPTIONS      RCU_OPT_AUTO_CHECK
int g_rcuoptions=RCU_DEFAULT_OPTIONS;

int rcuSetOptions(int options)
{
  g_rcuoptions=options;
  return g_rcuoptions;
}

int rcuSetOptionFlag(int of)
{
  g_rcuoptions|=of;
  return g_rcuoptions;
}

int rcuClearOptionFlag(int of)
{
  g_rcuoptions&=~of;
  return g_rcuoptions;
}

int rcuCheckOptionFlag(int of)
{
  return (g_rcuoptions&of)!=0;
}


int RCUce_CleanIssue()
{
  int iResult=0;
  // nothing to be done so far
  return iResult;
}

/*******************************************************************************************
 * rcu commands
 */

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::checkRCUError()
{
  int iResult=0;
  __u32 reg=0;
  int busy=0;
  const int nofBusyLoops=3;
  do {
    iResult=rcuSingleRead(AltroErrSt, &reg);
    if (iResult>=0) {
      reg&=~RCU_HWADDR_MASK;
      if (reg>0) {
	if (reg&RCU_SEQ_BUSY) {
	  if (++busy < nofBusyLoops) {
	    continue;
	  } else if (busy == nofBusyLoops) {
	    usleep(10);
	    continue;
	  } else {
	    CE_Error("RCU sequencer remains in busy state\n");
	    iResult=-EBUSY;
	  }
	} else if (reg&SEQ_ABORT) {
	  CE_Warning("Altro sequencer reports SEQ_ABORT \n");
	  iResult=-EINTR;
	} else if (reg&FEC_TIMEOUT) {
	  CE_Error("Altro sequencer reports error FEC_TIMEOUT \n");
	  iResult=-ETIMEDOUT;
	} else if (reg&ALTRO_ERROR) {
	  CE_Error("Altro sequencer reports error ALTRO_ERROR \n");
	  iResult=-EIO;
	} else if (reg&ALTRO_HWADD_ERROR) {
	  CE_Error("Altro sequencer reports error ALTRO_HWADD_ERROR \n");
	  iResult=-EIO;
	} else if (reg&PATTERN_MISSMATCH) {
	  CE_Warning("Altro sequencer reports error PATTERN missmatch \n");
	  iResult=-EBADF;
	}
      }
    } else {
      CE_Error("failed to read from RCU memory\n");
    }
  } while (iResult>=0 && reg>0);
  return iResult;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::sendRCUExecCommand(unsigned short start)
{
  int iResult=0;
  if ((iResult=rcuSingleWrite(CMDExecALTRO, start))>=0) {
    CE_Debug("instruction buffer executed\n");
    if (rcuCheckOptionFlag(RCU_OPT_AUTO_CHECK)) {
      iResult=checkRCUError();
    }
  } else {
    CE_Error("altro exec command failed, error=%d", iResult);
  }
  return iResult;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::sendStopRCUExec()
{
  int iResult=0;
  if ((iResult=rcuSingleWrite(CMDAbortALTRO, 0))<0) {
    CE_Error("attemp to stop execution failed with error %d\n", iResult);
  }
  return iResult;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::writeCommandBufferToRCUMem(const char* pBuffer, int iCount, int iWordSize, __u32 u32BaseAddress)
{
  int iResult=0;
  const char* pSrc=pBuffer;
  __u32 u32Address= u32BaseAddress;
  __u32 data=0;
  int i=0;
  if (iWordSize==4 || iWordSize==2) {
    if (ceCheckOptionFlag(DEBUG_USE_SINGLE_WRITE)) {
      for (i=0;i<iCount && iResult>=0;i++) {
	data=*((__u32*)pSrc);
	if (iWordSize==2)
	  data&=0x0000ffff;
	else if (iWordSize!=4) {
	  CE_Error("bit width %d not supported by the debug version\n", iWordSize);
	  iResult=-ENOSYS;
	  break;
	}
#ifndef RCUDUMMY
	iResult=rcuSingleWrite(u32Address, data);
#else //RCUDUMMY
	if (u32Address==FECActiveList) {
	  extern __u32 g_RCUAFL;
	  g_RCUAFL=data;
	} else {
	  CE_Info("so far rcu dummy write just ignores the data\n"); 
	}
#endif //RCUDUMMY
	pSrc+=iWordSize;
	u32Address++;
      }
    } else {
#ifndef RCUDUMMY
      iResult=rcuMultipleWrite(u32BaseAddress, (__u32*)pBuffer, iCount, iWordSize);
#else //RCUDUMMY
      if (u32BaseAddress==FECActiveList) {
	extern __u32 g_RCUAFL;
	g_RCUAFL=*((__u32*)pBuffer);
      } else {
	CE_Info("so far rcu dummy write just ignores the data\n"); 
      }
#endif //RCUDUMMY
    }
    if (iResult>=0) {
      CE_Debug("wrote %d words to rcu buffer 0x%08x with result %d\n", iCount, u32BaseAddress, iResult);
      iResult=iCount;
    } else {
      CE_Error("writing to rcu buffer 0x%08x aborted after %d words, result %d\n", u32BaseAddress, i, iResult);
    }
  } else {
    CE_Error("writeCommandBufferToRCUMem: invalid word size\n");
  }
  return iResult;
}


/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
/*
int CErcu::writeCommandBufferToRCUInstructionMem(const char* pBuffer, int iSize)
{
  int iResult=0;
  __u32 u32Address=ALTROInstMEM;
  int iAlign=sizeof(__u32);
  int iCount=iSize/iAlign;
  if (iAlign*iCount!=iSize){
    CE_Warning("block of 32 bit words expected, but blocksize %d is not aligned to %d\n", iSize, iAlign);
  }
  if (iCount>ALTROInstMEM_SIZE) {
    CE_Error("command buffer (%d) exceeds size of rcu instruction buffer(%d)\n", iCount, ALTROInstMEM_SIZE);
  } else {
     if ((iResult=writeCommandBufferToRCUMem(pBuffer, iCount, iAlign, u32Address))>=0) {
    }
  }
  return iResult;
}
*/

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
/*
int CErcu::writeDataBlockToRCUPatternMem(const char* pBuffer, int iSize, int iWordSize)
{
  int iResult=0;
  __u32 u32Address=ALTROPatternMEM;
  const char* pSrc=pBuffer; // pointer to data block used further down, can be altered by the bit processing
  int iSrcSize=iSize; // size of the data block pSrc is pointing to
 
  // check the width of the data words
  // 8, 16 and 32 bit words can be written without further processing
  // 10 bit is a special compressed format and needs further processing first 
  int iAlign=0;
  if (iWordSize==8 || iWordSize==16 || iWordSize==32) {
    iAlign=iWordSize/8;
  } else if (iWordSize==0) {
    iAlign=4;
  } else if (iWordSize==10) {
    CE_Error("processing of 10 bit data not yet implemented\n");
    iResult=-ENOSYS;
  } else {
    CE_Error("bit width %d not supported\n", iWordSize);
    iResult=-ENOSYS;
  }

  // write the data
  if (iResult>=0 && iAlign>0) {
    int iCount=iSrcSize/iAlign;
    if (iAlign*iCount!=iSrcSize){
      CE_Warning("block of 16 bit words expected, but blocksize %d is not aligned to %d\n", iSize, iAlign);
    }
    if (iCount>ALTROPatternMEM_SIZE) {
      CE_Error("command buffer (%d) exceeds size of rcu pattern memory(%d)\n", iCount, ALTROPatternMEM_SIZE);
    } else {
      if ((iResult=writeCommandBufferToRCUMem(pSrc, iCount, iAlign, u32Address))>=0) {
	CE_Info("data block of %d words written to rcu pattern memory\n", iCount);
      }
    }
  }
  return iResult;
}
*/

int CErcu::writeDataBlockToRCUMem(const char* pBuffer, int iSize, int iNofWords, int iWordSize,
				  __u32 tgtAddress, int partSize)
{
  int iResult=0;
  // check the width of the data words
  int iWordsPer32=0;
  if (iWordSize==8 || iWordSize==16 || iWordSize==32 || iWordSize==10) {
    iWordsPer32=32/iWordSize;
  } else if (iWordSize==0) {
    iWordsPer32=1;
  } else {
    CE_Error("bit width %d not supported\n", iWordSize);
    iResult=-ENOSYS;
  }

  // write the data
  if (iResult>=0 && iWordsPer32>0) {
    // align to 32 bit
    int iMinBufferSize=(((iNofWords-1)/iWordsPer32)+1)*sizeof(__u32);
    if (iMinBufferSize<=iSize){
      if (partSize>0 && iNofWords>partSize) {
	CE_Error("buffer (size %d) exceeds size of rcu memory partition %#x (%d)\n", iNofWords, tgtAddress, partSize);
      } else {
	/* for some historical reason, the 'align' parameter of the rcuMultipleWrite method
	   of the dcscMsgBuffer interface has the following meaning:
	   1 = 8 bit, 2 = 16 bit, 4 = 32 bit and 3 = 10 bit (although the last one is not 
	   logical)
	*/
	int iAlign=0;
	switch (iWordsPer32) {
	case 1: iAlign=4; break;
	case 4: iAlign=1; break;
	default: iAlign=iWordsPer32;
	}
	if ((iResult=writeCommandBufferToRCUMem(pBuffer, iNofWords, iAlign, tgtAddress))>=0) {
	  iResult=iMinBufferSize;
	  CE_Debug("data block of %d words written to rcu memory at %#x\n", iNofWords, tgtAddress);
	}
      }
    } else {
      CE_Error("too little data (%d byte(s)) to write %d %d-bit words\n", iSize, iNofWords, iWordSize);
      iResult=-ENODATA;
    }
  }
  return iResult;
}
/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
int CErcu::readRcuMemoryIntoResultBuffer(__u32 address, int size, int partSize,
					 CEResultBuffer& rb)
{
  int iResult=0;
  if (address>0xffff) {
    CE_Error("address %#x exceeds RCU memory space\n", address);
    return -EINVAL;
  }
  if (partSize>0 && size>partSize) {
    size=partSize;
    CE_Warning("requested size exceeds size of partition %#x, truncated\n", address);
  }
  if (address+size>0xffff) {
    size=0x10000-address;
    CE_Warning("requested size exceeds size of RCU memory space, truncated\n");    
  }
  int iCurrent=rb.size();
  rb.resize(iCurrent+size, 0);
  int i=0;
    if (ceCheckOptionFlag(DEBUG_USE_SINGLE_WRITE)) {
      __u32 addr=address;
      for (i=0; addr<address+size && iResult>=0; addr++, i++) {
#ifndef RCUDUMMY
	iResult=rcuSingleRead(addr, &rb[iCurrent+i]);
#else //RCUDUMMY
	if (addr==FECActiveList) {
	  extern __u32 g_RCUAFL;
	  rb[iCurrent+i]=g_RCUAFL;
	} else {
	  rb[iCurrent+i]=addr;
	}
#endif //RCUDUMMY
      }
      if (iResult>=0) iResult=size;
    } else {
#ifndef RCUDUMMY
      iResult=rcuMultipleRead(address, size, &rb[iCurrent]);
#else //RCUDUMMY
      CE_Debug("dummy read, address %x\n", address);
      __u32 addr=address;
      for (i=0; addr<address+size && iResult>=0; addr++, i++) {
	if (addr==FECActiveList) {
	  extern __u32 g_RCUAFL;
	  rb[iCurrent+i]=g_RCUAFL;
	} else {
	  rb[iCurrent+i]=addr;
	}
      }
      iResult=size;
#endif //RCUDUMMY
    }
    if (iResult<size) {
      // shrink the array if there were less words read
      rb.resize(iCurrent+(iResult<0?0:iResult));
    } else if (iResult>size) {
      CE_Warning("result missmatch: %d words read, but %d requested to read\n", iResult, size);
      iResult=size;
    }
  return iResult;
}

/* This code was merged into the CErcu class but for conveniece and in order to make tracking
 * of changes easier the implementation stays here. The handler function was moved to dev_rcu.cpp
 */
extern int translateRcuCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb);
int CErcu::TranslateRcuCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb) {
  int iResult=0;
  int iBitWidth=8;
//   int iProcessed=0;
  CE_Debug("translateRcuCommand cmd=%#x parameter=%#x datasize=%d\n", cmd, parameter, iDataSize);
  switch (cmd) {
  case RCU_WRITE_INSTRUCTION:
  case RCU_EXEC_INSTRUCTION:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, parameter, 32, 
				   ALTROInstMEM, ALTROInstMEM_SIZE);
    if (iResult>=0) {
      if (cmd==RCU_EXEC_INSTRUCTION) {
	iResult=sendRCUExecCommand(0);
      }
    }
    /* the old instruction memory write code
      if (parameter*sizeof(__u32)<=iDataSize) {
	iProcessed=parameter*sizeof(__u32);
	CE_Info("write data to rcu instruction memory\n");
	iResult=writeCommandBufferToRCUInstructionMem(pData, parameter*sizeof(__u32));
	if (iResult>=0) {
	  if (iResult!=parameter) {
	    CE_Error("instruction buffer not completely written, %d of %d\n", iResult, parameter);
	    iResult=-EIO;
	  } else
	    iResult=0;
	}
	if (iResult>=0 && cmd==RCU_EXEC_INSTRUCTION) {
	  iResult=sendRCUExecCommand(0);
	}
      } else {
	CE_Error("data size missmatch, %d byte available in buffer but %d requested by command (%#x)\n", iDataSize, parameter, cmd);
	iResult=-EINVAL;
      }
    */
    break;
  case RCU_WRITE_PATTERN32:
    iBitWidth*=2;
    // fall through intended
  case RCU_WRITE_PATTERN16:
    iBitWidth*=2;
    // fall through intended
  case RCU_WRITE_PATTERN8:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, parameter, iBitWidth, 
				   ALTROPatternMEM, ALTROPatternMEM_SIZE);
    break;
  case RCU_WRITE_PATTERN10:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, parameter, 10, 
				   ALTROPatternMEM, ALTROPatternMEM_SIZE);
    /* this is the old pattern memory write code
    if (cmd==RCU_WRITE_PATTERN10) {
      // TODO: check the parameter versus the data size
      iBitWidth=10;
      CE_Error("writing of 10 bit compressed data not yet implemented\n");
    } else
    // TODO: quick hack working for 8, 16 and 32 bit operations this check has to be done correctly
    if (parameter*iBitWidth/8<=iDataSize) {
      iProcessed=parameter*iBitWidth/8;
      CE_Info("write data to rcu pattern memory\n");
      iResult=writeDataBlockToRCUPatternMem(pData, iProcessed, iBitWidth);
      if (iResult>=0) {
	if (iResult!=parameter) {
	  CE_Error("pattern buffer not completely written, %d of %d\n", iResult, parameter);
	  iProcessed=0;
	  iResult=-EIO;
	} else {
	  iResult=0;
	}
      }
    } else {
      CE_Error("data size missmatch, %d byte available in buffer but %d requested by command (%#x)\n", iDataSize, parameter*iBitWidth/8, cmd);
      iResult=-EINVAL;
    }
    */
    break;
  case RCU_EXEC:
    // TODO: in conjunction with the pre-scanning to be implemented there has to be 
    // a version check of the command set
    iResult=sendRCUExecCommand(parameter);
    if (iResult>=0) iResult=0;
    break;
  case RCU_STOP_EXEC:
    iResult=sendStopRCUExec();
    if (iResult>=0) iResult=0;
    break;
  case RCU_READ_INSTRUCTION:
    {
      /* the old code
      __u32 address=ALTROInstMEM;
      int size=parameter;
      if (size>ALTROInstMEM_SIZE) {
	size=ALTROInstMEM_SIZE;
	CE_Warning("requested size exceeds size of instruction memory, truncated\n");
      }
      iProcessed=0;
      */
      iResult=readRcuMemoryIntoResultBuffer(ALTROInstMEM, parameter, ALTROInstMEM_SIZE, rb);
      if (iResult>=0) iResult=0;
    }
    break;
  case RCU_READ_PATTERN:
    {
      /* the old code
      __u32 address=ALTROPatternMEM;
      int size=parameter;
      if (size>ALTROPatternMEM_SIZE) {
	size=ALTROPatternMEM_SIZE;
	CE_Warning("requested size exceeds size of pattern memory, truncated\n");
      }
      iProcessed=0;
      */
      iResult=readRcuMemoryIntoResultBuffer(ALTROPatternMEM, parameter, ALTROPatternMEM_SIZE, rb);
      if (iResult>=0) iResult=0;
    }
    break;
  case RCU_READ_MEMORY:
    iResult=readRcuMemoryIntoResultBuffer(parameter, 1, 0, rb);
    /*
    iProcessed=0;
    */
    if (iResult>=0) iResult=0;
    break;
  case RCU_WRITE_MEMORY:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, 1, 32, 
				   parameter, 0);
    /* the old code
    if (pData && iDataSize>=sizeof(__u32)) {
      iResult=writeCommandBufferToRCUMem(pData , 1, 4, parameter);
      iProcessed=sizeof(__u32);
    } else {
      CE_Error("write memory: no data avaiable\n");
    }
    */
    break;
  case RCU_WRITE_RESULT:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, parameter, 32, 
				   ALTROResultMEM, ALTROResultMEM_SIZE);
    /* the old code
      if (parameter*sizeof(__u32)<=iDataSize) {
	iProcessed=parameter*sizeof(__u32);
	__u32 u32Address=ALTROResultMEM;
	int iSize=parameter*sizeof(__u32);
	int iAlign=sizeof(__u32);
	int iCount=iSize/iAlign;
	if (iAlign*iCount!=iSize){
	  CE_Warning("block of 32 bit words expected, but blocksize %d is not aligned to %d\n", iSize, iAlign);
	}
	if (iCount>ALTROResultMEM_SIZE) {
	  CE_Error("command buffer (%d) exceeds size of rcu result buffer(%d)\n", iCount, ALTROResultMEM_SIZE);
	} else {
	  if ((iResult=writeCommandBufferToRCUMem(pData, iCount, iAlign, u32Address))>=0) {
	  }
	}
	if (iResult>=0) {
	  if (iResult!=parameter) {
	    CE_Error("result buffer not completely written, %d of %d\n", iResult, parameter);
	    iResult=-EIO;
	  } else
	    iResult=0;
	}
      } else {
	CE_Error("data size missmatch, %d byte available in buffer but %d requested by command (%#x)\n", iDataSize, parameter, cmd);
	iResult=-EINVAL;
      }
    */
    break;
  case RCU_READ_RESULT:
    {
      /* the old code
      __u32 address=ALTROResultMEM;
      int size=parameter;
      if (size>ALTROResultMEM_SIZE) {
	size=ALTROResultMEM_SIZE;
	CE_Warning("requested size exceeds size of result memory, truncated\n");
      }
      iProcessed=0;
      */
      iResult=readRcuMemoryIntoResultBuffer(ALTROResultMEM, parameter, ALTROResultMEM_SIZE, rb);
      if (iResult>=0) iResult=0;
    }
    break;
  case RCU_WRITE_MEMBLOCK: // write block to rcu memory {parameter+1}
    // the first 32 bit word of the data is the address to write to, the data is following
    if ((parameter+1)*sizeof(__u32)<=iDataSize) {
      __u32 u32Address=*((__u32*)pData);
      /* the old code
      iProcessed=(parameter+1)*sizeof(__u32);
      const char* pDataBuffer=pData;
      __u32 u32Address=*((__u32*)pDataBuffer);
      pDataBuffer+=sizeof(__u32);
      int iSize=parameter*sizeof(__u32);
      int iAlign=sizeof(__u32);
      int iCount=iSize/iAlign;
      if (iAlign*iCount!=iSize){
	CE_Warning("block of 32 bit words expected, but blocksize %d is not aligned to %d\n", iSize, iAlign);
      }
      */
      if (u32Address<0x10000 && parameter<0x10000) {
	if (u32Address+parameter>0xffff) {
	  CE_Error("data buffer (%d) exceeds size of rcu address range\n", parameter);
	} else {
	  iResult=writeDataBlockToRCUMem(pData+sizeof(__u32), iDataSize, parameter, 32, 
					 u32Address, 0);
	  if (iResult>=0) iResult++;
	  /* the old code
	  if ((iResult=writeCommandBufferToRCUMem(pDataBuffer, iCount, iAlign, u32Address))>=0) {
	    if (iResult!=parameter) {
	      CE_Error("buffer not completely written, %d of %d\n", iResult, parameter);
	      iResult=-EIO;
	    } else
	      iResult=0;
	  }
	  */
	}
      } else {
	CE_Error("address/count exceeds rcu address range\n");
	iResult=-EINVAL;
      }
    } else {
      CE_Error("data size missmatch, %d byte available in buffer but %d requested by command (%#x)\n", iDataSize, (parameter+1)*sizeof(__u32), cmd);
      iResult=-EINVAL;
    }
    break;
  case RCU_READ_MEMBLOCK: // read block from rcu memory {1}
    // number of data words to read specified by parameter, address is the 32 bit word following the command id
    if (iDataSize>=sizeof(__u32)) {
      /*
      iProcessed=sizeof(__u32);
      */
      __u32 address=*((__u32*)pData);
      iResult=readRcuMemoryIntoResultBuffer(address, parameter, 0, rb);
      if (iResult>=0) iResult=sizeof(__u32);
      /*
      int size=parameter;
      if (address<0x10000 && parameter<0x10000) {
	if (address+parameter>0xffff) {
	  size=0xffff-address;
	  CE_Warning("requested size exceeds size of rcu address range, truncated\n");
	}
	iResult=readRcuMemoryIntoResultBuffer(address, size, rb);
	if (iResult>=0) iResult=sizeof(__u32);
      } else {
	CE_Error("address/count exceeds rcu address range\n");
	iResult=-EINVAL;
      }
      */
    }
    break;
  case RCU_CHECK_ERROR:
    rb.resize(sizeof(__u32), 0);
    rb[0]=checkRCUError();
    iResult=0;
    /*
    iProcessed=0;
    */
    break;
  case RCU_EN_AUTO_CHECK:
    if (parameter!=0) {
      iResult=rcuSetOptionFlag(RCU_OPT_AUTO_CHECK);
      CE_Info("switch on automatic check of rcu sequencer (%0x)\n", iResult);
    } else {
      iResult=rcuClearOptionFlag(RCU_OPT_AUTO_CHECK);
      CE_Info("switch off automatic check of rcu sequencer (%0x)\n", iResult);
    }
    iResult=0;
    /*
    iProcessed=0;
    */
    break;
  case RCU_READ_ERRST:
    iResult=readRcuMemoryIntoResultBuffer(AltroErrSt, 1, AltroErrSt_SIZE, rb);
    if (iResult>=0) iResult=0;
    break;
  case RCU_WRITE_TRGCFG:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, 1, 32, 
				   AltroTrCfg, AltroTrCfg_SIZE);
    break;
  case RCU_READ_TRGCFG:
    iResult=readRcuMemoryIntoResultBuffer(AltroTrCfg, 1, AltroTrCfg_SIZE, rb);
    if (iResult>=0) iResult=0;
    break;
  case RCU_WRITE_PMCFG:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, 1, 32, 
				   AltroPmCfg, AltroPmCfg_SIZE);
    break;
  case RCU_READ_PMCFG:
    iResult=readRcuMemoryIntoResultBuffer(AltroPmCfg, 1, AltroPmCfg_SIZE, rb);
    if (iResult>=0) iResult=0;
    break;
  case RCU_WRITE_AFL:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, 1, 32, 
				   FECActiveList, FECActiveList_SIZE);
    break;
  case RCU_READ_AFL:
    iResult=readRcuMemoryIntoResultBuffer(FECActiveList, 1, FECActiveList_SIZE, rb);
    if (iResult>=0) iResult=0;
    break;
  case RCU_WRITE_ACL:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, parameter, 32, 
				   ALTROACL, ALTROACL_SIZE);
    break;
  case RCU_READ_ACL:
    iResult=readRcuMemoryIntoResultBuffer(ALTROACL, parameter, ALTROACL_SIZE, rb);
    if (iResult>=0) iResult=0;
    break;
  case RCU_WRITE_HEADER:
    iResult=writeDataBlockToRCUMem(pData, iDataSize, parameter, 32, 
				   DataHEADER, DataHEADER_SIZE);
    break;
  case RCU_READ_HEADER:
    iResult=readRcuMemoryIntoResultBuffer(DataHEADER, parameter, DataHEADER_SIZE, rb);
    if (iResult>=0) iResult=0;
    break;
  case RCU_RESET:
    {
      __u32 dummy=0;
      __u32 cmd=CMDRESET;
      switch (parameter) {
      case 1: cmd=CMDRESETFEC; break;
      case 2: cmd=CMDRESETRCU; break;
      }
      writeDataBlockToRCUMem((const char*)&dummy, sizeof(__u32), 1, 32, cmd, 1);
      iResult=0;
    }
    break;
  case RCU_L1TRG_SELECT:
    {
      __u32 dummy=0;
      __u32 cmd=CMDL1CMD;
      switch (parameter) {
      case 1: cmd=CMDL1TTC; break;
      case 2: cmd=CMDL1I2C; break;
      }
      writeDataBlockToRCUMem((const char*)&dummy, sizeof(__u32), 1, 32, cmd, 1);
      iResult=0;
    }
    break;
  case RCU_SEND_L1_TRIGGER:
    {
      __u32 dummy=0;
      __u32 cmd=CMDL1;
      writeDataBlockToRCUMem((const char*)&dummy, sizeof(__u32), 1, 32, cmd, 1);
      iResult=0;
    }
    break;
  default:
    CE_Warning("unrecognized command id (%#x)\n", cmd);
    iResult=-ENOSYS;
  }
  return iResult;
}

/* quick fix to identify the main configure command */
int g_hwAddress=-1;
extern int actionHandler(CETransitionId transition);
int translateCommand(char* buffer, int size, CEResultBuffer& rb, int bSingleCmd);
/*******************************************************************************************
/
/**
 * the commands concerning and explicitly handled by the CE.
 * @param cmd       command id stripped py the parameter
 * @param parameter the parameter (16 lsb of the command header)
 * @param pData     payload buffer excluding the header
 * @param iDataSize size of the payload buffer in bytes 
 * @param rb        reference to result buffer
 * @return          number of bytes of the payload buffer which have been processed
 *                  neg. error code if failed
 * @ingroup rcu_issue
 */
int translateCeCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb) {
  int iResult=0;
  int iProcessed=0;
  CE_Debug("translateCeCommand cmd=%#x parameter=%#x\n", cmd, parameter);
  switch (cmd) {
  case CEDBG_SET_BUF_PRINT_SIZE:
    g_iMaxBufferPrintSize=parameter;
    CE_Info("printing maximum %d bytes of incoming buffer\n", g_iMaxBufferPrintSize);
    break;
  case CEDBG_EN_SERVICE_UPDATE:
    if (parameter==0)
      ceSetOptionFlag(DEBUG_DISABLE_SRV_UPDT);
    else
      ceClearOptionFlag(DEBUG_DISABLE_SRV_UPDT);
    iResult=setValidFECs(parameter);
    break;
  case CEDBG_SET_SERVICE_VALUE:
    if (pData && iDataSize>0) {
      iProcessed=(parameter-1)/sizeof(__u32)+1+sizeof(__u32);
      if (iDataSize>=iProcessed) {
	__u32 value=*((__u32*)pData);
	const char* name=pData+sizeof(__u32);
	if (*(name+parameter)==0) {
	  iResult=ceWriteService(name, value);
	} else {
	  CE_Error("service name is not zero terminated\n");
	}
      } else {
	CE_Error("data size missmatch, %d byte(s) available, but %d expected\n", iDataSize, parameter+sizeof(__u32));
	iProcessed=0;
      }
    } else {
      CE_Error("write service value: no data avaiable\n");
    }
    break;
  case CE_RELAX_CMD_VERS_CHECK:
    iResult=ceSetOptionFlag(DEBUG_RELAX_CMD_CHECK);
    CE_Info("relax command end marker and version check\n");
    break;
  case CE_SET_LOGGING_LEVEL:
    iResult=ceSetLogLevel(parameter);
    CE_Info("set logging level to %d\n", iResult);
    break;
  case CE_GET_HIGHLEVEL_CMDS:
    if (pData && parameter<=iDataSize) {
      CE_Debug("CE_GET_HIGHLEVEL_CMDS %s\n", pData);
    }
    iProcessed=((parameter-1)/4+1)*4;
    break;
  case CE_FORCE_CH_UPDATE:
    if (pData && parameter<=iDataSize) {
      CE_Debug("CE_FORCE_CH_UPDATE %s\n", pData);
    }
    iProcessed=((parameter-1)/4+1)*4;
    break;
  case CE_TRIGGER_TRANSITION:
    if (pData && parameter<=iDataSize) {
      CE_Debug("CE_TRIGGER_TRANSITION %s\n", pData);
    }
    iProcessed=((parameter-1)/4+1)*4;
    break;
  case CE_GET_STATES:
    if (pData && parameter<=iDataSize) {
      CE_Debug("CE_GET_STATES %s\n", pData);
    }
    iProcessed=((parameter-1)/4+1)*4;
    break;
  case CE_GET_TRANSITIONS:
    if (pData && parameter<=iDataSize) {
      CE_Debug("CE_GET_TRANSITIONS %s\n", pData);
    }
    iProcessed=((parameter-1)/4+1)*4;
    break;
  case FEE_CONFIGURE:
    if (iDataSize>=2*sizeof(__u32)) {
      __u32 hwAddress=*((__u32*)pData);
      __u32 checksum=*(((__u32*)pData)+1);
      CE_Debug("got FEE_CONFIGURE: hardware address %#x check sum %#x \n", hwAddress, checksum);
      /* this is a quick fix to identify the main configure command and its end
       * will be replaced by a hierarchically command handling
       */
      if (g_hwAddress<0) {
	iResult=actionHandler(eConfigure);
	g_hwAddress=hwAddress;
      }
      int i=0;
      if (iResult>=0) {
      iProcessed=2*sizeof(__u32);
      for (i=0; i<parameter && iResult>=0; i++) {
	if ((iResult=translateCommand((char*)pData+iProcessed, iDataSize-iProcessed, rb, 1))>=0) {
	  iProcessed+=iResult;
	}
      }
      } else {
	CE_Error("can not switch main state machine to configuration mode, error %d\n", iResult);
      }
      CE_Debug("end of FEE_CONFIGURE sequence with result %d (%d of %d commands)\n", iResult, i, parameter);
    } else {
      CE_Error("missing payload for FEE_CONFIGURE\n");
      iResult=-EPROTO;
    }
    break;
  case FEE_CONFIGURE_END:
    if (iDataSize>=sizeof(__u32)) {
      __u32 hwAddress=*((__u32*)pData);
      CE_Debug("got FEE_CONFIGURE_END: hardware address %#x \n", hwAddress);
      if (g_hwAddress==hwAddress) {
	iResult=actionHandler(eConfigureDone);
	g_hwAddress=-1;
      }
      if (iResult>=0) {
      } else {
	CE_Error("state machine termination of configuration mode, error %d\n", iResult);
      }
      iProcessed=sizeof(__u32);
    } else {
      CE_Error("missing hardware address for FEE_CONFIGURE_END\n");
      iResult=-EPROTO;
    }
    break;
  case FEE_VERIFICATION:
    if (parameter>0) {
      CE_Error("non-empty FEE_VERIFICATION command currently not available, use empty one\n");
      iResult=-ENOSYS;
    }
    break;
  case FEE_EXTERNAL_CONFIGURATION:
    CE_Error("external configuration currently no available\n");
    iResult=-ENOSYS;
    break;
  default:
    CE_Warning("unrecognized command id (%#x)\n", cmd);
    iResult=-ENOSYS;
  }

  if (iResult>=0) iResult=iProcessed;
  return iResult;
}

/********************************************************************************************/

/**
 * the commands concerning the message buffer access library
 * @param cmd       command id stripped py the parameter
 * @param parameter the parameter (16 lsb of the command header)
 * @param pData     payload buffer excluding the header
 * @param iDataSize size of the payload buffer in bytes 
 * @param rb        reference to result buffer
 * @return          number of bytes of the payload buffer which have been processed
 *                  neg. error code if failed
 * @ingroup rcu_issue
 */
int translateDcscCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb) {
  int iResult=0;
  int iProcessed=0;
  CE_Debug("translateDcscCommand cmd=%#x parameter=%#x\n", cmd, parameter);
  switch (cmd) {
  case DCSC_SET_OPTION_FLAG:
    iResult=setDebugOptionFlag(parameter);
    CE_Info("dcscRCUaccess debug options: %#x\n", iResult);
    break;
  case DCSC_CLEAR_OPTION_FLAG:
    iResult=clearDebugOptionFlag(parameter);
    CE_Info("dcscRCUaccess debug options: %#x\n", iResult);
    break;
  case DCSC_USE_SINGLE_WRITE:
    if (parameter!=0) {
      iResult=ceSetOptionFlag(DEBUG_USE_SINGLE_WRITE);
      CE_Info("use single write (%0x)\n", iResult);
    } else {
      iResult=ceClearOptionFlag(DEBUG_USE_SINGLE_WRITE);
      CE_Info("use multiple write (%0x)\n", iResult);
    }
    break;
  default:
    CE_Warning("unrecognized command id (%#x)\n", cmd);
    iResult=-ENOSYS;
  }
  if (iResult>=0) iResult=iProcessed;
  return iResult;
}

/*******************************************************************************************/

/**
 * setting of values for data points in the Front-end electronics
 * @param cmd       command id stripped py the parameter
 * @param parameter the parameter (16 lsb of the command header)
 * @param pData     payload buffer excluding the header
 * @param iDataSize size of the payload buffer in bytes 
 * @param rb        reference to result buffer
 * @return          number of bytes of the payload buffer which have been processed
 *                  neg. error code if failed
 * @ingroup rcu_issue
 */
int setFeroData(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb) {
  int iResult=0;
  int iProcessed=0;
  CE_Debug("setFeroData cmd=%#x parameter=%#x datasize=%d\n", cmd, parameter, iDataSize);
  int iWordSize=1;
  switch (cmd) {
  case FEESVR_SET_FERO_DFLOAT:
    iWordSize=sizeof(float)/sizeof(__u32);
    //fall through
  case FEESVR_SET_FERO_DATA32:
    iWordSize*=2;
    //fall through
  case FEESVR_SET_FERO_DATA16:
    iWordSize*=2;
    //fall through
  case FEESVR_SET_FERO_DATA8:
    if (pData && iDataSize>0) {
      if (iDataSize>=parameter+iWordSize) {
	iProcessed=parameter+iWordSize;
	const char* name=pData+iWordSize;
	CE_Debug("set fero data: %s\n", name);
	if (*(name+parameter-1)==0) {
	  float fVal=0;
	  switch (cmd) {
	  case FEESVR_SET_FERO_DFLOAT:
	    fVal=*((float*)pData);
	    break;
	  case FEESVR_SET_FERO_DATA32:
	    fVal=*((__u32*)pData);
	    break;
	  case FEESVR_SET_FERO_DATA16:
	    fVal=*((__u16*)pData);
	    break;
	  case FEESVR_SET_FERO_DATA8:
	    fVal=*((__u8*)pData);
	    break;
	  }
	  CE_Debug("set value for service \'%s\' (%f)\n", name, fVal);
	  iResult=ceSetValue(name, fVal);
	} else {
	  CE_Error("service name is not zero terminated\n");
	}
      } else {
	CE_Error("data size missmatch, %d byte(s) available, but %d expected\n", iDataSize, parameter+sizeof(__u32));
      }
    } else {
      CE_Error("write service value: no data avaiable\n");
    }
    break;
  default:
    CE_Warning("unknown command id (%#x)\n", cmd);
    iResult=-ENOSYS;
  }
  if (iResult>=0) iResult=iProcessed;
  return iResult;
}

/*******************************************************************************
 * data readout
 */
int dataReadout(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb) {
  int iResult=0;
  CE_Debug("dataReadout cmd=%#x parameter=%#x\n", cmd, parameter);
  CE_Error("function not yet implemented\n");
  iResult=-ENOSYS;
  return iResult;
}

extern int translateShellCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, 
				 CEResultBuffer& rb);
#ifdef TPC
extern int translateTpcCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb);
#endif //TPC
#ifdef PHOS
extern int translatePhosCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb);
#endif //PHOS

/*******************************************************************************/
/**
 * Global switch to the corresponding handlers.
 * The function checks the incoming command block for CE commands, encoded
 * Msg Buffer blocks end high level commands and calls the corresponding
 * handler to process the command. CE commands can appear in a sequence. The
 * handler functions must return the number of bytes they have processed in
 * the incoming buffer. There is no scanning functionality and decomposition
 * forseen in the switch, it is a sequential processing. Note that some of the
 * commands can not appear in a command sequence (@ref translateShellCommand).
 * @param buffer      the incoming command buffer
 * @param size        size of the incoming command buffer in byte
 * @param rb          result buffer to receive the command result
 * @param bSingleCmd  execute just the first command of a sequence
 * @return            number of processed bytes of the buffer, neg error code if
 *                    failed
 */
int translateCommand(char* buffer, int size, CEResultBuffer& rb, int bSingleCmd)
{
  int iResult=0;
  CE_Debug("translateCommand size=%d\n", size);
  int iProcessed=0;
  char* pData=buffer;
  int iNofTrailerBytes=0;
  char* searchKey=NULL;
  char* highlevelKey=NULL;
  int highlevelCmd=0;
  if (size>=2*sizeof(__u32) 
      && (*((__u32*)(pData+size-sizeof(__u32)))&CE_CMD_EM_MASK) 
         == CE_CMD_ENDMARKER) {
    // if the last word of the command block is an end-marker, this is exluded
    // from the payload sent to the indivifual handlers, this is due to 
    // commands which depent on the size of the payload and thus can not be
    // part of a command sequence
    iNofTrailerBytes=sizeof(__u32);
  }
  do {
    if (size<sizeof(__u32)) {
      CE_Error("unknown header format (less than 32bit)\n");
      iResult=-EPROTO;
    } else {
      if (pData!=buffer+iProcessed) {
	CE_Fatal("internal variable missmatch\n");
	iResult=-EFAULT;
	break;
      }
      __u32 u32header=0;
      if (size-iProcessed>=sizeof(__u32)) {
	u32header=*((__u32*)pData);
	CE_Debug("checking command header %#x\n", u32header);
	iResult=checkFeeServerCommand(u32header);
      } else {
	CE_Warning("truncated command header (less than 32bit)\n");
	iResult=0;
      }
      if (iResult==1) { // this is a FeeServer command
	// the complete command code
	__u32 cmd=u32header&(FEESERVER_CMD_MASK|FEESERVER_CMD_ID_MASK|FEESERVER_CMD_SUB_MASK);
	// truncated command code without sub id
	__u32 cmdId=u32header&(FEESERVER_CMD_MASK|FEESERVER_CMD_ID_MASK);
	// the parameter extracted from the header
	__u32 parameter=u32header&FEESERVER_CMD_PARAM_MASK;
	iProcessed+=sizeof(__u32);
	pData+=sizeof(__u32);
	switch (cmdId) {
	case FEESERVER_CE_CMD:
	  iResult=translateCeCommand(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
	case FEESVR_DCSC_CMD:
	  iResult=translateDcscCommand(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
	case FEESVR_CMD_RCU:
	  iResult=translateRcuCommand(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
	case FEESVR_CMD_SHELL:
	  iResult=translateShellCommand(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
	case FEESVR_SET_FERO_DATA:
	  iResult=setFeroData(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
	case FEESVR_CMD_DATA_RO:
	  iResult=dataReadout(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
#ifdef TPC
	case FEESVR_CMD_TPC:
	  iResult=translateTpcCommand(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
#endif //TPC
#ifdef PHOS
	case FEESVR_CMD_PHOS:
	  iResult=translatePhosCommand(cmd, parameter, pData, size-iProcessed-iNofTrailerBytes, rb);
	  break;
#endif //PHOS
	default:
	  CE_Warning("unrecognized command id (%#x)\n", cmdId);
	  iResult=-ENOSYS;
	}
	if (iResult>size-iProcessed-iNofTrailerBytes) {
	  CE_Error("command handler offset %d out of command block size %d\n", iResult, size-iProcessed);
	  iResult=-EPROTO;
	}else if (iResult>=0) {
	  CE_Debug("command %#x finished with result %d\n", u32header, iResult);
	  iProcessed+=iResult;
	  pData+=iResult;
	  iResult=0;
	  // check for the end-marker
	  __u32 u32tailer=*((__u32*)(pData));
	  if ((u32tailer&CE_CMD_EM_MASK) != CE_CMD_ENDMARKER) {
	    if (ceCheckOptionFlag(DEBUG_RELAX_CMD_CHECK)==0) {
	      CE_Error("missing end marker\n");
	      iResult=-EPROTO;
	    }
	  } else {
	    iProcessed+=sizeof(__u32);
	    pData+=sizeof(__u32);
	  }
	}
      } else if (iResult==0 && (iResult=checkMsgBufferCommand(u32header))==1) {
	// this is an encoded buffer
	CE_Warning("handling of encoded data blocks not yet implemented\n");
	pData+=size-iProcessed;
	iProcessed=size;
      } else if (iResult==0 
		 /* note: the assignment of searchKey is by purpose! */
		 && (((highlevelKey=strstr(pData, "<fee>"))!=NULL && (highlevelCmd=1) && (searchKey="</fee>")) ||
		     ((highlevelKey=strstr(pData, "<action>"))!=NULL && (highlevelCmd=2) && (searchKey="</action>"))
		     )
		 ) {
	char* pEndKey=NULL;
	if ((pEndKey=strstr(pData, searchKey))!=NULL) {
	  highlevelKey=strchr(highlevelKey, '>');
	  // remove all heading blanks
	  while (*(++highlevelKey)==' ' && (highlevelKey<(pData+size-iProcessed)));
	  // remove all trailing blanks
	  char* pEndCmd=pEndKey;
	  *pEndKey=0;
	  while (--pEndCmd>highlevelKey && (*pEndCmd==' ' || *pEndCmd==0)) *pEndCmd=0;
	  if (strlen(highlevelKey)>0) {
	    if (highlevelCmd==2) {
	      CE_Info("got action: %s\n", highlevelKey);
	      extern int actionHandler(const char* pAction);
	      iResult=actionHandler(highlevelKey);
	    } else {
	      CE_Info("got highlevel command: %s\n", highlevelKey);
	      extern int hlcHandler(const char* pAction);
	      iResult=hlcHandler(highlevelKey);
	    }
	  } else {
	    CE_Warning("empty %s ignored\n", highlevelCmd==1?"high-level command":"action");
	  }
	  if (iResult>=0) {
	    iResult=(int)pEndKey-(int)pData+strlen(searchKey);
	    if (size-iProcessed-iResult>=4) {
	      CE_Warning("stacking of high level commands not possible, remaining data is ignored\n");
	    }
	    pData+=size-iProcessed;
	    iProcessed=size;
	  }
	} else {
	  CE_Error("missing end-keyword for command buffer\n");
	  iResult=-EPROTO;
	}
      } else if (u32header==0) {
	// try the next word in the buffer
	pData+=sizeof(__u32);
	iProcessed+=sizeof(__u32);
      } else if (iResult==0) {
	CE_Error("unknown command header: %#x\n", u32header);
	iResult=-EPROTO;
      }
    }
  } while (iResult>=0 && size>iProcessed && bSingleCmd==0);
  if (iResult>=0) iResult=iProcessed;
  return iResult;
}

int hlcHandler(const char* pCommand) {
  int iResult=0;
  if (pCommand) {
    if (strstr(pCommand, "FEESVR_SET_FERO_DATA32")!=0) {
      int iParam=-1;
      const char* pServiceName=pCommand+strlen("FEESVR_SET_FERO_DATA32");
      while (*pServiceName!=0 && *pServiceName==' ') pServiceName++; // skip all blanks
      int iNameLen=0;
      while (pServiceName[iNameLen]!=0 && pServiceName[iNameLen]!=' ') iNameLen++; // jump over the service name
      iNameLen++;
      if (iNameLen>1) {
	int iBufferSize=((iNameLen-1)/sizeof(__u32))+1+1;
	__u32* cmdBuffer=new __u32[iBufferSize];
	if (cmdBuffer) {
	  if (sscanf(&pServiceName[iNameLen], "%d", &cmdBuffer[0])>0) {
	    cmdBuffer[iBufferSize-1]=0;
	    strncpy((char*)&cmdBuffer[1], pServiceName, iNameLen-1);
	    CEResultBuffer rb;
	    setFeroData(FEESVR_SET_FERO_DATA32, iNameLen, (const char*)cmdBuffer, iBufferSize*sizeof(__u32), rb);
	  } else {
	    CE_Error("error scanning high-level command %s\n", pCommand);
	    iResult=-EPROTO;
	  }
	  delete [] cmdBuffer;
	}
      } else {
	CE_Error("invalid service name in high-level command %s\n", pCommand);
	iResult=-EPROTO;
      }
    } else {
      CE_Error("unknown high-level command %s\n", pCommand);
      iResult=-EPROTO;
    }
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

int RCUce_Issue(char* command, char** result, int* size) {
  int iResult=0;
  if (command && size && *size!=0) {
    int iPrintSize=*size;
    if (*size>g_iMaxBufferPrintSize)
      iPrintSize=g_iMaxBufferPrintSize;
    if (iPrintSize>0) {
      printBufferHex((unsigned char*)command, iPrintSize, 4, "CE Debug: issue command buffer");
/*       char command_print[4]; */
/*       int i=0; */
/*       for (i=0; i<4; i++) { */
/* 	if (i<*size && command[i]>=0x20) command_print[i]=command[i]; */
/* 	else command_print[i]='.'; */
/*       } */
/*       CE_Debug("issue command buffer\n  %x %x %x %x    :  %c %c %c %c\n" */
/* 	       , command[0], command[1], command[2], command[3] */
/* 	       , command_print[0], command_print[1], command_print[2], command_print[3]); */
    }
    CEResultBuffer rb;
    rb.clear();
    iResult=translateCommand(command, *size, rb, 0);
    *size=0;
    *result=NULL;
    if (iResult>=0) {
      *size=rb.size()*sizeof(CEResultBuffer::value_type);
      if (*size>0) {
	*result=(char*)malloc(*size);
	if (*result) {
	  //CE_Info("result buffer of size %d", *size);
	  memcpy(*result, &(rb[0]), *size);
	  if (*size>g_iMaxBufferPrintSize)
	    iPrintSize=g_iMaxBufferPrintSize;
	  if (iPrintSize>0)
	    printBufferHex((unsigned char*)*result, iPrintSize, 4, "CE Debug: issue result buffer");
	} else {
	  CE_Error("can not allocate result memory\n");
	}
      }
    } else {
    }
  } else {
  }
  return 0;
}

/*******************************************************************************
 * helper functions
 */

int MASK_SHIFT(int val, int mask) {
  int iResult=val&mask;
  int shift=0;
  while (((mask>>shift)&0x1)==0 && (mask>>shift)>0) shift++;
  iResult>>=shift;
  return iResult;
}

/* check if the provided header represents FeeServer command or not
 * result: 1 yes, 0 no
 */
int checkFeeServerCommand(__u32 header) {
  if ((header&FEESERVER_CMD_MASK)==FEESERVER_CMD) return 1;
  return 0;
}

/* check if the provided header represents a Msg Buffer command or not
 * result: 1 yes, 0 no
 */
int checkMsgBufferCommand(__u32 header) {
  __u32 masked=header&FEESERVER_CMD_MASK;
  switch (masked) {
    // all previous firmware versions are not supported as direct commands
  case MSGBUF_VERSION_2:    
  case MSGBUF_VERSION_2_2:
    return 1;
  }
  return 0;
}
/* extract command id from the provided header
 * result: 0 - 15 command id
 *         -1 no FeeServer command
 */
int extractCmdId(__u32 header) {
  if (checkFeeServerCommand(header)) return -1;
  return ((header&FEESERVER_CMD_ID_MASK)>>FEESERVER_CMD_ID_BITSHIFT);
}

/* extract command sub id from the provided header
 * result: 0 - 255 command id
 *         -1 no FeeServer command
 */
int extractCmdSubId(__u32 header) {
  if (checkFeeServerCommand(header)) return -1;
  return ((header&FEESERVER_CMD_SUB_MASK)>>FEESERVER_CMD_SUB_BITSHIFT);
}

