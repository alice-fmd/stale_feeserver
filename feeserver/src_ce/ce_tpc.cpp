// $Id: ce_tpc.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter,
** Please report bugs to Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

/***************************************************************************
 * ce_tpc.c
 * this file implements the specific methods for the TPC
 */

#include <errno.h>
#include <stdlib.h>       // malloc, free,
#include <string.h>       // memcpy
#include "fee_errors.h"
#include "ce_command.h"   // CE API
#include "codebook_rcu.h" // the mapping of the rcu memory
#include "dcscMsgBufferInterface.h" // access library to the dcs board message buffer interface
#include "ce_base.h"
#include "ce_tpc.h"
#include "device.hpp"     // CEResultBuffer

#ifdef TPC

int TPCinitializeCE() {
  int iResult=CE_OK;
  return iResult;
}

int TPCcleanUpCE() {
  int iResult=CE_OK;
  return iResult;
}

/**
 * Issue handler for TPC commands.
 * specific command translation for the TPC Control Engine
 * see rcu_issue.h for command ids and for further information
 * @param cmd       complete command id (upper 16 bit of 4 byte header)
 * @param parameter command parameter extracted from the 4 byte header (lower 16 bit)
 * @param pData     pointer to the data following the header
 * @param iDataSize size of the data in bytes
 * @return: number of bytes in the result buffer if successful
 *    <0 : error code
 * @ingroup rcu_ce_tpc
 */
int translateTpcCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb)
{
  int iResult=0;
  createLogMessage(MSG_WARNING,"not yet implemented","CE::translateTpcCommand");
  CE_Warning("function not yet implemented\n");
  iResult=-ENOSYS;
  return iResult;
}

#endif //TPC
