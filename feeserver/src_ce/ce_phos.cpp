// $Id: ce_phos.cpp,v 1.1.1.1 2006-12-27 15:39:04 hehi Exp $

/************************************************************************
**
**
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

/***************************************************************************
 * ce_phos.c
 * this file implements the specific methods for the PHOS
 */

#ifdef PHOS
#include <errno.h>
#include <stdlib.h>       // malloc, free,
#include <string.h>       // memcpy
#include <stdio.h>
#include "fee_errors.h"
#include "ce_command.h"   // CE API
#include "codebook_rcu.h" // the mapping of the rcu memory
#include "dcscMsgBufferInterface.h" // access library to the dcs board message buffer interface
#include "ce_phos.h"
#include "rcu_service.h"
#include "ce_base.h"
#include "device.hpp"     // CEResultBuffer

/**
 * <i>Set</i> function for APD channels.
 * The 'set' function is of type int (*ceSetFeeValue)(float, int, int, void*)<br>
 * The function pointer is passed to the CE service framework during service
 * registration. The function handler is called, Whenever a 
 * @ref FEESVR_SET_FERO_DATA command for the service is received, 
 * @see ceSetFeeValue
 * @see rcu_ce_base_services
 * @ingroup rcu_ce_phos
 */
int PHOSsetAPD(float value, int major, int minor, void* parameter) 
{
  int iResult=CE_OK;
  // implement the hardware access here
  CE_Info("set APD value for FEC %d, channel %d: %f\n", minor, major, value);
  CE_Warning("hardware access not yet implemented\n");
  return iResult;
}

/**
 * <i>Update</i> function for APD channels.
 * The 'update' function is of type int (*ceUpdateService)(float*, int, int, void*)<br>
 * The function pointer is passed to the CE service framework during service
 * registration. The function handler is called, whenever an update of the value of the
 * published data is requested. This is done periodically by the framework.
 * @see ceSetFeeValue
 * @see rcu_ce_base_services
 * @ingroup rcu_ce_phos
 */
int PHOSupdateAPD(float* pValue, int major, int minor, void* parameter) 
{
  int iResult=CE_OK;
  if (pValue) {
    // since RegisterServiceGroup was used to register all the apd services for one FEC,
    // major represents the channel no and minor the FEC no
    if (RCUisFECactive(minor)) { // test whether the FEC is currently active
      // implement the hardware access here
      //if (*pValue>1000 || *pValue<0) *pValue=1; 
      //else *pValue+=1;
    } else {
      // non active services have to be set to -2000
      *pValue=-2000;
    }
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

int PHOSinitializeCE() {
  int iResult=CE_OK;
  int i=0;
  char name[30]="";
  float defaultDeadband=1.0;
  /* registration of the FECs is done separately for the two branches in order
   * to set appropriate service names
   * NOTE: the services are registered with the 'global' address of the FEC
   */
  for (i=0; i<NOF_FEC_BRANCH_A && iResult<=0; i++) {
    if (RCUisFECvalid(FEC_BRANCH_A_TO_GLOBAL(i))) {
      sprintf(name, "FEC_A%02d_APD", i);
      iResult=RegisterServiceGroup(name, numberOfAPDchannels, defaultDeadband, PHOSupdateAPD, PHOSsetAPD, FEC_BRANCH_A_TO_GLOBAL(i), NULL); 
    }
  }
  for (i=0; i<NOF_FEC_BRANCH_B && iResult<=0; i++) {
    if (RCUisFECvalid(FEC_BRANCH_B_TO_GLOBAL(i))) {
      sprintf(name, "FEC_B%02d_APD", i);
      iResult=RegisterServiceGroup(name, numberOfAPDchannels, defaultDeadband, PHOSupdateAPD, PHOSsetAPD, FEC_BRANCH_B_TO_GLOBAL(i), NULL); 
    }
  }
  return iResult;
}

int PHOScleanUpCE() {
  int iResult=CE_OK;
  return iResult;
}

/**
 * Issue handler for PHOS commands.
 * specific command translation for the PHOS Control Engine
 * see rcu_issue.h for command ids and for further information
 * @param cmd       complete command id (upper 16 bit of 4 byte header)
 * @param parameter command parameter extracted from the 4 byte header (lower 16 bit)
 * @param pData     pointer to the data following the header
 * @param iDataSize size of the data in bytes
 * @return: number of bytes in the result buffer if successful
 *    <0 : error code
 * @ingroup rcu_ce_phos
 */
int translatePhosCommand(__u32 cmd, __u32 parameter, const char* pData, int iDataSize, CEResultBuffer& rb)
{
  int iResult=0;
  createLogMessage(MSG_WARNING,"not yet implemented","CE::translatePhosCommand");
  CE_Warning("not yet implemented\n");
  iResult=-ENOSYS;
  return iResult;
}

#endif //PHOS
