#ifndef FEE_DEFINES_H
#define FEE_DEFINES_H

#include "fee_loglevels.h"

//-----------------------------------------------------------------------------
// Definition of globals for the feeserver, control engine, tests etc.
//
// @date        2003-05-06
// @author      Christian Kofler, Sebastian Bablok
//-----------------------------------------------------------------------------

/**
 * version of the current FeeServer
 * @ingroup feesrv_core
 */
#define FEESERVER_VERSION "0.7.6"

/**
 * state of the FeeServer: collecting service-items
 * @ingroup feesrv_core
 */
#define COLLECTING 1

/**
 * state of the FeeServer: server is running
 * @ingroup feesrv_core
 */
#define RUNNING 2

/**
 * state of FeeServer is running, but CE failed to initialize. Now it is only
 * accepting commands for the FeeServer itself, no monitoring.
 * @ingroup feesrv_core
 */
#define ERROR_STATE 3

/**
 * TAG define, identifying the ACK service.
 * @ingroup feesrv_core
 */
#define ACK_SERVICE_TAG 321

/**
 * Timeout, the FeeServer is waiting to initialize the CE (in sec).
 * NOTE: This time spread is added to the value of TIMEOUT_INIT_CE_MSEC !
 * This value has to be lower than 4294 to prevent buffer overflows.
 * @ingroup feesrv_core
 */
#define TIMEOUT_INIT_CE_SEC 30

/**
 * Timeout, the FeeServer is waiting to initialize the CE (in ms).
 * NOTE: This time spread is added to the value of TIMEOUT_INIT_CE_SEC !
 * This value has to be lower than 4294 to prevent buffer overflows.
 * Best keep it below 1000 (some machines have problems with larger numbers).
 * @ingroup feesrv_core
 */
#define TIMEOUT_INIT_CE_MSEC 100

/**
 * Timeout; the amount of milliseconds, the FEE-server is waiting for the return
 * of the issue-function (in ms). This value has to be lower than
 * MAX_ISSUE_TIMEOUT (4294967) !
 * @ingroup feesrv_core
 */
#define DEFAULT_ISSUE_TIMEOUT 60000

/**
 * Maximum issue timeout value to prevent buffer overflows.
 * @ingroup feesrv_core
 */
#define MAX_ISSUE_TIMEOUT 4294967

/**
 * Default update rate for check of Item list in ms.
 * @ingroup feesrv_core
 */
#define DEFAULT_UPDATE_RATE 1000

/**
 * This value multiplied with the deadband checker updateRate and the amount
 * of nodes in the service list defines the time amount, after that each
 * service is at least updated once. This multiplier is used to enlargen the
 * time interval, if needed.
 * @ingroup feesrv_core
 */
#define TIME_INTERVAL_MULTIPLIER 10000


/**
 * Default deadband size for monitored values.
 * !!! has to be defined, check it with TPC and TRD !!!
 * @ingroup feesrv_core
 */
//#define DEFAULT_DEADBAND 6.0  // is now set for each item individually

/**
 * The default LogLevel of the FeeServer set on start up (MSG_ALARM has to be
 * set always!).
 * @ingroup feesrv_core
 */
#define DEFAULT_LOGLEVEL (MSG_WARNING + MSG_ERROR + MSG_ALARM + MSG_INFO + MSG_SUCCESS_AUDIT)

/**
 * This define sets the flag of a FeeProperty to inform about a update rate.
 * @ingroup feesrv_core
 */
#define PROPERTY_UPDATE_RATE 1

/**
 * FeePacket header size in bytes
 * @ingroup feesrv_core
 */
#define HEADER_SIZE 12

/**
 * The size of the ID - field in the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_SIZE_ID 4

/**
 * The size of the ErrorCode - field in the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_SIZE_ERROR_CODE 2

/**
 * The size of the Flags - field in the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_SIZE_FLAGS 2

/**
 * The size of the Checksum - field in the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_SIZE_CHECKSUM 4

/**
 * The offset - size of the ID - field in the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_OFFSET_ID 4

/**
 * The offset - size of (ID - field + errorCode - field) in the command header.
 * (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_OFFSET_ERROR_CODE 6

/**
 * The offset - size of (ID + errorCode + reserved + flags - field) in
 * the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_OFFSET_FLAGS 8

/**
 * The offset - size of (ID + errorCode + reserved + flags + checksum - field)
 * in the command header. (in bytes)
 * @ingroup feesrv_core
 */
#define HEADER_OFFSET_CHECKSUM 12

/**
 * Define for the Adler base; largest prime number smaller than 2^16 (65536).
 * Used by the checksum algorithm.
 * @ingroup feesrv_core
 */
#define ADLER_BASE 65521

/**
 * Define for the local detector (for now TRD or TPC possible...)
 * ZTT is the debug and "testing detector"
 * @ingroup feesrv_core
 */
#ifdef TPC
#define LOCAL_DETECTOR "tpc\0"
#else
#ifdef TRD
#define LOCAL_DETECTOR "trd\0"
#else
#define LOCAL_DETECTOR "ZTT\0"
#endif
#endif


#endif

