#ifndef FEE_TYPES_H
#define FEE_TYPES_H

#include "fee_loglevels.h"
//-----------------------------------------------------------------------------
// Definition of datatypes for the feeserver, control engine, tests etc.
//
// @date	2003-05-06
// @author	Christian Kofler, Sebastian Bablok
//-----------------------------------------------------------------------------


/**
 * Typedef Item.
 * Item is a struct, with a pointer to the item value (volatile double* location)
 * and the item name (char* name). This represents the core of an item. In
 * addition a default dead band is provided.
 * @ingroup feesrv_core
 */
typedef struct {
	/** struct value location -> pointer to the actual value of the item.*/
	volatile float* location;
	/** struct value name -> name of item.*/
	char* name;
	/** struct value defaultDeadband -> default deadband of this item */
	float defaultDeadband;
} Item; /**< The item which represents a service. */


/**
 * Typedef FeeProperty.
 * This struct is used to inform the CE about a property change in the
 * FeeServer. It provides a flag, identifying, which kind of property and the
 * property value itself. For now, only the update rate of the deadband check
 * is forseen to be signaled.
 * @ingroup feesrv_core
 */
typedef struct {
	/** struct value flag -> the flag identifying the property.*/
	unsigned short flag;
	/**
	* struct value uShortVal -> stores the value of an unsigned short
	* property.
	*/
	unsigned short uShortVal;
} FeeProperty; /**< The FeeProperty, to signal a value change.*/


/**
 * Typedef ItemNode.
 * ItemNode is a struct for building a doubly linked list of Items and
 * corresponding information.
 * @ingroup feesrv_core
 */
typedef struct Node{
	/** struct value prev -> pointer to previous ItemNode.*/
	struct Node* prev;
	/** struct value next -> pointer to next ItemNode.*/
	struct Node* next;
	/** struct value id -> DIM-ServiceID (used by the DIM-framework).*/
	unsigned int id;
	/** struct value item -> pointer to the Item.*/
	Item* item;

	/**
	 * struct value lastTransmittedValue -> contains the last sent value of this
	 * node's item.
	 */
	float lastTransmittedValue;
	/**
	 * struct value threshold -> contains the deadband size for this item
	 * divided by 2. The dead band is around the lastTransmittedValue, the
	 * threshold can be used, because it is equal to both sides.
	 */
	float threshold;

	/**
	 * This is a backup of the location (address) for the item of this node.
	 */
	volatile float* locBackup;
	 /**
	  * Checksum of the location, to detect bit flips in "location".
	  */
	unsigned int checksum;
	/**
	 * Checksum backup to be able to verify original checksum.
	 */
	unsigned int checksumBackup;

	//AlarmCond*
} ItemNode; /**< ItemNode is a node of the local doubly linked list. */


/**
 * Typedef IssueStruct.
 * IssueStruct contains pointer to the datatypes used by the issue-function.
 * This struct is mainly used for data-exchange between command handler and
 * issue-thread.
 *
 * @see issue()
 * @ingroup feesrv_core
 */
typedef struct {
	/**
	 * struct value nRet -> contains the return value of the issue-function.
	 * (FEE_OK or error code).
	 */
	int nRet;
	/** struct value command -> pointer to the command-data (see issue-function). */
	char* command;
	/** struct value result -> pointer to the result-data (see issue-function). */
	char* result;
	/**
	 * struct value size -> the size of command-data for input and the size of the
	 * result-data for ouput (see issue-function).
	 */
	int size;
} IssueStruct;

/**
 * Typedef MessageStruct.
 * MessageStruct contains the data fields provided by the message service.
 *
 * @see sendMessage()
 * @ingroup feesrv_core
 */
typedef struct {
	/**
	 * struct value eventType -> contains the (log) level type of the current
	 * message. (info, warning, error , ...)
	 *
	 * @see fee_loglevels.h for more details.
	 */
	unsigned int eventType;
	/** struct value detector -> the detector, which triggered this message. */
	char detector[MSG_DETECTOR_SIZE];
	/**
	 * struct value source -> the source (area, eg: FeeServer or CE), where the
	 * event occured.
	 */
	char source[MSG_SOURCE_SIZE];
	/** struct value description -> the actual message of the event. */
	char description[MSG_DESCRIPTION_SIZE];
	/** struct value date -> the date, when the event occured. */
	char date[MSG_DATE_SIZE];
} MessageStruct;

/**
 * Typedef FlagBits
 * 2 byte field for flag bits in FeePacket header.
 * @ingroup feesrv_core
 */
typedef unsigned short FlagBits;

/**
 * Typedef CommandHeader.
 * CommandHeader contains all elements of a FeePacket header as members of the
 * struct.
 * @ingroup feesrv_core
 */
typedef struct {
	/** identification number of the command (used in the FeePacket header) */
	unsigned int id;
	/** the error code - see fee_errors.h for details! */
	short errorCode;
	/**
	 * The flags of the command, such as "HuffmannEncoded" and "CheckSum" and
	 * all other commands for the FeeServer itself.
	 */
	FlagBits flags;
	/** the checksum of the command */
	int checksum;
} CommandHeader;

#endif
