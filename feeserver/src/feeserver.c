#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>				// for pause() necessary
#include <string.h>
#include <dim/dis.h>					// dimserver library

#include <time.h>					// time for threads
#include <sys/time.h>			// for gettimeofday()
#include <pthread.h>
#include <stdbool.h>
#include <errno.h>      			// for the error numbers
#include <signal.h>

#include "fee_types.h"			// declaration of own datatypes
#include "fee_functions.h"		// declaration of feeServer functions
#include "fee_defines.h"			// declaration of all globaly used constants
#include "feepacket_flags.h"	// declaration of flag bits in a feepacket
#include "fee_errors.h"			// defines of error codes
#include "ce_command.h"		//control engine header file

#ifdef __UTEST
#include "fee_utest.h"
#endif

/**
 * @defgroup feesrv_core The FeeServer core
 */

//-- global variables --

/**
 * state of the server, possible states are: COLLECTING, RUNNING and ERROR_STATE.
 * @ingroup feesrv_core
 */
static int state = COLLECTING;

/**
 * indicates, if CEReady has been signaled (used in backup solution of init watch dog).
 * @ingroup feesrv_core
 */
static bool ceReadySignaled = false;

/**
 * Variable provides the init state of the CE.
 * @ingroup feesrv_core
 */
static int ceInitState = CE_OK;

/**
 * pointer to the first ItemNode of the doubly linked list
 * @ingroup feesrv_core
 */
static ItemNode* firstNode = 0;

/**
 * pointer to the last ItemNode of the doubly linked list
 * @ingroup feesrv_core
 */
static ItemNode* lastNode = 0;

/**
 * The message struct providing the data for an event message.
 * @ingroup feesrv_core
 */
static MessageStruct message;

/**
 * Stores the number of added nodes to the item list.
 * @ingroup feesrv_core
 */
static unsigned int nodesAmount = 0;

/**
 * DIM-serviceID for the dedicated acknowledge-service
 * @ingroup feesrv_core
 */
static unsigned int serviceACKID;

/**
 * DIM-serviceID for the dedicated message - service
 * @ingroup feesrv_core
 */
static unsigned int messageServiceID;

/**
 * DIM-commandID
 * @ingroup feesrv_core
 */
static unsigned int commandID;

/**
 * Pointer to acknowledge Data (used by the DIM-framework)
 * @ingroup feesrv_core
 */
static char* cmndACK = 0;

/**
 * size of the acknowledge Data
 * @ingroup feesrv_core
 */
static int cmndACKSize = 0;

/**
 * Name of the FeeServer
 * @ingroup feesrv_core
 */
static char* serverName = 0;

/**
 * length of FeeServer name
 * @ingroup feesrv_core
 */
static int serverNameLength = 0;

/**
 * Update rate, in which the whole Item-list should be checked for changes.
 * This value is given in milliseconds.
 * @ingroup feesrv_core
 */
static unsigned short updateRate = DEFAULT_UPDATE_RATE;

/**
 * Timeout for call of issue - the longest time a command can be executed by the CE,
 * before the watch dog kills this thread. This value is given in milliseconds.
 * @ingroup feesrv_core
 */
static unsigned long issueTimeout = DEFAULT_ISSUE_TIMEOUT;

/**
 * Stores the current log level for this FeeServer.
 * In case that an environmental variable (FEE_LOG_LEVEL) tells the desired
 * loglevel, the DEFAULT_LOGLEVEL is overwritten during init process.
 * @ingroup feesrv_core
 */
static unsigned int logLevel = DEFAULT_LOGLEVEL;

/**
 * thread handle to the initialize thread
 * @ingroup feesrv_core
 */
static pthread_t thread_init;

/**
 * thread handle to the monitoring thread
 * @ingroup feesrv_core
 */
static pthread_t thread_mon;

/**
 * thread condition variable for the "watchdog" timer
 * @ingroup feesrv_core
 */
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

/**
 * thread condition variable for the "initialisation complete" - signal
 * @ingroup feesrv_core
 */
static pthread_cond_t init_cond = PTHREAD_COND_INITIALIZER;

/**
 * thread mutex variable for the "watchdog" in command handler
 * @ingroup feesrv_core
 */
static pthread_mutex_t wait_mut = PTHREAD_MUTEX_INITIALIZER;

/**
 * thread mutex variable for the initialize CE thread
 * @ingroup feesrv_core
 */
static pthread_mutex_t wait_init_mut = PTHREAD_MUTEX_INITIALIZER;

/**
 * thread mutex variable for the commandAck data
 * @ingroup feesrv_core
 */
static pthread_mutex_t command_mut = PTHREAD_MUTEX_INITIALIZER;

/**
 * mutex to lock access to the logging function ( createLogMessage() )
 * @ingroup feesrv_core
 */
static pthread_mutex_t log_mut = PTHREAD_MUTEX_INITIALIZER;



//-- Main --

/**
 * Main of FeeServer.
 * This programm represents the DIM-Server running on the DCS-boards.
 * It uses the DIM-Server-Library implemented by C. Gaspar from Cern.
 *
 * @author Christian Kofler, Sebastian Bablok
 *
 * @date 2003-04-24
 *
 * @update 2004-11-22
 *
 * @version 0.7
 * @ingroup feesrv_core
 */
int main(int argc, char** arg) {
	//-- only for unit tests
#	ifdef __UTEST
	// insert here the testfunction-calls
	testFrameWork();
	return 0;
#	endif

	// now here starts the real stuff
	initialize();
	// test server (functional test)
	while (1) {
		// maybe do some checks here, like:
		// - monitoring thread is still in good state
		// - CE is still in good state
		// - everything within the FeeServer is OK (assertions?)
		pause();
	}
	return 0;
}


void initialize() {
	//-- Declaring variables --
	struct timeval now;
	struct timespec timeout;
	pthread_attr_t attr;
	int nRet;
	int status;
	int initState  = FEE_CE_NOTINIT;
	char* name = 0;
	char* dns = 0;
	bool initOk = true;
	unsigned int envVal = 0;

	//-- register interrupt handler (CTRL-C)
	// not used yet, causes problems
//	if (signal(SIGINT, interrupt_handler) == SIG_ERR) {
//#		ifdef __DEBUG
//		printf("Unable to register interrupt handler.\n");
//		printf("This is not fatal -> continuing.\n");
//#		endif
//	}

	//-- get name of the server --
	name = getenv("FEE_SERVER_NAME");
	if (name == 0) {
#		ifdef __DEBUG
		printf("No FEE_SERVER_NAME \n");
#		endif
		exit(202);
	}

	serverName = (char*) malloc(strlen(name) + 1);
	if (serverName == 0) {
		//no memory available!
#		ifdef __DEBUG
		printf("no memory available while trying to create server name!\n");
#		endif
		exit(201);
	}
	strcpy(serverName, name);
	serverNameLength = strlen(serverName);

	//-- test, if DIM_DNS_NODE is specified
	dns = getenv("DIM_DNS_NODE");
	if (dns == 0) {
#		ifdef __DEBUG
		printf("No DIM_DNS_NODE specified. \n");
#		endif
		exit(203);
	}

	// set the desired log level, if provided
	if (getenv("FEE_LOG_LEVEL")) {
		sscanf(getenv("FEE_LOG_LEVEL"), "%d", &envVal);
		if ((envVal < 0) || (envVal > MSG_MAX_VAL)) {
#		    ifdef __DEBUG
			printf("Environmental variable has invalid Log Level, using default instead.\n");
		   	fflush(stdout);
#			endif
		} else {
			logLevel = envVal | MSG_ALARM;
		}
	}

# 	ifdef __DEBUG
	printf("\n  **  FeeServer version %s  ** \n\n", FEESERVER_VERSION);
	printf("FeeServer name: %s\n", serverName);
	printf("Using DIM_DNS_NODE: %s\n", dns);
#   ifdef __BENCHMARK
    printf(" -> Benchmark version of FeeServer <- \n");
#	endif
	printf("Current log level is: %d (MSG_ALARM (%d) is always on)\n", logLevel, MSG_ALARM);
#	endif

	//set dummy exit_handler to disable framework exit command, returns void
	dis_add_exit_handler(&dim_dummy_exit_handler);

	//set error handler to catch DIM framework messages
	dis_add_error_handler(&dim_error_msg_handler);

	// to ensure that signal is in correct state before init procedure
	ceReadySignaled = false;

	// lock mutex
	status = pthread_mutex_lock(&wait_init_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Lock init mutex error: %d\n", status);
		fflush(stdout);
#		endif
		initOk = false;
	} else {
		// initiailisation of thread attribute only if mutex has been locked
		status = pthread_attr_init(&attr);
		if (status != 0) {
#			ifdef __DEBUG
			printf("Init attribute error: %d\n", status);
			fflush(stdout);
#			endif
			initOk = false;
		} else {
			status = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
			if (status != 0) {
#				ifdef __DEBUG
				printf("Set attribute error: %d\n", status);
				fflush(stdout);
#				endif
				initOk = false;
			}
		}
	}

	if (initOk == true) {
		// call only if initOk == true,
		status = pthread_create(&thread_init, &attr, &threadInitializeCE, 0);
		if (status != 0) {
#			ifdef __DEBUG
			printf("Create thread error: %d\n", status);
			fflush(stdout);
#			endif
			initState = FEE_CE_NOTINIT;
		} else {
			// timeout set in ms, should be enough for initialisation; see fee_defines.h for current value
			status = gettimeofday(&now, 0);
			if (status != 0) {
				// backup solution for detetcting end of init process
#				ifdef __DEBUG
				printf("Get time of day error: %d, using backup solution\n", status);
				fflush(stdout);
#				endif
				// unlock mutex to enable functionality of signalCEReady
				status = pthread_mutex_unlock(&wait_init_mut);
#				ifdef __DEBUG
				if (status != 0) {
					printf("Unlock mutex error: %d\n", status);
					fflush(stdout);
				}
#				endif
				// sleep init-timeout length
				usleep((TIMEOUT_INIT_CE_MSEC * 1000) +
						(TIMEOUT_INIT_CE_SEC * 1000000));
				if (ceReadySignaled == false) {
					status = pthread_cancel(thread_init);
#					ifdef __DEBUG
					if (status != 0) {
						printf("No thread to cancel: %d\n", status);
						fflush(stdout);
					}
#					endif
					// start with "the CE is not initialized!"
					initState = FEE_CE_NOTINIT;
#					ifdef __DEBUG
					printf("Timeout in init\n");
					fflush(stdout);
#					endif
				} else {
					if (ceInitState != CE_OK) {
						// init failed, but no timeout occured
						// (insufficient memory, etc. ... or something else)
#						ifdef __DEBUG
						printf("Init of CE failed, error: %d\n", ceInitState);
						fflush(stdout);
#						endif
						initState = FEE_CE_NOTINIT;
					} else {
						// start with "everything is fine"
						initState = FEE_OK;
#						ifdef __DEBUG
						printf("Init OK\n");
						fflush(stdout);
#						endif
					}
				}
			} else {
				timeout.tv_sec = now.tv_sec + TIMEOUT_INIT_CE_SEC;
				timeout.tv_nsec = (now.tv_usec * 1000) +
						(TIMEOUT_INIT_CE_MSEC * 1000000);

				// wait for finishing "issue" or timeout after the mutex is unlocked
				// a retcode of 0 means, that pthread_cond_timedwait has returned
				// with the cond_init signaled
				status = pthread_cond_timedwait(&init_cond, &wait_init_mut, &timeout);
				// -- start FeeServer depending on the state of the CE --
				if (status != 0) {
					status = pthread_cancel(thread_init);
#					ifdef __DEBUG
					if (status != 0) {
						printf("No thread to cancel: %d\n", status);
						fflush(stdout);
					}
#					endif
					// start with "the CE is not initialized!"
					initState = FEE_CE_NOTINIT;
#					ifdef __DEBUG
					printf("Timeout in init\n");
					fflush(stdout);
#					endif
				} else {
					if (ceInitState != CE_OK) {
						// init failed, but no timeout occured
						// (insufficient memory, etc. ... or something else)
#						ifdef __DEBUG
						printf("Init of CE failed, error: %d\n", ceInitState);
						fflush(stdout);
#						endif
						initState = FEE_CE_NOTINIT;
					} else {
						// start with "everything is fine"
						initState = FEE_OK;
#						ifdef __DEBUG
						printf("Init OK\n");
						fflush(stdout);
#						endif
					}
				}
			}
		}
		// destroy thread attribute
		status = pthread_attr_destroy(&attr);
#		ifdef __DEBUG
		if (status != 0) {
			printf("Destroy attribute error: %d\n", status);
			fflush(stdout);
		}
#		endif
	}

	// init message struct -> FeeServer name, version and DNS are also provided
	initMessageStruct();

	if (initState != FEE_OK) {
		// remove all services of Items of ItemList
#		ifdef __DEBUG
		printf("Init failed, unpublishing item list\n");
		fflush(stdout);
#		endif
		unpublishItemList();
	}

	// look through watchdog and backup souliton about ceInitState and check it again !!!
	// afterwards the following line won't be necessary !!!
	// needed later in information about properties !!!
	ceInitState = initState;
	// add div. services and the command channel and then start DIM server
	nRet = start(initState);

	// unlock mutex
	status = pthread_mutex_unlock(&wait_init_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Unlock mutex error: %d\n", status);
		fflush(stdout);
#		endif
		if (nRet == FEE_OK) {
			createLogMessage(MSG_WARNING, "Unable to unlock init mutex.", 0);
		}
	}

	if (nRet != FEE_OK) {
#		ifdef __DEBUG
		printf("unable to start DIM server, exiting.\n");
		fflush(stdout);
#		endif
		fee_exit_handler(205);
	} else {
#		ifdef __DEBUG
		printf("DIM Server successfully started, ready to accept commands.\n");
		fflush(stdout);
#		endif
	}

	return;
}


void* threadInitializeCE(void* none) {
	int status;
	status = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
	// if cancelation is not able, it won't hurt ?!
#	ifdef __DEBUG
	if (status != 0) {
		printf("Set cancel state (init) error: %d\n", status);
		fflush(stdout);
	}
#	endif

	status = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);
	// if cancelation is not able, it won't hurt ?!
#	ifdef __DEBUG
	if (status != 0) {
		printf("Set cancel type (init) error: %d\n", status);
		fflush(stdout);
	}
#	endif

	// Here starts the actual CE
	initializeCE();

	// not necessary, return 0 is better
//	pthread_exit(0);
	return 0;

}


void signalCEready(int ceState) {
	int status = -1;

	// set cancel type to deferred
	status = pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, 0);
#   ifdef __DEBUG
	if (status != 0) {
		printf("Set cancel type error: %d\n", status);
	    fflush(stdout);
	}
#   endif

	//lock the mutex before broadcast
	status = pthread_mutex_lock(&wait_init_mut);
#	ifdef __DEBUG
	if (status != 0) {
		printf("Lock mutex error: %d\n", status);
		fflush(stdout);
	}
#	endif

	// provide init state of CE
	ceInitState = ceState;

	//signal that CE has completed initialisation
	// maybe try the call pthread_cond_signal instead for performance
	pthread_cond_broadcast(&init_cond);

	// set variable for backup solution
	ceReadySignaled = true;

	// unlock mutex
	status = pthread_mutex_unlock(&wait_init_mut);
#	ifdef __DEBUG
	if (status != 0) {
		printf("Unlock mutex error: %d\n", status);
		fflush(stdout);
	}
#	endif

    // set cancel type to asyncroneous
    status = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);
#   ifdef __DEBUG
    if (status != 0) {
        printf("Set cancel type error: %d\n", status);
		fflush(stdout);
    }
#   endif
}


// -- Command handler routine --
void command_handler(int* tag, char* address, int* size) {
	struct timeval now;
	struct timespec timeout;
	int retcode  = -1;
	int status = -1;
	pthread_t thread_handle;
	pthread_attr_t attr;
	IssueStruct issueParam;
	CommandHeader header;
	char* pHeaderStream = 0;

#ifdef __BENCHMARK
	char benchmsg[200];
	// make benchmark entry
	if ((size != 0 ) && (*size >= 4)) {
		benchmsg[sprintf(benchmsg, 
				"FeeServer CommandHandler (Received command) - Packet-ID: %d",
				*address)] = 0;
		createBenchmark(benchmsg);
	} else {
		createBenchmark("FeeServer CommandHandler (Received command)");
	}
#endif

	// init struct
	initIssueStruct(&issueParam);

	issueParam.nRet = FEE_UNKNOWN_RETVAL;

	// check state (ERROR state is allowed for FeeServer commands, not CE)
	if ((state != RUNNING) && (state != ERROR_STATE)) {
		return;
	}

	// lock command mutex to save command &ACK data until it is send
	// and only one CE-Thread exists at one time
	status = pthread_mutex_lock(&command_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Lock command mutex error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING, "Unable to lock command mutex.", 0);
	}

	if ((tag == 0) || (address == 0) || (size == 0)) {
		leaveCommandHandler(0, FEE_NULLPOINTER, MSG_WARNING,
 				"Received null pointer of DIM framework in command handler.");
		return;
	}

	if (*size < HEADER_SIZE) {
		leaveCommandHandler(0, FEE_INVALID_PARAM, MSG_WARNING,
 				"FeeServer received corrupted command.");
		return;
	}

#	ifdef __DEBUG
	printf(" Cmnd - Size: %d\n", *size);
	fflush(stdout);
#	endif

	//-- storing the header information in struct --
	memcpy(&header.id, address, HEADER_SIZE_ID);
	memcpy(&header.errorCode, address + HEADER_OFFSET_ID, HEADER_SIZE_ERROR_CODE);
	memcpy(&header.flags, address + HEADER_OFFSET_ERROR_CODE, HEADER_SIZE_FLAGS);
	memcpy(&header.checksum, address + HEADER_OFFSET_FLAGS, HEADER_SIZE_CHECKSUM);

	// --------------------- Check Flags --------------------------
	if ((header.flags & HUFFMAN_FLAG) != 0) {
		//-- do Huffmann decoding if flag is set --
		// not implemented yet !!!
	}

	issueParam.size = *size - HEADER_SIZE;
	issueParam.command = (address + HEADER_SIZE);
	// !!! if Huffman decoding necessary, think about memory management ???

	if ((header.flags & CHECKSUM_FLAG) != 0) {
		//-- do checksum test if flag is set --
		if (!checkCommand(issueParam.command, issueParam.size, header.checksum)) {
			// -- checksum failed - notification
			leaveCommandHandler(header.id, FEE_CHECKSUM_FAILED, MSG_WARNING,
 					"FeeServer received corrupted command data (checksum failed).");
			return;
		}
	}

	// -- here start the Commands for the FeeServer itself --
	if ((header.flags & FEESERVER_UPDATE_FLAG) != 0) {
#ifdef ENABLE_MASTERMODE
		updateFeeServer(&issueParam);
#else
		createLogMessage(MSG_WARNING, "FeeServer is not authorized to execute shell programs, skip ...", 0);
#endif //ENABLE_MASTERMODE
		// this is only reached, if update has not been sucessful
		issueParam.nRet = FEE_FAILED;
		issueParam.size = 0;
	} else if ((header.flags & FEESERVER_RESTART_FLAG) != 0) {
		restartFeeServer();
	} else if ((header.flags & FEESERVER_REBOOT_FLAG) != 0) {
		createLogMessage(MSG_INFO, "Rebooting DCS board.", 0);
		system("reboot");
		exit(0);
	} else if ((header.flags & FEESERVER_SHUTDOWN_FLAG) != 0) {
		createLogMessage(MSG_INFO, "Shuting down DCS board.", 0);
		system("poweroff");
		exit(0);
	} else if ((header.flags & FEESERVER_EXIT_FLAG) != 0) {
		fee_exit_handler(0);
	} else if ((header.flags & FEESERVER_SET_DEADBAND_FLAG) != 0) {
		issueParam.nRet = setDeadband(&issueParam);
	} else if ((header.flags & FEESERVER_GET_DEADBAND_FLAG) != 0) {
		issueParam.nRet = getDeadband(&issueParam);
	} else if ((header.flags & FEESERVER_SET_ISSUE_TIMEOUT_FLAG) != 0) {
		issueParam.nRet = setIssueTimeout(&issueParam);
	} else if ((header.flags & FEESERVER_GET_ISSUE_TIMEOUT_FLAG) != 0) {
		issueParam.nRet = getIssueTimeout(&issueParam);
	} else if ((header.flags & FEESERVER_SET_UPDATERATE_FLAG) != 0) {
		issueParam.nRet = setUpdateRate(&issueParam);
	} else if ((header.flags & FEESERVER_GET_UPDATERATE_FLAG) != 0) {
		issueParam.nRet = getUpdateRate(&issueParam);
	} else if ((header.flags & FEESERVER_SET_LOGLEVEL_FLAG) != 0) {
		issueParam.nRet = setLogLevel(&issueParam);
	} else if ((header.flags & FEESERVER_GET_LOGLEVEL_FLAG) != 0) {
		issueParam.nRet = getLogLevel(&issueParam);
	} else {
		// commands for CE are not allowed in ERROR state
		if (state == ERROR_STATE) {
			leaveCommandHandler(header.id, FEE_WRONG_STATE, MSG_ERROR,
 					"FeeServer is in ERROR_STATE, ignoring command for CE!");
			return;
		}

		// packet with no flags in header and no payload makes no sense
		if (issueParam.size == 0) {
			leaveCommandHandler(header.id, FEE_INVALID_PARAM, MSG_WARNING,
 					"FeeServer received empty command.");
			return;
		}

		// lock mutex
		status = pthread_mutex_lock(&wait_mut);
		if (status != 0) {
			leaveCommandHandler(header.id, FEE_THREAD_ERROR, MSG_ERROR,
 					"Unable to lock condition mutex for watchdog.");
			return;
		}

		status = pthread_attr_init(&attr);
		if (status != 0) {
			unlockIssueMutex();
			leaveCommandHandler(header.id, FEE_THREAD_ERROR, MSG_ERROR,
 					"Unable to initialize issue thread.");
			return;
		}

		status = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
		if (status != 0) {
			unlockIssueMutex();
			leaveCommandHandler(header.id, FEE_THREAD_ERROR, MSG_ERROR,
 					"Unable to initialize issue thread.");
			return;
		}

		status = pthread_create(&thread_handle, &attr, &threadIssue, (void*) &issueParam);
		if (status != 0) {
			unlockIssueMutex();
			leaveCommandHandler(header.id, FEE_THREAD_ERROR, MSG_ERROR,
 					"Unable to create issue thread.");
			return;
		}

		status = pthread_attr_destroy(&attr);
		if (status != 0) {
#			ifdef __DEBUG
			printf("Destroy attribute error: %d\n", status);
			fflush(stdout);
#			endif
			createLogMessage(MSG_WARNING,
					"Unable to destroy thread attribute.", 0);
		}

		// timeout set in ms, see fee_defines.h for current value
		status = gettimeofday(&now, 0);
		if (status == 0) {
			// issueTimeout is in milliseconds:
			// get second-part with dividing by 1000
			timeout.tv_sec = now.tv_sec + (int) (issueTimeout / 1000);
			// get rest of division by 1000 (which is milliseconds)
			// and make it nanoseconds
			timeout.tv_nsec = (now.tv_usec * 1000) +
										((issueTimeout % 1000) * 1000000);

			// wait for finishing "issue" or timeout, if signal has been sent
			// retcode is 0 !
			// this is the main logic of the watchdog for the CE of the FeeServer
			retcode = pthread_cond_timedwait(&cond, &wait_mut, &timeout);
#			ifdef __DEBUG
			printf("Retcode of CMND timedwait: %d\n", retcode);
			fflush(stdout);
#			endif

			// check retcode to detect and handle Timeout
			if (retcode == ETIMEDOUT) {
#				ifdef __DEBUG
				printf("ControlEngine watchdog detected TimeOut.\n");
				fflush(stdout);
#				endif
				createLogMessage(MSG_WARNING,
						"ControlEngine watch dog noticed a time out for last command.", 0);

				// kill not finished thread. no problem if this returns an error
				pthread_cancel(thread_handle);
				// setting errorCode to "a timout occured"
				issueParam.nRet = FEE_TIMEOUT;
				issueParam.size = 0;
			} else if (retcode != 0) {
				// "handling" of other error than timeout
#				ifdef __DEBUG
				printf("ControlEngine watchdog detected unknown error.\n");
				fflush(stdout);
#				endif
				createLogMessage(MSG_WARNING,
						"ControlEngine watch dog received an unknown for last command.", 0);

				// kill not finished thread. no problem if this returns an error
				pthread_cancel(thread_handle);
				// setting errorCode to "a thread error occured"
				issueParam.nRet = FEE_THREAD_ERROR;
				issueParam.size = 0;
			}

		} else {
#			ifdef __DEBUG
			printf("Get time of day error: %d\n", status);
			fflush(stdout);
#			endif
			createLogMessage(MSG_WARNING,
				"Watchdog timer could not be initialized. Using non-reliable sleep instead.",
				0);
			// release mutex to avoid hang up in issueThread before signaling condition
			unlockIssueMutex();
			// watchdog with condition signal could not be used, because gettimeofday failed.
			// sleeping instead for usual amount of time and trying to cancel thread aftterwards.
			usleep(issueTimeout * 1000);
			status = pthread_cancel(thread_handle);
			// if thread did still exist something went wrong -> "timeout" (== 0)
			if (status == 0) {
#				ifdef __DEBUG
				printf("TimeOut occured.\n");
#				endif
				createLogMessage(MSG_WARNING,
						"ControlEngine issue did not return in time.", 0);
				issueParam.nRet = FEE_TIMEOUT;
				issueParam.size = 0;
			}
		}

		unlockIssueMutex();
	}
	//--- end of CE call area --------------------

	// ---------- start to compose result -----------------
#	ifdef __DEBUG
	printf("Issue-nRet: %d\n", issueParam.nRet);
	fflush(stdout);
#	endif
	// check return value of issue
	if ((issueParam.nRet < FEE_UNKNOWN_RETVAL) ||
			(issueParam.nRet > FEE_MAX_RETVAL)) {
		issueParam.nRet = FEE_UNKNOWN_RETVAL;
		createLogMessage(MSG_DEBUG,
				"ControlEngine [command] returned unkown RetVal.", 0);
	}

	if (cmndACK != 0) {
		free(cmndACK);
		cmndACK = 0;
	}
	// create Acknowledge as return value of command
	// HEADER_SIZE bytes are added before result to insert the command
	// header before the result -> see CommandHeader in Client for details
	cmndACK = (char*) malloc(issueParam.size + HEADER_SIZE);
	if (cmndACK == 0) {
		//no memory available!
#		ifdef __DEBUG
		printf("no memory available!\n");
		fflush(stdout);
#		endif
		createLogMessage(MSG_ERROR, "Insufficient memory for ACK.", 0);

		// no ACK because no memory!
		cmndACKSize = 0;
		status = pthread_mutex_unlock(&command_mut);
		if (status != 0) {
#			ifdef __DEBUG
			printf("Lock command mutex error: %d\n", status);
			fflush(stdout);
#			endif
			createLogMessage(MSG_WARNING,
					"Error while trying to unlock command mutex.", 0);
		}
		return;
	}

#	ifdef __DEBUG
	if (issueParam.size > 0) {
//		printf("in cmnd-Handler -> issue result: ");
//		printData(issueParam.result, 0, issueParam.size);
//		fflush(stdout);
	}
#	endif

	// checks checksumflag and calculates it if necessary
	if ((header.flags & CHECKSUM_FLAG) != 0) {
		header.checksum = calculateChecksum((unsigned char*) issueParam.result,
					issueParam.size);
		// !!! Do (Huffman- ) encoding, if wished afterwards.
	} else {
		header.checksum = CHECKSUM_ZERO;
	}

	// keep the whole flags also for the result packet
	header.errorCode = (short) issueParam.nRet;
#	ifdef __DEBUG
	printf("ErrorCode in Header: %d\n", header.errorCode);
	fflush(stdout);
#	endif

	pHeaderStream = marshallHeader(&header);
	memcpy((void*) cmndACK, (void*) pHeaderStream, HEADER_SIZE);
	if (pHeaderStream != 0) {
		free(pHeaderStream);
	}
	memcpy(((void*) cmndACK + HEADER_SIZE), (void*) issueParam.result,
				issueParam.size);

	//store the size of the result globally
	cmndACKSize = issueParam.size + HEADER_SIZE;
	// propagate change of ACK(nowledge channel) to upper Layers
	dis_update_service(serviceACKID);

#	ifdef __DEBUG
	// -- see the cmndACK as a char - string
	printf("ACK \n");
//	printData(cmndACK, HEADER_SIZE, cmndACKSize);
	// -- see the cmndACK in a HEX view for the ALTRO
	//print_package(cmndACK + HEADER_SIZE);
#	endif

	if (issueParam.result != 0) {
		free(issueParam.result);
	}

	// unlock command mutex, data has been sent
	status = pthread_mutex_unlock(&command_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Lock command mutex error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
				"Error while trying to unlock command mutex.", 0);
	}
}


//-- user_routine to provide the ACK-data
void ack_service(int* tag, char** address, int* size) {
#ifdef __BENCHMARK
	char benchmsg[200];
#endif	

	if ((tag == 0) || (*tag != ACK_SERVICE_TAG)) {
#		ifdef __DEBUG
		printf("invalid ACK Service\n");
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING, "DIM Framework called wrong ACK channel.",
				0);
		return;
	}
// use the line below for checking flags of an outgoing feePacket!
//        printf("\nack_service was called flags are:%x%x\n", *(cmndACK+6), *(cmndACK+7));
	if ((cmndACKSize > 0) && (cmndACK != 0)) {
		*address = cmndACK;
		*size = cmndACKSize;
	} else {
		*size = 0;
	}
#ifdef __BENCHMARK
    // make benchmark entry
	benchmsg[sprintf(benchmsg, 
			"FeeServer AckHandler (sending ACK) - Packet-ID: %d", *cmndACK)] = 0;
    createBenchmark(benchmsg);
#endif

}


void leaveCommandHandler(unsigned int id, short errorCode,
			unsigned int msgType, char* message) {
	int status = -1;

#	ifdef __DEBUG
	printf("%s\n", message);
	fflush(stdout);
#	endif

	createLogMessage(msgType, message, 0);

	// tell client that command is ignored
	if (cmndACK != 0) {
		free(cmndACK);
		cmndACK = 0;
	}
	// send error code
	cmndACK = createHeader(id, errorCode, false, false, 0);
	cmndACKSize = HEADER_SIZE;
	dis_update_service(serviceACKID);

	// unlock command mutex to "free" commandHandler
	status = pthread_mutex_unlock(&command_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Lock command mutex error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
				"Error while trying to unlock command mutex.", 0);
	}
}


void unlockIssueMutex() {
	int status = -1;

	status = pthread_mutex_unlock(&wait_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Unlock condition mutex error: %d. Going in ERROR state!\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_ALARM,
			"Unable to unlock watchdog mutex. No more commands will be possible for CE. Going in ERROR state!",
			0);
		state = ERROR_STATE;
	}
}


//-- publish-function called by CE (Control Engine) to declare Items
int publish(Item* item) {
	unsigned int id;
	char* serviceName = 0;

	// check for right state
	if (state != COLLECTING) {
		return FEE_WRONG_STATE;
	}

	// Testing for NULL - Pointer
	// !! Attention: if pointer is not initialized and also NOT set to NULL, this won't help !!
	if (item == 0) {
#		ifdef __DEBUG
		printf("Bad item, not published\n");
		fflush(stdout);
#		endif
		return FEE_NULLPOINTER;
	}
	if (item->name == 0 || item->location == 0) {
#		ifdef __DEBUG
		printf("Bad item, not published\n");
		fflush(stdout);
#		endif
		return FEE_NULLPOINTER;
	}

	// ------------------ //
	// Check name for duplicate here
	if (findItem(item->name) != 0) {
#		ifdef __DEBUG
		printf("Item name already published, item discarded.\n");
		fflush(stdout);
#		endif
		return FEE_ITEM_NAME_EXISTS;
	}

	// -- add item as service --
	serviceName = (char*) malloc(serverNameLength + strlen(item->name) + 2);
	if (serviceName == 0) {
		return FEE_INSUFFICIENT_MEMORY;
	}
	// terminate string with '\0'
	serviceName[sprintf(serviceName, "%s_%s", serverName, item->name)] = 0;
	id = dis_add_service(serviceName, "F", (int*) item->location,
			sizeof(float), 0, 0);
	free(serviceName);
	add_item_node(id, item);

	return FEE_OK;
}


//-- function to add service to our servicelist
void add_item_node(unsigned int _id, Item* _item) {
	//create new node with enough memory
	ItemNode* newNode = 0;

	newNode = (ItemNode*) malloc(sizeof(ItemNode));
	if (newNode == 0) {
		//no memory available!
#		ifdef __DEBUG
		printf("no memory available while adding itemNode!\n");
#		endif
		// !!! unable to run FeeServer, write msg in kernel logger !!! (->Tobias)
		cleanUp();
		exit(201);
	}
	//initialize "members" of node
	newNode->prev = 0;
	newNode->next = 0;
	newNode->id = _id;
	newNode->item = _item;
	newNode->lastTransmittedValue = *(_item->location);
	//if default deadband is negative -> set threshold 0, otherwise set half of defaultDeadband
	newNode->threshold = (_item->defaultDeadband < 0) ? 0.0 : (_item->defaultDeadband / 2);
 /*
		if(_item->defaultDeadband < 0) {
		newNode->threshold = 0.0;
        } else {
		newNode->threshold = _item->defaultDeadband / 2;
	}
*/
	newNode->locBackup = _item->location;
	newNode->checksum = calculateChecksum((unsigned char*) &(_item->location),
			sizeof(volatile float*));
	newNode->checksumBackup = newNode->checksum;

#ifdef __DEBUG
	// complete debug display of added Item
/*
	printf("Item: %d\n", _id);
	printf("location: %f, locBackup %f\n", *(_item->location), *(newNode->locBackup));
	printf("location addr: %x, locBackup addr %x\n", _item->location, newNode->locBackup);
	printf("checksum1: %d, checksum2: %d\n\n", newNode->checksum,
			newNode->checksumBackup);
*/
#endif

#	ifdef __DEBUG
	// short debug display of added Item
	printf("init of %s with ID %d: %f\n", newNode->item->name, newNode->id,
				newNode->lastTransmittedValue);
	fflush(stdout);
#	endif

	++nodesAmount;
	//redirect pointers of doubly linked list
	if (firstNode != 0) {
		lastNode->next = newNode;
		newNode->prev = lastNode;
		lastNode = newNode;
	} else {
		firstNode = newNode;
		lastNode = newNode;
	}
}


//-- Logging function -----
void createLogMessage(unsigned int type, char* description, char* origin) {
	int status = -1; // for mutex
	int descLength = 0;
	int originLength = 0;
	time_t timeVal;
	struct tm* now = 0;

	//lock access with mutex due to the fact that FeeServer & CE can use it
	status = pthread_mutex_lock(&log_mut);
	// discard eventual error, this would cause more problems
	// in each case, do NOT call createLogMessage ;) !
#	ifdef __DEBUG
	if (status != 0) {
		printf("Lock log mutex error: %d\n", status);
		fflush(stdout);
	}
#	endif

	if (!checkLogLevel(type)) {   //if not -> unlock mutex -> return
		//unlock mutex
		status = pthread_mutex_unlock(&log_mut);
		// discard eventual error, this would cause more problems
		// in each case, do NOT call createLogMessage ;)
#		ifdef __DEBUG
		if (status != 0) {
			printf("Unlock log mutex error: %d\n", status);
			fflush(stdout);
		}
#		endif
		return;
	}

	// prepare data (cut off overlength)
	if (description != 0) {
		// limit description to maximum of field in message struct if longer
		descLength = ((strlen(description) >= MSG_DESCRIPTION_SIZE)
				? (MSG_DESCRIPTION_SIZE - 1) : strlen(description));
	}
	if (origin != 0) {
		// limit origin to maximum of field in message struct if longer
		// be aware that "source" also contains server name and a slash
		originLength = ((strlen(origin) >= MSG_SOURCE_SIZE - serverNameLength - 1)
				? (MSG_SOURCE_SIZE - serverNameLength - 2) : strlen(origin));
	}

	//set type
	message.eventType = type;
	//set detector
	memcpy(message.detector, LOCAL_DETECTOR, MSG_DETECTOR_SIZE);
	//set origin
	strcpy(message.source, serverName);
	if (origin != 0) {
		// append slash
		strcpy(message.source + serverNameLength, "/");
		// append origin maximum til end of source field in message struct
		strncpy(message.source + serverNameLength + 1, origin, originLength);
		// terminate with '\0'
		message.source[serverNameLength + 1 + originLength] = 0;
	}
	//set description
	if (description != 0) {
		// fill description field of message struct maximum til end
		strncpy(message.description, description, descLength);
		// terminate with '\0'
		message.description[descLength] = 0;
	} else {
		strcpy(message.description, "No description specified.");
	}
	//set current date and time
	time(&timeVal);
	now = localtime(&timeVal);
	message.date[strftime(message.date, MSG_DATE_SIZE, "%Y-%m-%d %H:%M:%S",
				now)] = 0;

	//updateService
	dis_update_service(messageServiceID);

	//unlock mutex
	status = pthread_mutex_unlock(&log_mut);
	// discard eventual error, this would cause more problems
	// in each case, do NOT call createLogMessage ;)
#	ifdef __DEBUG
	if (status != 0) {
		printf("Unlock log mutex error: %d\n", status);
		fflush(stdout);
	}
#	endif
}


bool checkLogLevel(int event) {
	// Comparision with binary AND, if result has 1 as any digit, event is
	// included in current logLevel
	if ((logLevel & event) != 0) {
		return true;
	}
	return false;
}

void dim_error_msg_handler(int severity, int error_code, char* msg) {
	char type[8];
	int eventType = 0;
	char message[MSG_DESCRIPTION_SIZE];

	// map severity to own log levels
	switch (severity) {
		case 0:
			type[sprintf(type, "INFO")] = 0;
			eventType = MSG_INFO;
			break;
		case 1:
			type[sprintf(type, "WARNING")] = 0;
			eventType = MSG_WARNING;
			break;
		case 2:
			type[sprintf(type, "ERROR")] = 0;
			eventType = MSG_ERROR;
			break;
		case 3:
			type[sprintf(type, "FATAL")] = 0;
			eventType = MSG_ERROR;
			break;
		default :
			type[sprintf(type, "UNKNOWN")] = 0;
			eventType = MSG_WARNING;
			break;
	}

#   ifdef __DEBUG
	// print to command line if wanted
	printf("DIM: [%s] - %s.\n", type, msg);
	fflush(stdout);
#	endif

	// send message only if FeeServer is in serving or error state
	if ((state == RUNNING) || (state == ERROR_STATE)) {
		strncpy(message, msg, (MSG_DESCRIPTION_SIZE - 1));
		message[MSG_DESCRIPTION_SIZE - 1] = 0;
		// deliver message to FeeServer message system
		createLogMessage(eventType, message, "DIM\0");
	}
}


//-- tells the server, that he can start serving;
//-- no services can be added when server is in state RUNNING
int start(int initState) {
	int nRet = FEE_UNKNOWN_RETVAL;
	char* serviceName = 0;
	char* messageName = 0;
	char* commandName = 0;
	char msgStructure[50];

	if (state == COLLECTING) {
		//----- add service for acknowledge -----
		serviceName = (char*) malloc(serverNameLength + 13);
		if (serviceName == 0) {
			//no memory available!
#			ifdef __DEBUG
			printf("no memory available while trying to create ACK channel!\n");
			fflush(stdout);
#			endif
			// !!! unable to run FeeServer, write msg in kernel logger !!! (-> Tobias)
			cleanUp();
			exit(201);
		}
		// compose ACK channel name and terminate with '\0'
		serviceName[sprintf(serviceName, "%s_Acknowledge", serverName)] = 0;
		if (cmndACK != 0) {
			free(cmndACK);
			cmndACK = 0;
		}
		// take created header
		cmndACK = createHeader(0, initState, false, false, 0);
		cmndACKSize = HEADER_SIZE;
		// add ACK channel as service to DIM
		serviceACKID = dis_add_service(serviceName, "C", 0, 0, &ack_service,
				ACK_SERVICE_TAG);
		free(serviceName);

		//----- add message service -----
		messageName = (char*) malloc(serverNameLength + 9);
		if (messageName == 0) {
			//no memory available!
#			ifdef __DEBUG
			printf("no memory available while trying to create message channel!\n");
			fflush(stdout);
#			endif
			// !!! unable to run FeeServer, write msg in kernel logger !!! (->Tobias)
			cleanUp();
			exit(201);
		}
		// compose message channel name and terminate with '\0'
		messageName[sprintf(messageName, "%s_Message", serverName)] = 0;
		// compose message structure
		msgStructure[sprintf(msgStructure, "I:1;C:%d;C:%d;C:%d;C:%d",
				MSG_DETECTOR_SIZE, MSG_SOURCE_SIZE, MSG_DESCRIPTION_SIZE,
				MSG_DATE_SIZE)] = 0;
		// add message channel as service to DIM
		messageServiceID = dis_add_service(messageName, msgStructure, (int*) &message,
				sizeof(unsigned int) + MSG_DETECTOR_SIZE + MSG_SOURCE_SIZE +
				MSG_DESCRIPTION_SIZE + MSG_DATE_SIZE, 0, 0);
		free(messageName);

		//----- before start serving we add the only command handled by the server -----
		commandName = (char*) malloc(serverNameLength + 9);
		if (commandName == 0) {
			//no memory available!
#			ifdef __DEBUG
			printf("no memory available while trying to create CMD channel!\n");
			fflush(stdout);
#			endif
			// !!! unable to run FeeServer, write msg in kernel logger !!! (->Tobias)
			cleanUp();
			exit(201);
		}
		// compose Command channel name and terminate with '\0'
		commandName[sprintf(commandName, "%s_Command", serverName)] = 0;
		// add CMD channel as command to DIM, no tag needed,
		// only one command possible
		commandID = dis_add_cmnd(commandName, "C", &command_handler, 0);
		free(commandName);

		//-- now start serving --
		if (dis_start_serving(serverName) == 1) {
			// if start server was successful
			if (initState == FEE_OK) {
				state = RUNNING;
				// start monitoring thread now
				nRet = startMonitorThread();
				if (nRet != FEE_OK) {
#					ifdef __DEBUG
					printf("Could NOT start monitor thread, error: %d\n", nRet);
					fflush(stdout);
#					endif
					createLogMessage(MSG_ERROR,
							"Unable to start monitor thread on FeeServer.", 0);
					return nRet;
				}
				// inform CE about update rate
				provideUpdateRate();
				createLogMessage(MSG_INFO,
						"FeeServer started correctly, including monitor thread.", 0);
				nRet = FEE_OK;
			} else {
				state = ERROR_STATE;
				createLogMessage(MSG_ERROR,
						"Initialisation of ControlEngine failed. FeeServer is running in ERROR state (without CE).",
						0);
				// starting itself worked, so nRet is OK
				nRet = FEE_OK;
			}
		} else {
			// starting server was not successful, so remove added core - services
			// so they can be added again by next start() - call
			dis_remove_service(serviceACKID);
			free(cmndACK);
			cmndACK = 0;
			cmndACKSize = 0;
			dis_remove_service(messageServiceID);
			dis_remove_service(commandID);
			nRet = FEE_FAILED;
		}
		return nRet;
	}
	//server is already running
	return FEE_OK;
}

// ****************************************
// ---- starts the monitoring thread ----
// ****************************************
int startMonitorThread() {
	int status = -1;
	pthread_attr_t attr;

	// when item list is empty, no monitor thread is needed
	if (nodesAmount == 0) {
		createLogMessage(MSG_INFO, "No Items for monitoring available.", 0);
		return FEE_OK;
	}

	// init thread attribut and set it
	status = pthread_attr_init(&attr);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Init attribute error [mon]: %d\n", status);
		fflush(stdout);
#		endif
		return FEE_MONITORING_FAILED;
	}
	status = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Set attribute error [mon]: %d\n", status);
		fflush(stdout);
#		endif
		return FEE_MONITORING_FAILED;
	}

	// start the thread
	status = pthread_create(&thread_mon, &attr, (void*)&monitorValues, 0);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Create thread error [mon]: %d\n", status);
		fflush(stdout);
#		endif
		return FEE_MONITORING_FAILED;
	}

	// cleanup attribut
	status = pthread_attr_destroy(&attr);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Destroy attribute error [mon]: %d\n", status);
		fflush(stdout);
#		endif
		// no error return value necessary !
	}
	return FEE_OK;
}

// --- this is the monitoring thread ---
void monitorValues() {
	int status = -1;
	int nRet;
	unsigned long sleepTime = 0;
	ItemNode* current = 0;
	char msg[120];
    unsigned long innerCounter = 0; // used for update check after time interval
    unsigned long outerCounter = 0; // used for update check after time interval


	// set cancelation type
	status = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Set cancel state error [mon]: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
			"Unable to configure monitor thread properly. Monitoring is not affected.", 0);
	}
	status = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Set cancel type error [mon]: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
			"Unable to configure monitor thread properly. Monitoring is not affected.", 0);
	}

	createLogMessage(MSG_DEBUG, "started monitoring thread...", 0);

	while (1) {
		current = firstNode;
		sleepTime = (unsigned long) (updateRate / nodesAmount);
		while (current != 0) { // is lastNode->next (end of list)
 			if (!checkLocation(current)) {
				msg[sprintf(msg, "Value of item %s is corrupt, reconstruction failed. Ignoring!",
						current->item->name)] = 0;
				createLogMessage(MSG_ERROR, msg, 0);
				// message and do some stuff (like invalidate value)
				// continue; ??? (test, what happens if pointer is redirected !!!)
			}
			if ((abs((*(current->item->location)) - current->lastTransmittedValue)
						>= current->threshold) || (outerCounter ==
						(innerCounter * TIME_INTERVAL_MULTIPLIER))) {
				nRet = dis_update_service(current->id);
				current->lastTransmittedValue = *(current->item->location);
#				ifdef __DEBUG
/* 				printf("Updated %d clients for service %s (%d)\n", nRet, */
/* 						current->item->name, current->id); */
/* 				fflush(stdout); */
#				endif
			}

			++innerCounter;
			current = current->next;
			usleep(sleepTime * 1000);
			// sleeps xy microseconds, needed milliseconds-> "* 1000"
		}
		// with the check of both counter, each service is at least updated after
		// every (deadband updateRate * nodesAmount) seconds
		innerCounter = 0;
		++outerCounter;
		// after every service in list is updated set counter back to 0
		// the TIME_INTERVAL_MULTIPLIER is used to enlarge the time interval of
		// the request of services without touching the deadband checker updateRate
		if (outerCounter >= (nodesAmount * TIME_INTERVAL_MULTIPLIER)) {
			outerCounter = 0;
		}
	}
	// should never be reached !
	pthread_exit(0);
}

// checks against bitflips in location
bool checkLocation(ItemNode* node) {
	if (node->item->location == node->locBackup) {
		// locations are identical, so no bitflip
		return true;
	}
	// locations are not identical, check further

	if (node->checksum == calculateChecksum((unsigned char*)
			&(node->item->location), sizeof(volatile float*))) {
		// checksum tells, that first location should be valid, repair backup
		node->locBackup = node->item->location;
		return true;
	}
	// original location or first checksum is wrong, continue checking

	if (node->checksum == calculateChecksum((unsigned char*)
			&(node->locBackup), sizeof(volatile float*))) {
		// checksum tells, that location backup should be valid, repair original
		node->item->location = node->locBackup;
		return true;
	}
	// location backup or first checksum is wrong, continue checking

	if (node->checksum == node->checksumBackup) {
		// it seems that location and location backup are wrong
		// or checksum value runs banana, not repairable
		return false;
	}
	// it seems that first checksum is wrong
	// try to fix with second checksum

	if (node->checksumBackup == calculateChecksum((unsigned char*)
			&(node->item->location), sizeof(volatile float*))) {
		// checksum backup tells, that first location should be valid, repair backup
		node->locBackup = node->item->location;
		// repair first checksum
		node->checksum = node->checksumBackup;
		return true;
	}
	// original location or second checksum is wrong, continue checking

	if (node->checksumBackup == calculateChecksum((unsigned char*)
			&(node->locBackup), sizeof(volatile float*))) {
		// checksum backup tells, that location backup should be valid, repair original
		node->item->location = node->locBackup;
		// repair checksum
		node->checksum = node->checksumBackup;
		return true;
	}
	// value is totally banana, no chance to fix
	return false;
}


void* threadIssue(void* threadParam) {
	IssueStruct* issueParam = (IssueStruct*) threadParam;
	int status;

	status = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Set cancel state error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
			"Unable to configure issue thread properly. Execution might eventually be affected.", 0);
	}

	status = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Set cancel type error error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
			"Unable to configure issue thread properly. Execution might eventually be affected.", 0);
	}

	// executing command inside CE
	issueParam->nRet = issue(issueParam->command, &(issueParam->result), &(issueParam->size));

    //set cancel type to deferred
    status = pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, 0);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Set cancel type error: %d\n", status);
		fflush(stdout);
#	   endif
		createLogMessage(MSG_WARNING,
			"Unable to configure issue thread properly. Execution might eventually be affected.", 0);
    }

	//lock the mutex before broadcast
	status = pthread_mutex_lock(&wait_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Lock cond mutex error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
			"Unable to lock condition mutex for watchdog in issue thread. Execution might eventually be affected.",
			0);
	}

	//signal that issue has returned from ControlEngine
	// maybe try the call pthread_cond_signal instead for performance
	pthread_cond_broadcast(&cond);

	// unlock mutex
	status = pthread_mutex_unlock(&wait_mut);
	if (status != 0) {
#		ifdef __DEBUG
		printf("Unlock cond mutex error: %d\n", status);
		fflush(stdout);
#		endif
		createLogMessage(MSG_WARNING,
			"Unable to unlock condition mutex for watchdog in issue thread. Execution might eventually be affected.",
			0);
	}

//	not needed, return 0 is better solution
//	pthread_exit(0);
	return 0;
}


char* createHeader(unsigned int id, short errorCode, bool huffmanFlag,
					bool checksumFlag, int checksum) {
	char* pHeader = 0;
	FlagBits flags = NO_FLAGS;

	if (huffmanFlag) {
		//set huffman flag via binary OR
		flags |= HUFFMAN_FLAG;
	}
	if (checksumFlag) {
		//set checksum flag via binary OR
		flags |= CHECKSUM_FLAG;
	}

	pHeader = (char*) malloc(HEADER_SIZE);
	if (pHeader == 0) {
		//no memory available!
#		ifdef __DEBUG
		printf("no memory available while trying to create header!\n");
		fflush(stdout);
#		endif
		createLogMessage(MSG_ALARM,
				"No more memory available, unable to continue serving - exiting.",
				0);
		cleanUp();
		exit(201);
	}

	memcpy(pHeader, &id, HEADER_SIZE_ID);
	memcpy(pHeader + HEADER_OFFSET_ID, &errorCode, HEADER_SIZE_ERROR_CODE);
	memcpy(pHeader + HEADER_OFFSET_ERROR_CODE, &flags, HEADER_SIZE_FLAGS);
	memcpy(pHeader + HEADER_OFFSET_FLAGS, &checksum, HEADER_SIZE_CHECKSUM);

	return pHeader;
}


bool checkCommand(char* payload, int size, unsigned int checksum) {
	unsigned int payloadChecksum = 0;

	// payload has to contain data, if size is greater than 0,
	// and size must not be negative
	if (((payload == 0) && (size > 0)) || (size < 0)) {
		return false;
	}

	payloadChecksum = calculateChecksum((unsigned char*) payload, size);
#	ifdef __DEBUG
	printf("\nReceived Checksum: \t%x ,  \nCalculated Checksum: \t%x .\n\n",
		checksum, payloadChecksum);
	fflush(stdout);
#	endif
	return (payloadChecksum == checksum) ? true : false;
}

// ! Problems with signed and unsigned char, make differences in checksum
// -> so USE "unsigned char*" !
unsigned int calculateChecksum(unsigned char* buffer, int size) {
	int n;
	unsigned int checks = 0;
	unsigned long adler = 1L;
	unsigned long part1 = adler & 0xffff;
	unsigned long part2 = (adler >> 16) & 0xffff;

	// calculates the checksum with the Adler32 algorithm
	for (n = 0; n < size; n++) {
		part1 = (part1 + buffer[n]) % ADLER_BASE;
		part2 = (part2 + part1) % ADLER_BASE;
	}
	checks = (unsigned int) ((part2 << 16) + part1);

	return checks;
}

char* marshallHeader(CommandHeader* pHeader) {
	char* tempHeader = 0;

	tempHeader = (char*) malloc(HEADER_SIZE);
	if (tempHeader == 0) {
		//no memory available!
#		ifdef __DEBUG
		printf("no memory available!\n");
		fflush(stdout);
#		endif
		createLogMessage(MSG_ALARM,
				"No more memory available, unable to continue serving - exiting.",
				0);
		cleanUp();
		exit(201);
	}

	memcpy(tempHeader, &(pHeader->id), HEADER_SIZE_ID);
	memcpy(tempHeader + HEADER_OFFSET_ID, &(pHeader->errorCode), HEADER_SIZE_ERROR_CODE);
	memcpy(tempHeader + HEADER_OFFSET_ERROR_CODE, &(pHeader->flags), HEADER_SIZE_FLAGS);
	memcpy(tempHeader + HEADER_OFFSET_FLAGS, &(pHeader->checksum), HEADER_SIZE_CHECKSUM);

	return tempHeader;
}

// *********************************************************************************************
// ---------------- here come all the FeeServer commands ----------------------------------------
// *********************************************************************************************
void updateFeeServer(IssueStruct* issueParam) {
#ifdef ENABLE_MASTERMODE
	int status = 0;
	int i = 0;
	FILE* fp = 0;

	if ((*issueParam).size == 0) {
		createLogMessage(MSG_ERROR, "Received new FeeServer with size 0.", 0);
		return;
	}
#	ifdef __DEBUG
	printf("Received update command, updating FeeServer now!\n");
	fflush(stdout);
#	endif
	// execute instruction self and release mutex before return
	fp = fopen("newFeeserver", "w+b");
	if (fp == 0) {
		createLogMessage(MSG_ERROR, "Unable to save new FeeServer binary.", 0);
		return;
	}

	for ( i = 0; i < (*issueParam).size; ++i) {
		fputc((*issueParam).command[i], fp);
	}
	fclose(fp);
	createLogMessage(MSG_INFO, "FeeServer updated.", 0);

	// should we call cleanUp() before restart
	// better not: another possibility to hang (ask Tobias !!!)
	// -> cleanUp is in restart implicit !!! except opened drivers
	// could only be necessary, if some driver conns have to be closed.
	cleanUp();
	status = pthread_mutex_unlock(&command_mut);
#	ifdef __DEBUG
	if (status != 0) {
		printf("Unlock FeeCommand mutex error: %d\n", status);
		fflush(stdout);
	}
#	endif

	// Exit status "3" tells the starting script to finalise update and restart
	// FeeServer after termination.
	exit(3);
#endif //ENABLE_MASTERMODE
}

void restartFeeServer() {
	int status = 0;

	createLogMessage(MSG_INFO, "Restarting FeeServer.", 0);

	// should we call cleanUp() before restart
	// better not: another possibility to hang (ask Tobias !!!)
	// -> cleanUp is in restart implicit !!! except opened drivers
	// could only be necessary, if some driver cons has to be closed.
	cleanUp();
	status = pthread_mutex_unlock(&command_mut);
#	ifdef __DEBUG
	if (status != 0) {
		printf("Unlock FeeCommand mutex error: %d\n", status);
		fflush(stdout);
	}
#	endif

	// Exit status "2" tells the starting script to restart FeeServer after
	// termination.
	exit(2);
}

int setDeadband(IssueStruct* issueParam) {
	char* itemName = 0;
	float newDeadband = 0;
	int nameLength = 0;
	ItemNode* node = 0;
	char msg[100];
	unsigned int count = 0;

	if ((*issueParam).size <= sizeof(newDeadband)) {
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for setting dead band contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	}

	nameLength = (*issueParam).size - sizeof(newDeadband);

	if (nameLength <= 0) {
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for setting dead band contained no service name.",
				0);
		return FEE_INVALID_PARAM;
	}

	itemName = (char*) malloc(nameLength + 1);
	if (itemName == 0) {
		(*issueParam).size = 0;
		return FEE_INSUFFICIENT_MEMORY;
	}

	memcpy(&newDeadband, (*issueParam).command, sizeof(newDeadband));
	memcpy(itemName, (*issueParam).command + sizeof(newDeadband), nameLength);
	itemName[nameLength] = 0;

	if (itemName[0] != '*') {
		// search wanted itemNode
		node = findItem(itemName);
		if (node == 0) {
			// message already sent in findItem()
			free(itemName);
			(*issueParam).size = 0;
			createLogMessage(MSG_DEBUG,
					"FeeServer command for setting dead band contained invalid parameter.",
					0);
			return FEE_INVALID_PARAM;
		} else {
			// set new threshold ( = dead  band / 2)
			node->threshold = newDeadband / 2;
#			ifdef __DEBUG
			printf("Set deadband on item %s to %f.\n", itemName, newDeadband);
			fflush(stdout);
#			endif
			msg[sprintf(msg, "New dead band (%f) is set for item %s.",
					newDeadband, itemName)] = 0;
			createLogMessage(MSG_INFO, msg, 0);
		}
	} else {
		// set now for all wanted value the new deadband
		count = setDeadbandBroadcast(itemName, newDeadband);

#		ifdef __DEBUG
		printf("Set deadband for %d items (%s) to %f.\n", count, itemName, newDeadband);
		fflush(stdout);
#		endif
		msg[sprintf(msg, "New dead band (%f) is set for %d items (%s).",
				newDeadband, count, itemName)] = 0;
		createLogMessage(MSG_INFO, msg, 0);
	}

	free(itemName);
	(*issueParam).size = 0;
	return FEE_OK;
}

int getDeadband(IssueStruct* issueParam) {
	char* itemName = 0;
	int nameLength = 0;
	ItemNode* node = 0;
	float currentDeadband = 0;
	char msg[120];

	if ((*issueParam).size <= 0) {
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for getting dead band contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	}

	nameLength = (*issueParam).size;
	itemName = (char*) malloc(nameLength + 1);
	if (itemName == 0) {
		(*issueParam).size = 0;
		return FEE_INSUFFICIENT_MEMORY;
	}

	memcpy(itemName, (*issueParam).command, nameLength);
	itemName[nameLength] = 0;

	// search wanted itemNode
	node = findItem(itemName);
	if (node == 0) {
		// message already sent in findItem()
		free(itemName);
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for getting dead band contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	} else {
		(*issueParam).result = (char*) malloc(sizeof(float) + nameLength);
		if ((*issueParam).result == 0) {
			(*issueParam).size = 0;
			return FEE_INSUFFICIENT_MEMORY;
		}

		// copy current deadband value to ACK result
		currentDeadband = node->threshold * 2.0; // compute deadband
		memcpy((*issueParam).result, &currentDeadband, sizeof(float));
		memcpy((*issueParam).result + sizeof(float), itemName, nameLength);
		(*issueParam).size = sizeof(float) + nameLength;
#		ifdef __DEBUG
		printf("Current deadband on item %s is %f.\n", itemName, currentDeadband);
		fflush(stdout);
#		endif
		msg[sprintf(msg, "Current deadband for item %s is %f.", itemName,
				currentDeadband)] = 0;
		createLogMessage(MSG_DEBUG, msg, 0);
	}
	free(itemName);
	return FEE_OK;
}

int setIssueTimeout(IssueStruct* issueParam) {
	char msg[70];
	unsigned long newTimeout;

	if ((*issueParam).size < sizeof(unsigned long)) {
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for setting issue timeout contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	}
	memcpy(&newTimeout, (*issueParam).command, sizeof(unsigned long));

	// check new timeout for possible buffer overflow (value will be multiplied
	// with 1000 later -> has to lower than 4294967 )
	if (newTimeout > MAX_ISSUE_TIMEOUT) {
        (*issueParam).size = 0;
        createLogMessage(MSG_WARNING,
                "New timeout for issue watchdog exceeded value limit (4294967).",
                0);
        return FEE_INVALID_PARAM;
    }

	issueTimeout = newTimeout;
#	ifdef __DEBUG
	printf("set new Issue timeout to %lu\n", issueTimeout);
	fflush(stdout);
#	endif
	msg[sprintf(msg, "Watch dog time out is set to %lu.", issueTimeout)] = 0;
	createLogMessage(MSG_INFO, msg, 0);

	(*issueParam).size = 0;
	return FEE_OK;
}

int getIssueTimeout(IssueStruct* issueParam) {
	char msg[50];

	(*issueParam).result = (char*) malloc(sizeof(unsigned long));
	if ((*issueParam).result == 0) {
		(*issueParam).size = 0;
		return FEE_INSUFFICIENT_MEMORY;
	}

	// copy current timeout for issue to ACK result
	memcpy((*issueParam).result, &issueTimeout, sizeof(unsigned long));
	(*issueParam).size = sizeof(unsigned long);
#	ifdef __DEBUG
	printf("issue timeout is %lu\n", issueTimeout);
	fflush(stdout);
#	endif
	msg[sprintf(msg, "Issue timeout is %lu.", issueTimeout)] = 0;
	createLogMessage(MSG_DEBUG, msg, 0);

	return FEE_OK;
}

int setUpdateRate(IssueStruct* issueParam) {
	char msg[70];
	unsigned short newRate;

	if ((*issueParam).size < sizeof(unsigned short)) {
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for setting update rate contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	}
	memcpy(&newRate, (*issueParam).command, sizeof(unsigned short));
	updateRate = newRate;
	// inform CE about update rate change
	provideUpdateRate();
#	ifdef __DEBUG
	printf("set new update rate to %d\n", updateRate);
	fflush(stdout);
#	endif
	msg[sprintf(msg, "New update rate for monitoring items: %d.", updateRate)] = 0;
	createLogMessage(MSG_INFO, msg, 0);

	(*issueParam).size = 0;
	return FEE_OK;
}

int getUpdateRate(IssueStruct* issueParam) {
	char msg[50];

	(*issueParam).result = (char*) malloc(sizeof(unsigned short));
	if ((*issueParam).result == 0) {
		(*issueParam).size = 0;
		return FEE_INSUFFICIENT_MEMORY;
	}

	// copy current update rate to ACK result
	memcpy((*issueParam).result, &updateRate, sizeof(unsigned short));
	(*issueParam).size = sizeof(unsigned short);
#	ifdef __DEBUG
	printf("update rate is %d\n", updateRate);
	fflush(stdout);
#	endif
	msg[sprintf(msg, "Monitoring update rate is %d.", updateRate)] = 0;
	createLogMessage(MSG_DEBUG, msg, 0);

	return FEE_OK;
}

// be aware of different size of unsigned int in heterogen systems !!
int setLogLevel(IssueStruct* issueParam) {
	int status = -1;
	char msg[70];
	unsigned int testLogLevel = 0;

	if ((*issueParam).size < sizeof(unsigned int)) {
		(*issueParam).size = 0;
		createLogMessage(MSG_DEBUG,
				"FeeServer command for setting log level contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	}
	memcpy(&testLogLevel, (*issueParam).command, sizeof(unsigned int));
	// check loglevel for valid data
	if (testLogLevel > MSG_MAX_VAL) {
#		ifdef __DEBUG
		printf("received invalid log level %d\n", testLogLevel);
		fflush(stdout);
#		endif
		createLogMessage(MSG_DEBUG,
				"FeeServer command for setting log level contained invalid parameter.",
				0);
		return FEE_INVALID_PARAM;
	}

	status = pthread_mutex_lock(&log_mut);
	// discard eventual error
	if (status != 0) {
#		ifdef __DEBUG
		printf("Lock log mutex error: %d\n", status);
		fflush(stdout);
#		endif
		(*issueParam).size = 0;
		return FEE_FAILED;
	} else {
		logLevel = testLogLevel | MSG_ALARM;

		status = pthread_mutex_unlock(&log_mut);
		if (status != 0) {
#			ifdef __DEBUG
			printf("Unlock log mutex error: %d\n", status);
			fflush(stdout);
#			endif
			createLogMessage(MSG_WARNING, "Unable to unlock logger mutex.", 0);
		}

#		ifdef __DEBUG
		printf("set new logLevel to %d\n", logLevel);
		fflush(stdout);
#		endif
		msg[sprintf(msg, "New log level on FeeServer: %d.", logLevel)] = 0;
		createLogMessage(MSG_INFO, msg, 0);

		(*issueParam).size = 0;
		return FEE_OK;
	}
}

int getLogLevel(IssueStruct* issueParam) {
	char msg[50];

	(*issueParam).result = (char*) malloc(sizeof(unsigned int));
	if ((*issueParam).result == 0) {
		(*issueParam).size = 0;
		return FEE_INSUFFICIENT_MEMORY;
	}

	// copy current update rate to ACK result
	memcpy((*issueParam).result, &logLevel, sizeof(unsigned int));
	(*issueParam).size = sizeof(unsigned int);
#	ifdef __DEBUG
	printf("Requested loglevel is %d\n", logLevel);
	fflush(stdout);
#	endif
	msg[sprintf(msg, "Requested LogLevel is %d.", logLevel)] = 0;
	createLogMessage(MSG_DEBUG, msg, 0);

	return FEE_OK;
}

void provideUpdateRate() {
	FeeProperty feeProp;
	if ((ceInitState == CE_OK) && (nodesAmount > 0)) {
		feeProp.flag = PROPERTY_UPDATE_RATE;
		feeProp.uShortVal = updateRate;
		signalFeePropertyChanged(&feeProp);
	}
}


unsigned int setDeadbandBroadcast(char* name, float newDeadbandBC) {
	unsigned int count = 0;
	ItemNode* current = 0;
	char* namePart = 0;
	char* itemNamePart = 0;

	if (name == 0) {
		return count;
	}

	// pointer at first occurance of "_"
	namePart = strpbrk(name, "_");
	if (namePart == 0) {
		return count;
	}

	current = firstNode;
	while (current != 0) {  // is end of list
		// pointer at first occurance of "_"
		itemNamePart = strpbrk(current->item->name, "_");
		// check if "_" not first character in name and is existing
		if ((itemNamePart == 0) || (itemNamePart == current->item->name)) {
			current = current->next;
			continue;
		}
		if (strcmp(namePart, itemNamePart) == 0) {
			// success, set threshold (= deadband / 2)
			current->threshold = newDeadbandBC / 2;
			++count;
		}
		current = current->next;
	}

	return count;
}

int updateFeeService(char* serviceName) {
	ItemNode* node = 0;
	int nRet = 0;

	if (state != RUNNING) {
		return FEE_WRONG_STATE;
	}
	if (serviceName == 0) {
		return FEE_NULLPOINTER;
	}

	// find desired service
	node = findItem(serviceName);

	// check node
	if (node == 0) {
		return FEE_INVALID_PARAM;
	}

	nRet = dis_update_service(node->id);
	// return number of updated clients
	return nRet;
}


// *********************************************************************************************
// ---------- here comes all the initialisation of selfdefined datatypes -----------------------
// *********************************************************************************************
void initIssueStruct(IssueStruct* issueStr) {
	issueStr->nRet = 0;
	issueStr->command = 0;
	issueStr->result = 0;
	issueStr->size = 0;
}

void initMessageStruct() {
	time_t timeVal;
	struct tm* now;

	message.eventType = MSG_INFO;
	memcpy(message.detector, LOCAL_DETECTOR, MSG_DETECTOR_SIZE);
	memcpy(message.source, "FeeServer\0", 10);
	message.description[sprintf(message.description,
			"FeeServer %s (Version: %s) has been initialized (DNS: %s) ...",
			serverName, FEESERVER_VERSION, getenv("DIM_DNS_NODE"))] = 0;
	//set current date and time
	time(&timeVal);
	now = localtime(&timeVal);
	message.date[strftime(message.date, MSG_DATE_SIZE, "%Y-%m-%d %H:%M:%S",
			now)] = 0;
}


void initItemNode(ItemNode* iNode) {
	iNode->prev = 0;
	iNode->next = 0;
	iNode->id = 0;
	iNode->item = 0;
	iNode->lastTransmittedValue = 0.0;
	iNode->threshold = 0.0;
	iNode->locBackup = 0;
	iNode->checksum = 0;
	iNode->checksumBackup = 0;
}

ItemNode* findItem(char* name) {
	char msg[70];
	ItemNode* current = 0;

	if (name == 0) {
		return 0;
	}
	current = firstNode;
	while (current != 0) {  // is end of list
		if (strcmp(name, current->item->name) == 0) {
			// success, give back itemNode
			return current;
		}
		current = current->next;
	}
	if (state == RUNNING) {
		msg[sprintf(msg, "Item %s not found in list.", name)] = 0;
		createLogMessage(MSG_WARNING, msg, 0);
#		ifdef __DEBUG
		printf("Item %s not found in list.\n", name);
		fflush(stdout);
#		endif
	}
	return 0;
}

void unpublishItemList() {
	ItemNode* current = 0;

	current = firstNode;
	while (current != 0) {
		dis_remove_service(current->id);
		current = current->next;
	}
	// pretending ItemList is completely empty to avoid access
	// to not existing elements
	nodesAmount = 0;
	firstNode = 0;
	lastNode = 0;
}

// ****************************************************************************
// ------------------ here come all the closing and cleanup functions ---------
// ****************************************************************************
void interrupt_handler(int sig) {
// *** causes props on DCS board -> not used yet ***
#	ifdef __DEBUG
	printf("Received interrupt: %d, exiting now.\n", sig);
	fflush(stdout);
#	endif

	if ((state == RUNNING) || (state == ERROR_STATE)) {
		fee_exit_handler(0);
	} else {
		cleanUp();
		exit(0);
	}
}

void fee_exit_handler(unsigned int state) {
	char msg[70];

#	ifdef __DEBUG
	printf("Exit state: %d\n\n", state);
	fflush(stdout);
#	endif
	msg[sprintf(msg, "Exiting FeeServer (exit state: %d).", state)] = 0;
	createLogMessage(MSG_INFO, msg, 0);

	cleanUp();
	exit(state);
}

void dim_dummy_exit_handler(int* bufp) {
	char msg[200];
	char clientName[50];
	int dummy = 0;

	// if bufp null pointer, redirect to valid value
	if (bufp == 0) {
		bufp = &dummy;
	}

	// DO almost nothing, just to disable the build-in exit command of the DIM framework
	// just notifying about intrusion, except for framework exit
	clientName[0] = 0;
	dis_get_client(clientName);
	// let's asume exit from ambitious user has clientName (pid@host).
	if (clientName[0] == 0) {
#		ifdef __DEBUG
		printf("Framework tries to exit FeeServer (%d)\n", *bufp);
		printf("Most likely FeeServer name already exists.\n");
		fflush(stdout);
#		endif
		// IMPORTANT don't use the bufp - state of framework, it could interfere
		// with own specified exit states !!! (e.g. for restarting in case of "2")
		// the same state is signaled by kill all servers of dns !!! -> ???
		fee_exit_handler(204);
	} else {
		msg[sprintf(msg, "Ambitious user (%s) tried to kill FeeServer, ignoring command!",
				clientName)] = 0;
		createLogMessage(MSG_WARNING, msg, 0);
#		ifdef __DEBUG
		printf("Ambitious user (%s) tried to kill FeeServer (%d)\n", clientName, *bufp);
		fflush(stdout);
#		endif
	}
}

void cleanUp() {
	// the order of the clean up sequence here is important to evade seg faults
	cleanUpCE();

	if (state == RUNNING) {
		pthread_cancel(thread_mon);
		pthread_cancel(thread_init);
	}

	dis_stop_serving();

	deleteItemList();

	if (cmndACK != 0) {
		free(cmndACK);
	}
	if (serverName != 0) {
		free(serverName);
	}

}

int deleteItemList() {
	ItemNode* tmp = 0;

	while (firstNode != 0) {
		if (firstNode->item != 0) {
			if (firstNode->item->name != 0) {
				free(firstNode->item->name);
			}
			free(firstNode->item);
		}

		tmp = firstNode->next;
		free(firstNode);
		firstNode = tmp;
	}
	return FEE_OK;
}


/// ***************************************************************************
/// -- only for the benchmarking cases necessary
/// ***************************************************************************
#ifdef __BENCHMARK
void createBenchmark(char* msg) {
	// this part is only used for benchmarking
	// timestamp to benchmark reception of a command 
    struct tm *today;
	struct timeval tStamp;
	char benchmark_msg[200];
	int status = 0;
	FILE* pFile = 0;

    status = gettimeofday(&tStamp, 0);
    if (status != 0) {
	benchmark_msg[sprintf(benchmark_msg,
                "Unable to get timestamp for benchmark!")] = 0;
	} else {
        today = localtime(&(tStamp.tv_sec));
    	benchmark_msg[sprintf(benchmark_msg, 
				"%s: \t%.8s - %ld us", msg, asctime(today) + 11, 
				tStamp.tv_usec)] = 0;
	}
	
	if (getenv("FEE_BENCHMARK_FILENAME")) {
		pFile = fopen(getenv("FEE_BENCHMARK_FILENAME"), "a+");
		if (pFile) {
			fprintf(pFile, benchmark_msg);
			fprintf(pFile, "\n");
			fclose(pFile);
		} else {
#ifdef __DEBUG
			printf("Unable to open benchmark file.\n");
			printf("%s\n", benchmark_msg);
#endif
			createLogMessage(MSG_WARNING, "Unable to write to benchmarkfile.", 0);
		}
	} else {
		createLogMessage(MSG_SUCCESS_AUDIT, benchmark_msg, 0);
	}
}
#else
// empty function 
void createBenchmark(char* msg) {
}
#endif



/// ***************************************************************************
/// -- only for the debugging cases necessary
/// ***************************************************************************
#ifdef __DEBUG
void printData(char* data, int start, int size) {
	int i;
	int iBackUp;

	if ((data == 0) || (size == 0)) {
		return;
	}
	iBackUp = start;
	for (i = start; (i < size) || (iBackUp < size); ++i) {
		++iBackUp;
		printf("%c", data[i]);
	}
	printf("\nData - Size: %d", size);
	printf("\n\n");
	fflush(stdout);
}
#endif


//-- only for the testcases necessary
#ifdef __UTEST
const int getState() {
	return state;
}

const ItemNode* getFirstNode() {
	return firstNode;
}

const ItemNode* getLastNode() {
	return lastNode;
}

int listSize() {
	ItemNode* tmp = firstNode;
	int count = 0;

	while (tmp != 0) {
		tmp = tmp->next;
		++count;
	}
	return count;
}

const char* getCmndACK() {
	return cmndACK;
}

void setState(int newState) {
	state = newState;
}

void setCmndACK(char* newData) {
	cmndACK = newData;
}

void setCmndACKSize(int size) {
	cmndACKSize = size;
}

void setServerName(char* name) {
	if (serverName != 0) {
		free(serverName);
	}
	serverName = (char*) malloc(strlen(name) + 1);
	if (serverName == 0) {
		printf(" No memory available !\n");
	}
	strcpy(serverName, name);
}

void clearServerName() {
	if (serverName != 0) {
		free(serverName);
	}
}

void stopServer() {
	dis_remove_service(serviceACKID);
	dis_remove_service(commandID);
	dis_stop_serving();
}

pthread_cond_t* getInitCondPtr() {
	return &init_cond;
}


pthread_mutex_t* getWaitInitMutPtr() {
	return &wait_init_mut;
}

#endif

